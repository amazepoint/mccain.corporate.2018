﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace McCain.Integration.SalesForce
{
    public class SalesForceClient
    {
        public string LoginEndpoint { get; set; }

        public const string API_ENDPOINT = "/services/data/v45.0/";

        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string AuthToken { get; set; }
        public string InstanceUrl { get; set; }

        static SalesForceClient()
        {
            // SF requires TLS 1.1 or 1.2
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        public void Login()
        {
            String response;
            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new Dictionary<string, string>
                    {
                        {"grant_type", "password"},
                        {"client_id", ClientId},
                        {"client_secret", ClientSecret},
                        {"username", Username},
                        {"password", Password + Token}
                    }
                );
                var message = client.PostAsync(LoginEndpoint, content).Result;
                response = message.Content.ReadAsStringAsync().Result;
            }
            Console.WriteLine($"Response: {response}");
            var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

            if (values.ContainsKey("access_token"))
            {
                AuthToken = values["access_token"];
                InstanceUrl = values["instance_url"];
            }
            else
            {
                throw new Exception("Login failed. API Response: " + response);
            }
        }

        public HttpResponseMessage Post(string postPath, Dictionary<string, object> data)
        {
            using (var client = new HttpClient())
            {
                string restQuery = InstanceUrl + API_ENDPOINT + postPath;
                var request = new HttpRequestMessage(HttpMethod.Post, restQuery);
                request.Headers.Add("Authorization", "Bearer " + AuthToken);
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                request.Headers.Add("Cache-Control", "no-cache");

                request.Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                var response = client.SendAsync(request).Result;

                return response;
            }
        }

        public string Query(string queryString)
        {
            using (var client = new HttpClient())
            {
                string restQuery = InstanceUrl + API_ENDPOINT + "query/?q=" + queryString;
                var request = new HttpRequestMessage(HttpMethod.Get, restQuery);
                request.Headers.Add("Authorization", "Bearer " + AuthToken);
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                request.Headers.Add("X-PrettyPrint", "1");
                var response = client.SendAsync(request).Result;
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        public string Describe(string sobjectName)
        {
            using (var client = new HttpClient())
            {
                var url = InstanceUrl + API_ENDPOINT + "sobjects/" + sobjectName + "/describe/";

                var request = new HttpRequestMessage(HttpMethod.Get, url);
                request.Headers.Add("Authorization", "Bearer " + AuthToken);
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                request.Headers.Add("X-PrettyPrint", "1");
                var response = client.SendAsync(request).Result;
                var result = response.Content.ReadAsStringAsync().Result;

                return result;
            }
        }
    }
}
