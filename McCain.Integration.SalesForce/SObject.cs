﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McCain.Integration.SalesForce
{
    public class SObject
    {
        public string Name { get; set; }

        public List<SField> Fields { get; set; }
    }
}
