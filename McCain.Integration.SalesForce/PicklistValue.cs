﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McCain.Integration.SalesForce
{
    public class PicklistValue
    {
        public string Value { get; set; }

        public string Label { get; set; }
    }
}
