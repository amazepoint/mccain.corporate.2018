﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McCain.Integration.SalesForce
{
    public class SField
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public List<PicklistValue> PicklistValues { get; set; }
    }
}
