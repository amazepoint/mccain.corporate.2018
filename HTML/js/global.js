$(document).ready(function () {
// Worldwide
	$('.search-worldwide-holder .worldwide').on('click', function () {
		$('.worldwide-holder').toggleClass('active'); return false;
	});
		$('.worldwide-holder .worldwide-close').on('click', function () {
			$('.worldwide-holder').toggleClass('active'); return false;
		});
	$('.worldwide-holder .worldwide-links-holder .title').on('click',function () {
		$(this).toggleClass('active');
        $(this).parent('').toggleClass('active'); return false;
    });
		
// Search
	$('.search-worldwide-holder .search').on('click', function () {
		$('.main-search-holder').toggleClass('active'); return false;
	});
		$('.main-search-holder .main-search-close').on('click', function () {
			$('.main-search-holder').toggleClass('active'); return false;
		});
 // Menu
	$('.menu-item.level-first').hover(function () {
		 if ($(window).width() > 800) {
			$(this).toggleClass('active'); return false;
		 };
	});
	$('.menu-item.level-first').click(function () {
		 if ($(window).width() < 800) {
			$(this).toggleClass('active'); return false;
		 };
	});
 // Mobile Menu
	 $('.menu-mobile-icon').on('click', function () {
        if ($(window).width() < 800) {
            $(".menu-holder").toggle('fast')
            $('.menu-mobile-close').toggleClass('active')
        }
        
    });
    $('.menu-mobile-close').on('click', function () {
        $(this).toggleClass('active');
        $('.menu-holder').toggle(''); return false;
    });

    $(window).resize(function () {
        if ($(window).width() >= 800) {
            if (!($(".menu-holder").is(':visible')))
                $(".menu-holder").css('display', '')
        }
    });
    
// I like to know
	$('.like-to-know-holder').on('click', function () {
		$(this).toggleClass('active'); return false;
	});
	
// Footer Dropdown
	$('.footer-dropdown-button').on('click', function () {
		$(this).toggleClass('active');
		$(this).siblings('.footer-dropdown-holder').toggleClass('active'); return false;
	});
// Accordion Item
	$('.accordion .accordion-item').on('click', function () {
		$(this).toggleClass('active'); return false;
	});
});


jQuery.fn.hint = function (blurClass) {
    if (!blurClass) {
        blurClass = 'blur';
    }

    return this.each(function () {
        // get jQuery version of 'this'
        var $input = jQuery(this),

        // capture the rest of the variable to allow for reuse
      title = $input.attr('title'),
      $form = jQuery(this.form),
      $win = jQuery(window);

        function remove() {
            if ($input.val() === title && $input.hasClass(blurClass)) {
                $input.val('').removeClass(blurClass);
            }
        }

        // only apply logic if the element has the attribute
        if (title) {
            // on blur, set value to title attr if text is blank
            $input.blur(function () {
                if (this.value === '') {
                    $input.val(title).addClass(blurClass);
                }
            }).focus(remove).blur(); // now change all inputs to title

            // clear the pre-defined text when form is submitted
            $form.submit(remove);
            $win.unload(remove); // handles Firefox's autocomplete
        }
    });
};