﻿angular.module('umbraco').controller('Macro.BoxThemePickerMacro',
    function ($scope) {
        $scope.onLoad = function () {
            $scope.themeOptions =
                [{ value: 'yellow', name: 'yellow' }, { value: 'white', name: 'white' }]
           

            if ($scope.model.value == null || $scope.model.value === '') {
                $scope.model.value = 'yellow';
            }

            switch ($scope.model.value) {
                case 'white':
                    $scope.selected = $scope.themeOptions[1]
                    break;
                default:
                    $scope.selected = $scope.themeOptions[0]
                    break;
            }
            
        };

        $scope.onThemeChange = function () {
            $scope.model.value = $scope.selectedItem.value
        }

        $scope.onLoad();
    });