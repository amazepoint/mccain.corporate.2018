﻿angular.module("umbraco").controller("Macro.WorldwideCategoryPickerMacro",
    function ($scope, categoriesResource) {

        $scope.onLoad = function () {

            $scope.allcategories = [];

            // Use taggedResource service to call surface controller
            // Once the data is returned create array of categories names
            categoriesResource.getCategories().then(function (res) {
                var data = res.data;
                if (data) {
                    $scope.allcategories = data;
                }
            });
        };

        $scope.onLoad();




    });

angular.module("umbraco.resources").factory("categoriesResource", function ($http) {
    var categoriesService = {};

    // Service makes an ajax call to 
    // umbraco surface controller and returns the result
    categoriesService.getCategories = function () {
        return $http.get("/umbraco/surface/WorldWide/GetCategories");
    };

    return categoriesService;

});