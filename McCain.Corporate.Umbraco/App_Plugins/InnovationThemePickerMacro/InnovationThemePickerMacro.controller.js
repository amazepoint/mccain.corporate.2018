﻿angular.module("umbraco").controller("Macro.InnovationThemePickerMacro", function ($scope) {

    if ($scope.model.value == null) {
        $scope.model.value = 'yellow';
    }

    $scope.themes = [
        {
            Name: 'yellow'
        },
        {
            Name: 'white'
        },
        {
            Name: 'grey'
        }
    ];

    $scope.handleRadioClick = function (status) {
        // alert($scope.model.value); 
    };
});