﻿angular.module('umbraco').controller('Macro.GenericButtonSettingsMacro',
    function ($scope, $http, $filter) {
        $scope.onLoad = function () {
            $scope.cssClassOptions =
                ['yellow', 'yellow square', 'yellow rounded', 'yellow-border', 'yellow-border square', 'yellow-border rounded', 'black']

            $scope.alignmentOptions =
                ['left', 'center', 'right']

            // init model value
            if (!isObject($scope.model.value))
                $scope.model.value = {}

            function isObject(obj) {
                return obj !== undefined && obj !== null && obj.constructor == Object;
            }
        };

        $scope.onLoad();
    });