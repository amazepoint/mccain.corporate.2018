﻿angular.module("umbraco")
    .controller("Macro.RichTextMacro",
    function ($scope) {
        $scope.textInput = {
            label: 'bodyText',
            description: '...',
            view: 'rte',
            value: $scope.model.value,
            config: {
                editor: {
                    toolbar: ["code", "undo", "redo", "cut", "styleselect", "bold", "italic", "alignleft", "aligncenter", "alignright", "bullist", "numlist", "link", "umbmediapicker", "table", "umbembeddialog"],
                   // toolbar: ["code", "styleselect", "format", "removeformat", "link", "unlink", "bold", "italic", "underline", "strikethrough"],
                    stylesheets: [ "editor" ],
                    dimensions: {
                        height: 200
                    }
                }
            }
        };
        $scope.$watch('textInput.value', function (newValue, oldValue) {
            $scope.model.value = newValue;
        });
    });