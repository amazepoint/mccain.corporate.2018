﻿angular.module('umbraco').controller('Macro.ImageCssPickerMacro',
    function ($scope) {
        $scope.onLoad = function () {
            $scope.cssClasses =
                [{ value: 'full-width', name: 'full-width' }, { value: 'half-width', name: 'half-width' }]


            if ($scope.model.value == null || $scope.model.value === '') {
                $scope.model.value = 'full-width';
            }

            switch ($scope.model.value) {
                case 'half-width':
                    $scope.selected = $scope.cssClasses[1]
                    break;
                default:
                    $scope.selected = $scope.cssClasses[0]
                    break;
            }

        };

        $scope.onClassChange = function () {
            $scope.model.value = $scope.selectedItem.value
        }

        $scope.onLoad();
    });