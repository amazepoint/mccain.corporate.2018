﻿https://our.umbraco.org/forum/using-umbraco-and-getting-started/81150-grid-settings-checkboxlist-prevalueeditor
https://our.umbraco.org/documentation/getting-started/backoffice/property-editors/built-in-property-editors/Grid-Layout/build-your-own-editor
https://our.umbraco.org/documentation/getting-started/backoffice/property-editors/built-in-property-editors/Grid-Layout/Settings-and-styles
https://nicbell.github.io/ucreate/icons.html

{
    "label": "Background repeat",
    "description": "Set how the background should repeat. \"No repeat\" is default.",
    "key": "style$1",
    "view": "/App_Plugins/GridSettings.IconSelectList/icon-select-list.html",
    "applyTo": "row",
    "modifier": "background-repeat:{0};",
    "defaultConfig": {
      "selectMultiple": false,
      "displaySelectInversed": false,
      "items": [
        {
          "icon": "icon-thumbnails-small",
          "text": "Repeat",
          "value": "repeat"
        },
        {
          "icon": "icon-width",
          "text": "Repeat horizontal",
          "value": "repeat-x"
        },
        {
          "icon": "icon-height",
          "text": "Repeat vertically",
          "value": "repeat-y"
        },
        {
          "icon": "icon-stop",
          "text": "No repeat",
          "value": "no-repeat"
        }
      ]
    }
  }