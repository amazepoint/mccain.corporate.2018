﻿angular.module("umbraco").controller("Umbraco.Dashboard.Extended.CustomTools", function ($scope, $log, $http) {
    $scope.contentDataFixesClient = initToolSettings();

    function initToolSettings() {
        return {
            ready: true,
            error: false,
            errorMessage: 'Error occured!',
            responseMessage: ''
        };
    }

    $scope.contentDataFixesClient.fixHeaderBanner = function () {
        var self = this;

        self.ready = false;
        self.errorMessage = "";
        self.error = false;
        self.responseMessage = "";

        $http.post("/umbraco/backoffice/api/ContentDataApi/FixHeaderBanners")
            .then(function (response) {
                if (!response.data.Success) {
                    self.error = true;

                    if (response.data.Errors && response.data.Errors.length > 0) {
                        self.errorMessage = response.data.Errors[0];
                    }
                } else {
                    self.responseMessage = response.data.Data.message;
                }
                self.ready = true;
            }, function (data, status) {
                self.error = true;
                self.ready = true;
            });

    };
});