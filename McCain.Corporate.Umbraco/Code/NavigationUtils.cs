﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace McCain.Corporate.Umbraco
{
    public static class NavigationUtils
    {
        public static IPublishedContent GetNavigationSettingsNode()
        {
            var settingsNode = Settings.GetGeneralSettingsNode();
            return settingsNode.Children
                .FirstOrDefault(x => x.IsDocumentType(NavigationSettings.ModelTypeAlias));
        }
    }
}