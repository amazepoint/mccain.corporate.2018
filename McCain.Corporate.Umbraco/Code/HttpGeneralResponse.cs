﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McCain.Corporate.Umbraco
{
    public class HttpGeneralResponse
    {
        public bool Success { get; set; }

        public object Data { get; set; }

        public List<string> Errors { get; set; }

        public HttpGeneralResponse()
        {
            Errors = new List<string>();
        }
    }
}