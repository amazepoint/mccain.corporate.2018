﻿using McCain.Corporate.Umbraco;
using McCain.Corporate.Umbraco.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using Umbraco.Web;
using Umbraco.Core;
using Umbraco.Web.PublishedContentModels;
using Umbraco.Core.Models;
using Newtonsoft.Json;

namespace McCain.Corporate.Umbraco.Code
{
    public static class KenexaHelper
    {
        private static string GeneralInputXmlFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Code/Kenexa/KenexaRequest.xml");
        private static string InputXmlForHotJobsFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Code/Kenexa/KenexaRequestForHotJobs.xml");
        private static string InputXmlForFilterUrlFilePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Code/Kenexa/KenexaRequestForFilterUrl.xml");

        public static string ApiUrlDefault = "http://import.brassring.com/WebRouter/WebRouter.asmx/route";
        public static string EmployeeIdDefault = "12345";
        public static string ClientIdDefault = "25120";
        public static string SiteIdDefault = "5029";
        public static string SearchUrlDefault = "https://sjobs.brassring.com/TGWebHost/home.aspx?partnerid=25120&siteid=5029";


        static string m_WebServiceURL;
        static string m_EmployeeID;
        static string m_ClientID;
        static string m_SiteID;
        static string m_functionsJSON;


        public static void ReloadSettings()
        {
            var contentService = ApplicationContext.Current.Services.ContentService;

            var generalSettingsNode = contentService.GetRootContent().Where(x => x.ContentType.Alias == GeneralSettings.ModelTypeAlias).FirstOrDefault();
            IContent kenexaSettingsNode = null;

            if (generalSettingsNode != null)
            {
                kenexaSettingsNode = contentService.GetChildren(generalSettingsNode.Id).Where(x => x.ContentType.Alias == KenexaSettings.ModelTypeAlias).FirstOrDefault();
            }

            m_WebServiceURL = kenexaSettingsNode?.GetValueOrDefault("kenexaApiUrl", ApiUrlDefault) ?? ApiUrlDefault;
            m_EmployeeID = kenexaSettingsNode?.GetValueOrDefault("employeeID", EmployeeIdDefault) ?? EmployeeIdDefault;
            m_ClientID = kenexaSettingsNode?.GetValueOrDefault("clientID", ClientIdDefault) ?? ClientIdDefault;
            m_SiteID = kenexaSettingsNode?.GetValueOrDefault("siteID", SiteIdDefault) ?? SiteIdDefault;
            m_functionsJSON = kenexaSettingsNode?.GetValueOrDefault("functions", "") ?? "";

            UpdateTotalJobCount();
            UpdateFeaturedJobs();
            UpdateFunctionFilterUrls();
        }

        #region Jobs Count
        private static void UpdateTotalJobCount()
        {
            int totalJobCount = 0;

            try
            {
                int pagesCount;
                // determine pages count and job count per page
                int jobCount = GetJobsCountForPage(1, out pagesCount);

                if (pagesCount > 1)
                {
                    totalJobCount += jobCount * (pagesCount - 1);
                    totalJobCount += GetJobsCountForPage(pagesCount, out pagesCount);
                }
                else
                {
                    totalJobCount = jobCount;
                }

            }
            catch (Exception ex)
            {
                ApplicationContext.Current.ProfilingLogger.Logger.Error(typeof(KenexaHelper), "Error while Kenexa Jobs count reloading", ex);
                totalJobCount = 0;
            }

            ApplicationContext.Current.ApplicationCache.RuntimeCache.InsertCacheItem(BusinessContext.C_JobsCountCacheName, () => totalJobCount, TimeSpan.FromMinutes(5));
        }

        private static int GetJobsCountForPage(int pageNum, out int totalPagesCount)
        {
            string inputXmlFormat = System.IO.File.ReadAllText(GeneralInputXmlFilePath);
            int jobsCount = 0;
            totalPagesCount = 0;

            try
            {
                using (var webClient = new WebClient())
                {
                    var inputXml = String.Format(inputXmlFormat, m_EmployeeID, m_ClientID, m_SiteID, pageNum);
                    NameValueCollection postValues = new NameValueCollection();
                    postValues.Add("inputXml", inputXml);
                    byte[] responseArray = webClient.UploadValues(m_WebServiceURL, "POST", postValues);

                    string xmlResponse = CleanupXml(responseArray);

                    using (TextReader textReader = new StringReader(xmlResponse))
                    {
                        using (XmlTextReader xmlReader = new XmlTextReader(textReader))
                        {
                            while (xmlReader.Read())
                            {
                                if (xmlReader.Name == "Job" && xmlReader.IsStartElement())
                                    jobsCount++;
                                else if (xmlReader.Name == "MaxPages")
                                {
                                    if (xmlReader.Read())
                                    {
                                        totalPagesCount = xmlReader.ReadContentAsInt();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationContext.Current.ProfilingLogger.Logger.Error(typeof(KenexaHelper), "Error while Kenexa Jobs counting for request page " + pageNum, ex);
            }
            return jobsCount;
        }
        #endregion

        #region Featured Jobs
        private static void UpdateFeaturedJobs(int count = 3)
        {
            string inputXmlFormat = System.IO.File.ReadAllText(InputXmlForHotJobsFilePath);
            List<JobModel> _featuredJobsCached = new List<JobModel>();
            try
            {
                using (var webClient = new WebClient())
                {
                    var inputXml = String.Format(inputXmlFormat, m_EmployeeID, m_ClientID, m_SiteID, 1);
                    NameValueCollection postValues = new NameValueCollection();
                    postValues.Add("inputXml", inputXml);
                    byte[] responseArray = webClient.UploadValues(m_WebServiceURL, "POST", postValues);

                    string xmlResponse = CleanupXml(responseArray);

                    using (TextReader textReader = new StringReader(xmlResponse))
                    {
                        using (XmlReader xReader = XmlReader.Create(textReader))
                        {
                            XElement xdoc = XElement.Load(xReader);
                            var jobs = (from el in xdoc.Descendants("Job") select el).ToList();
                            count = (jobs != null && jobs.Count() < count) ? jobs.Count() : count;

                            for (int i = 0; i < count; i++)
                            {
                                JobModel job = new JobModel();
                                job.Title = (from el in jobs[i].Elements("Question") where (string)el.Attribute("Id") == "6923" select el.Value).FirstOrDefault();
                                job.Function = (from el in jobs[i].Elements("Question") where (string)el.Attribute("Id") == "39120" select el.Value).FirstOrDefault();
                                job.Company = (from el in jobs[i].Elements("Question") where (string)el.Attribute("Id") == "6903" select el.Value).FirstOrDefault();
                                job.Country = (from el in jobs[i].Elements("Question") where (string)el.Attribute("Id") == "6910" select el.Value).FirstOrDefault();
                                job.State = (from el in jobs[i].Elements("Question") where (string)el.Attribute("Id") == "4693" select el.Value).FirstOrDefault();
                                job.City = (from el in jobs[i].Elements("Question") where (string)el.Attribute("Id") == "4389" select el.Value).FirstOrDefault();
                                job.Description = jobs[i].Element("JobDescription").Value.StripHtml();
                                job.DetailLink = jobs[i].Element("JobDetailLink").Value;
                                job.LastUpdated = jobs[i].Element("LastUpdated").Value;

                                _featuredJobsCached.Add(job);
                            }
                        }
                    }

                    inputXmlFormat = System.IO.File.ReadAllText(InputXmlForFilterUrlFilePath);
                    const string criteriaTemplate = "<Question Sortorder=\"ASC\" Sort=\"No\"><Id>{0}</Id><Value> <![CDATA[{1}]]></Value></Question>";

                    // link search by function, country, city/state
                    foreach (var job in _featuredJobsCached)
                    {
                        string countryCriteria = String.Format(criteriaTemplate, "6910", job.Country);
                        string functionCriteria = String.Format(criteriaTemplate, "39120", job.Function);

                        job.CountryFilterLink = GetFilterUrl(webClient, inputXmlFormat, countryCriteria);
                        job.FunctionFilterLink = GetFilterUrl(webClient, inputXmlFormat, functionCriteria);
                    }
                }

            }
            catch (Exception ex)
            {
                ApplicationContext.Current.ProfilingLogger.Logger.Error(typeof(KenexaHelper), "Error while Kenexa Featured Jobs list reloading", ex);
            }

            ApplicationContext.Current.ApplicationCache.RuntimeCache.InsertCacheItem(BusinessContext.C_FeaturedJobListCacheName, () => _featuredJobsCached, TimeSpan.FromMinutes(5));
            ApplicationContext.Current.ApplicationCache.RuntimeCache.InsertCacheItem(BusinessContext.C_FeaturedJobCacheName, () => _featuredJobsCached.FirstOrDefault(), TimeSpan.FromMinutes(5));
        }
        #endregion

        private static void UpdateFunctionFilterUrls()
        {
            if (String.IsNullOrEmpty(m_functionsJSON))
                return;

            try
            {
                var inputXmlFormat = System.IO.File.ReadAllText(InputXmlForFilterUrlFilePath);
                const string criteriaTemplate = "<Question Sortorder=\"ASC\" Sort=\"No\"><Id>{0}</Id><Value> <![CDATA[{1}]]></Value></Question>";

                var functionsDict = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(m_functionsJSON);

                var dictionary = new Dictionary<string, string>();

                using (var webClient = new WebClient())
                {
                    foreach (var item in functionsDict)
                    {
                        var key = item.Key;
                        var functions = item.Value;

                        string functionCriteria = String.Format(criteriaTemplate, "39120", String.Join(", ", functions.ToArray()));

                        functionCriteria = String.Format(criteriaTemplate, "6923", "Graduate, Internship, Apprenticeship");

                        string functionFilterLink = GetFilterUrl(webClient, inputXmlFormat, functionCriteria);

                        dictionary.Add(key, functionFilterLink);
                    }

                    ApplicationContext.Current.ApplicationCache.RuntimeCache.InsertCacheItem(BusinessContext.C_SearchJobFiltersCacheName, () => dictionary, TimeSpan.FromMinutes(5));
                }
            }
            catch (Exception ex)
            {
                ApplicationContext.Current.ProfilingLogger.Logger.Error(typeof(KenexaHelper), "Error in UpdateFunctionFilterUrls method", ex);
            }
        }

        private static string GetFilterUrl(WebClient webClient, string inputXmlFormat, string criteria)
        {
            using (var webClient1 = new WebClient())
            {
                string result = String.Empty;

                string inputXml = String.Format(inputXmlFormat, m_EmployeeID, m_ClientID, m_SiteID, criteria);
                var postValues = new NameValueCollection();
                postValues.Add("inputXml", inputXml);
                var responseArray = webClient1.UploadValues(m_WebServiceURL, "POST", postValues);
                string xmlResponse = CleanupXml(responseArray);

                using (TextReader textReader = new StringReader(xmlResponse))
                {
                    using (XmlReader xReader = XmlReader.Create(textReader))
                    {
                        XElement xdoc = XElement.Load(xReader);

                        var resultSet = (from el in xdoc.Descendants("ResultSet") select el).ToList();
                        if (resultSet.Count() > 0)
                            result = resultSet[0].Elements("Link").FirstOrDefault().Value;
                    }
                }

                return result;
            }

        }

        private static string CleanupXml(byte[] responseArray)
        {
            string xmlResponse = String.Empty;

            //get rid of the 'string' tag
            using (Stream stream = new MemoryStream(responseArray))
            {
                using (XmlTextReader xmlReader = new XmlTextReader(stream))
                {
                    while (xmlReader.Read())
                    {
                        if (xmlReader.Name == "string")
                        {
                            if (xmlReader.Read())
                            {
                                xmlResponse = xmlReader.Value;
                                break;
                            }
                        }
                    }
                }
            }
            return xmlResponse;
        }
    }
}

