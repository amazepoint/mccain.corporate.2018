﻿using Examine;
using McCain.Corporate.Umbraco.Code;
using Skybrud.Umbraco.GridData;
using Skybrud.Umbraco.GridData.Dtge;
using Skybrud.Umbraco.GridData.Values;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Configuration;
using Umbraco.Core.Events;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.PropertyEditors;
using Umbraco.Core.Services;
using Umbraco.Forms.Mvc;
using Umbraco.Forms.Web.Controllers;
using Umbraco.Web;
using Umbraco.Web.Models.ContentEditing;
using Umbraco.Web.PublishedContentModels;
using Umbraco.Web.Routing;
using Umbraco.Web.Scheduling;
using UmbracoExamine;

namespace McCain.Corporate.Umbraco
{
    public class GlobalApplicationEventHandler : ApplicationEventHandler
    {
        
        
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication,
            ApplicationContext applicationContext)
        {
            base.ApplicationStarting(umbracoApplication, applicationContext);
            base.ApplicationStarting(umbracoApplication, applicationContext);
        }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Created += ContentService_Created;
            ContentService.Saving += ContentService_Saving;
            ContentService.Saved += ContentService_Saved;
            base.ApplicationStarted(umbracoApplication, applicationContext);

            // Routing
            WebApiConfig.Register(GlobalConfiguration.Configuration);

        }
        

        private void ContentService_Created(IContentService sender, NewEventArgs<IContent> e)
        {
            switch (e.Entity.ContentType.Alias.ToString())
            {
                case StoryPageDocumentType.ModelTypeAlias:
                    e.Entity.SetValue("pageContent", "");
                    break;
                case NewsArticleDocumentType.ModelTypeAlias:
                    e.Entity.SetValue("articleDate", DateTime.Today);
                    break;
            }

        }

        private void ContentService_Saving(IContentService contentService, SaveEventArgs<IContent> eventArgs)
        {

        }

        private void ContentService_Saved(IContentService contentService, SaveEventArgs<IContent> eventArgs)
        {

        }

        
    }
}