﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace McCain.Corporate.Umbraco
{
    public static class IEnumerableExtension
    {
        // divide into n parts, last part will also include elements remained after division by n
        public static IEnumerable<IEnumerable<T>> FloorPartition<T>(this IEnumerable<T> source, int n)
        {
            var count = source.Count();

            var groupped = source.Select((x, i) => new { value = x, index = i })
                .GroupBy(x => x.index / (int)Math.Floor(count / (double)n))
                .Select(x => x.Select(z => z.value));

            if (groupped.Count() > n)
            {
                // make n groups
                var length = groupped.Count();
                var grouppedList = groupped.ToList();

                grouppedList[length - 2] = groupped.ElementAt(length - 2).Concat(groupped.ElementAt(length - 1));
                grouppedList.RemoveAt(length - 1);

                return grouppedList;
            }

            return groupped;
        }

        // divide into n parts with ceiling
        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> source, int n)
        {
            var count = source.Count();

            return source.Select((x, i) => new { value = x, index = i })
                .GroupBy(x => x.index / (int)Math.Ceiling(count / (double)n))
                .Select(x => x.Select(z => z.value));
        }


        // nSize - is a max elements count of sublists
        public static IEnumerable<IEnumerable<T>> Split<T>(this IEnumerable<T> source, int nSize = 30)
        {
            for (int i = 0; i < source.Count(); i += nSize)
            {
                yield return source.ToList().GetRange(i, Math.Min(nSize, source.Count() - i));
            }
        }
    }
}