﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace McCain.Corporate.Umbraco
{
    public static class StringExtension
    {
        // if no such parameter in a query: creates query string or adds parameter with value to query string
        // else: replaces existing value with new one
        public static string ReplaceQueryStringValue(this string url, string parameter, string value)
        {
            string str;
            bool flag = url.Contains("?");
            if (flag)
            {
                flag = url.Contains(string.Concat(parameter, "="));
                str = (flag ? Regex.Replace(url, string.Concat("([?&]", parameter, ")=[^?&]+"), string.Concat("$1=", value)) : string.Format("{0}&{1}={2}", url, parameter, value));
            }
            else
            {
                str = string.Format("{0}?{1}={2}", url, parameter, value);
            }
            return str;
        }

        public static string TruncateAtWord(this string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf(" ", length, StringComparison.Ordinal);
            return string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }

        public static string ToAbsoluteUrl(this string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("http://", StringComparison.OrdinalIgnoreCase)
           || relativeUrl.StartsWith("https://", StringComparison.OrdinalIgnoreCase))
            {
                return relativeUrl;
            }

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        public static string ToPartialViewPath(this string path)
        {
            if (string.IsNullOrEmpty(path))
                return path;

            if (path.StartsWith("/"))
                path = path.Insert(0, "~");
            if (!path.StartsWith("~/"))
                path = path.Insert(0, "~/");

            return path;
        }

        public static string CapitalizeFirstLetter(this string s)
        {
            if (String.IsNullOrEmpty(s)) return s;
            if (s.Length == 1) return s.ToUpper();
            return s.Remove(1).ToUpper() + s.Substring(1);
        }
    }
}