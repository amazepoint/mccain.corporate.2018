﻿using System;
using Umbraco.Web;
using Umbraco.Core.Models;
using System.Net.Http;

namespace McCain.Corporate.Umbraco
{
    public static class UmbracoContentExsions
    {
        public static T GetPropertyValueOrDefault<T>(this IPublishedContent content, string propertyName, T defaultValue)
        {
            T value;
            try
            {
                value = content.GetPropertyValue<T>(propertyName);
                if (value == null || typeof(T).FullName == "System.String" && String.IsNullOrEmpty(value.ToString()))
                {
                    value = defaultValue;
                }
            }
            catch (Exception ex)
            {
                value = defaultValue;
            }
            return value;
        }

        public static T GetValueOrDefault<T>(this IContent content, string propertyName, T defaultValue)
        {
            T value;
            try
            {
                value = content.GetValue<T>(propertyName);
                if (value == null || typeof(T).FullName == "System.String" && String.IsNullOrEmpty(value.ToString()))
                {
                    value = defaultValue;
                }
            }
            catch (Exception ex)
            {
                value = defaultValue;
            }
            return value;
        }
    }

    public static class HttpRequestMessageExtensions
    {
        private const string HttpContext = "MS_HttpContext";
        private const string RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";

        public static string GetClientIpAddress(this HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey(HttpContext))
            {
                dynamic ctx = request.Properties[HttpContext];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }

            if (request.Properties.ContainsKey(RemoteEndpointMessage))
            {
                dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }

            return null;
        }
    }
}
