﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;

namespace McCain.Corporate.Umbraco
{
   
        public class VideoUtils
        {
            #region Video Utils

            public static string GetEmbeddedVideoUrl(string videoUrl)
            {
                string result = String.Empty;

                if (!String.IsNullOrEmpty(videoUrl))
                {
                    if (IsFileVimeoVideo(videoUrl))
                        result = "https://player.vimeo.com/video/" + GetVideoId(videoUrl);
                    else if (IsFileYoutubeVideo(videoUrl))
                        result = "http://www.youtube.com/embed/" + GetVideoId(videoUrl);
                }

                return result;
            }


            public static string GetVideoId(string videoUrl)
            {
                Regex youtubeVideoRegex = new Regex(YoutubeVideoRegexString, RegexOptions.IgnoreCase);
                Match youtubeMatch = youtubeVideoRegex.Match(videoUrl);
                if (youtubeMatch.Success)
                    return youtubeMatch.Groups[5].Value;

                Regex vimeoVideoRegex = new Regex(VimeoVideoRegexString, RegexOptions.IgnoreCase | RegexOptions.Multiline);
                Match vimeoMatch = vimeoVideoRegex.Match(videoUrl);
                if (vimeoMatch.Success)
                    return vimeoMatch.Groups[5].Value;

                return String.Empty;
            }

            public static bool IsFileYoutubeVideo(string fileUrl)
            {
                Regex youtubeVideoRegex = new Regex(YoutubeVideoRegexString, RegexOptions.IgnoreCase);

                Match youtubeMatch = youtubeVideoRegex.Match(fileUrl);
                if (youtubeMatch.Success)
                    return true;

                return false;
            }

            public static bool IsFileVimeoVideo(string fileUrl)
            {
                Regex vimeoVideoRegex = new Regex(VimeoVideoRegexString, RegexOptions.IgnoreCase | RegexOptions.Multiline);

                Match vimeoMatch = vimeoVideoRegex.Match(fileUrl);
                if (vimeoMatch.Success)
                    return true;

                return false;
            }

        
        public const string VimeoVideoRegexString = @"^(http:|https:|)\/\/(player.|www.)?(vimeo\.com)\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?";
        public const string YoutubeVideoRegexString = @"^(http:|https:|)\/\/(player.|www.)?(youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?";

        #endregion


        
    }

}