﻿using Examine;
using McCain.Corporate.Umbraco.Models;
using Examine.SearchCriteria;
using Umbraco.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Examine.LuceneEngine;
using System.Web;
using Examine.LuceneEngine.SearchCriteria;

namespace McCain.Corporate.Umbraco
{
    /// <summary>
    /// A helper class giving you everything you need for searching with Examine.
    /// </summary>
    public class SearchHelper
    {
        public static SearchResultsModel Search()
        {
            var criteria = new SearchCriteria()
            {
                SearchTerm = HttpContext.Current.Request.QueryString["s"] ?? "",
                FieldPropertyAliases = "pageTitle,pageContent"
            };

            int pageIndex = 0;
            int.TryParse(HttpContext.Current.Request.QueryString["page"], out pageIndex);
            if (pageIndex == 0)
                pageIndex = 1;

            var paging = new PagingModel()
            {
                PageIndex = pageIndex,
                PageSize = 10
            };

            var searchResults = SearchHelper.SearchUsingExamine(criteria, paging);

            var model = new SearchResultsModel(criteria.SearchTerm, searchResults, paging);

            return model;
        }

        public static ISearchResults SearchUsingExamine(SearchCriteria criteria, PagingModel paging)
        {
            string[] docTypeAliases = null;

            if (criteria.DocTypeAliases != null)
            {
                docTypeAliases = criteria.DocTypeAliases.Split(',');
            }

            string[] orderByFields = null;
            if (!String.IsNullOrEmpty(criteria.OrderByFieldPropertyAliases))
            {
                orderByFields = criteria.OrderByFieldPropertyAliases.Split(',');
            }

            return SearchUsingExamine(docTypeAliases, criteria.SearchGroups, paging.PageIndex * paging.PageSize, orderByFields, criteria.OrderDescending);
        }

        /// <summary>
        /// Performs a lucene search using Examine
        /// </summary>
        private static ISearchResults SearchUsingExamine(string[] documentTypes, List<SearchGroup> searchGroups, int maxResults, string[] orderByFields = null, bool orderDescending = true)
        {
            var searcher = ExamineManager.Instance.DefaultSearchProvider;
            ISearchCriteria searchCriteria = searcher.CreateSearchCriteria();
            IBooleanOperation query = null;

            //only shows results for visible documents
            query = searchCriteria.GroupedOr(new string[] { "umbracoNaviHide" }, "0", String.Empty);

            if (documentTypes != null && documentTypes.Length > 0)
            {
                query = query.And().GroupedOr(new string[] { "nodeTypeAlias" }, documentTypes);
            }

            if (searchGroups != null && searchGroups.Any())
            {
                //in each search group it looks for a match where the specified fields contain any of the specified search terms
                //usually would only have 1 search group, unless you want to filter out further, i.e. using categories as well as search terms
                foreach (SearchGroup searchGroup in searchGroups)
                {
                    //query = query.And().GroupedOr(searchGroup.FieldsToSearchIn, searchGroup.SearchTerms);
                    var values = new List<IExamineValue>();
                    foreach (string term in searchGroup.SearchTerms)
                    {
                        values.Add(term.Fuzzy(0.9F));
                    }

                    if (values.Any())
                    {
                        var array = values.ToArray();

                        query = query.And().GroupedOr(searchGroup.FieldsToSearchIn, array);
                    }
                }
            }

            // apply ordering settings 
            if (orderByFields != null && orderByFields.Length > 0)
            {
                if (orderDescending)
                {
                    query = query.And().OrderByDescending(orderByFields);
                }
                else
                {
                    query = query.And().OrderBy(orderByFields);
                }
            }

            //return the results of the search
            return searcher.Search(query.Compile(), maxResults);
        }

        /// <summary>
        /// Takes the examine search results and return the content for a specific search results page
        /// </summary>
        public static IEnumerable<IPublishedContent> GetResultsForPage(ISearchResults searchResults, PagingModel paging)
        {
            var uHelper = new UmbracoHelper(UmbracoContext.Current);
            return searchResults.Skip((paging.PageIndex - 1) * paging.PageSize).Take(paging.PageSize).Select(x => uHelper.TypedContent(x.Id));
        }

    }
}