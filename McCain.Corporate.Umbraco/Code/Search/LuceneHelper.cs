﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Examine;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Highlight;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using System.IO;
using Umbraco.Core.Logging;

namespace McCain.Corporate.Umbraco
{
    /// <summary>
    /// Summary description for LuceneHelper
    /// </summary>
    public class LuceneHelper
    {
        private readonly Lucene.Net.Util.Version _luceneVersion = Lucene.Net.Util.Version.LUCENE_29;

        protected Dictionary<string, QueryParser> QueryParsers = new Dictionary<string, QueryParser>();

        public string Separator { get; set; }
        public int MaxNumHighlights { get; set; }
        public Formatter HighlightFormatter { get; set; }
        public Analyzer HighlightAnalyzer { get; set; }

        private static readonly LuceneHelper instance = new LuceneHelper();

        public static LuceneHelper Instance
        {
            get { return instance; }
        }

        private LuceneHelper()
        {
            Separator = "...";
            MaxNumHighlights = 5;
            HighlightAnalyzer = new StandardAnalyzer(_luceneVersion);
            HighlightFormatter = new SimpleHTMLFormatter("<span class='highlight'>", "</span>");
        }

        public string GetHighlight(string value, string highlightField, Searcher searcher, string luceneRawQuery)
        {
            string result = String.Empty;

            try
            {
                var query = GetQueryParser(highlightField).Parse(luceneRawQuery);
                var scorer = new QueryScorer(searcher.Rewrite(query));

                var fragmenter = new SimpleFragmenter(600);

                var highlighter = new Highlighter(HighlightFormatter, scorer);
                highlighter.SetTextFragmenter(fragmenter);

                var tokenStream = HighlightAnalyzer.TokenStream(highlightField, new StringReader(value));
                string bestFragments = highlighter.GetBestFragments(tokenStream, value, MaxNumHighlights, Separator);
                result = bestFragments;
            }
            catch (Exception ex)
            {
                LogHelper.WarnWithException<LuceneHelper>($"GetHighlight failed for {highlightField}", ex);
            }

            return result;
        }


        private Query GetLuceneQueryObject(string q, string field)
        {

            var qt = new QueryParser(_luceneVersion, field, HighlightAnalyzer);
            qt.SetMultiTermRewriteMethod(MultiTermQuery.SCORING_BOOLEAN_QUERY_REWRITE);
            return qt.Parse(q);

        }

        protected QueryParser GetQueryParser(string highlightField)
        {
            if (!QueryParsers.ContainsKey(highlightField))
            {
                QueryParsers[highlightField] = new QueryParser(_luceneVersion, highlightField, HighlightAnalyzer);
            }
            return QueryParsers[highlightField];
        }
    }
}