﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McCain.Corporate.Umbraco
{
    // defines fields and terms for search condition
    public class SearchGroup
    {
        public string[] FieldsToSearchIn { get; set; }
        public string[] SearchTerms { get; set; }

        public SearchGroup(string[] fieldsToSearchIn, string[] searchTerms)
        {
            FieldsToSearchIn = fieldsToSearchIn;
            SearchTerms = searchTerms;
        }
    }
}