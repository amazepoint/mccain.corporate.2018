﻿using System.Collections.Generic;
using McCain.Corporate.Umbraco.Models;
using System;
using Lucene.Net.QueryParsers;
using System.Linq;

namespace McCain.Corporate.Umbraco
{
    public class SearchCriteria
    {
        // comma separated document types aliases
        public string DocTypeAliases { get; set; }

        // string of words separated by spaces
        public string SearchTerm { get; set; }

        // comma separated fields aliases
        public string FieldPropertyAliases { get; set; }

        // comma separated fields aliases
        public string OrderByFieldPropertyAliases { get; set; }

        public bool OrderDescending { get; set; }

        // built from defined fields and search terms
        public List<SearchGroup> SearchGroups { get { return GetSearchGroups(); } }

        private List<SearchGroup> GetSearchGroups()
        {
            List<SearchGroup> searchGroups = null;
            if (!string.IsNullOrEmpty(FieldPropertyAliases) && !string.IsNullOrEmpty(SearchTerm))
            {
                //searchGroups = new List<SearchGroup>();
                //searchGroups.Add(new SearchGroup(FieldPropertyAliases.Split(','), SearchTerm.Split(' ')));

                string[] searchTerms = SearchTerm.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                   .Select(QueryParser.Escape).ToArray();

                searchGroups = new List<SearchGroup>();
                searchGroups.Add(new SearchGroup(FieldPropertyAliases.Split(','),
                    searchTerms));
            }
            return searchGroups;
        }
    }
}