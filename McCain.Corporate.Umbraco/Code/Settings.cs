﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;

namespace McCain.Corporate.Umbraco
{
    public static class Settings
    {
        public static IPublishedContent GetGeneralSettingsNode()
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            return GetGeneralSettingsNode(umbracoHelper);
        }

        public static IPublishedContent GetHomePageNode(UmbracoHelper umbracoHelper)
        {
            IEnumerable<IPublishedContent> rootNodes = umbracoHelper.TypedContentAtRoot();
            return rootNodes.FirstOrDefault(x => x.IsDocumentType(HomeDocumentType.ModelTypeAlias));
        }

        public static IPublishedContent GetGeneralSettingsNode(UmbracoHelper umbracoHelper)
        {
            IEnumerable<IPublishedContent> rootNodes = umbracoHelper.TypedContentAtRoot();
            return rootNodes.FirstOrDefault(x => x.IsDocumentType(GeneralSettings.ModelTypeAlias));
        }

        public static IPublishedContent GetListsContentNode()
        {
            var settingsNode = GetGeneralSettingsNode();
            return settingsNode.Children.FirstOrDefault(x => x.IsDocumentType(ListsContent.ModelTypeAlias));
        }

        public static IPublishedContent GetKenexaSettingsNode()
        {
            var settingsNode = GetGeneralSettingsNode();
            return settingsNode.Children.FirstOrDefault(x => x.IsDocumentType(KenexaSettings.ModelTypeAlias));
        }

        public static SalesForceSettings GetSalesForceSettingsNode()
        {
            var settingsNode = GetGeneralSettingsNode();
            return new SalesForceSettings(settingsNode.Children.FirstOrDefault(x => x.IsDocumentType(SalesForceSettings.ModelTypeAlias)));
        }

        public static SalesForceProfileItem GetSalesForceProfile(SalesForceSettings settings, string name)
        {
            IPublishedContent content = null;

            if (!String.IsNullOrEmpty(name))
            {
                content = settings.Children.FirstOrDefault(i => i.Name.ToUpper() == name.ToUpper());
            }
            else
            {
                content = settings.DefaultApiProfile;
            }

            if (content != null)
                return new SalesForceProfileItem(content);
            else
                return null;
        }
    }
}