﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McCain.Corporate.Umbraco
{
    public class GenericButtonSettings
    {
        public string title { get; set; }
        public string url { get; set; }
        public bool openLinkInNewWindow { get; set; }
        public string cssClass { get; set; }

        public string alignment { get; set; }
    }
}