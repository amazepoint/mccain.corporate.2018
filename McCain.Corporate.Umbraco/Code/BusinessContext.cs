﻿using McCain.Corporate.Umbraco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web.PublishedContentModels;
using Umbraco.Web;
using McCain.Corporate.Umbraco.Code;
using Umbraco.Core;
using System.Web.Mvc;
using System.IO;

namespace McCain.Corporate.Umbraco
{
    public static class BusinessContext
    {
        public const string C_JobsSearchUrl = "https://sjobs.brassring.com/TGWebHost/searchopenings.aspx?partnerid=25120&siteid=5029";
        public const string C_FeaturedJobListCacheName = "featuredJobs";
        public const string C_FeaturedJobCacheName = "featuredJob";
        public const string C_JobsCountCacheName = "jobsCount";
        public const string C_SearchJobFiltersCacheName = "searchJobFilters";

        #region Media Utils

        public static string GetImageAltTextOrDefault(IPublishedContent mediaItem, string defaultText)
        {
            var altText = String.Empty;
            if (mediaItem != null)
            {
               altText = mediaItem.GetPropertyValue<string>("altText");

                if (String.IsNullOrWhiteSpace(altText))
                {
                    altText = Path.GetFileNameWithoutExtension(mediaItem.Name);
                    altText = altText.Replace("-", " ").CapitalizeFirstLetter();
                }
            }

            if (String.IsNullOrWhiteSpace(altText))
                altText = defaultText.StripHtml();

            return altText;
        }

        #endregion

        #region Global

        public static string GetGridHtmlStripped(MvcHtmlString value)
        {
            string result = value.ToString().StripHtml();
            result = result.Replace("-->", String.Empty); // fix html comments
            return result;
        }

        public static string GetHeaderScripts()
        {
            return new GeneralSettings(Settings.GetGeneralSettingsNode()).HeaderScripts;
        }

        public static string GetBodyScripts()
        {
            return new GeneralSettings(Settings.GetGeneralSettingsNode()).BodyStartScripts;
        }

        public static string GetFooterScripts()
        {
            return new GeneralSettings(Settings.GetGeneralSettingsNode()).BodyEndScripts;
        }

        #endregion

        #region Page Layout Utils

        public static string GetWrapperCssClass(string documentTypeAlias)
        {
            string result = "generic-page";

            switch (documentTypeAlias)
            {
                case HomeDocumentType.ModelTypeAlias:
                    result = "home-page";
                    break;
                case StoryPageDocumentType.ModelTypeAlias:
                    result = "story-page";
                    break;
                case PrivacyPageDocumentType.ModelTypeAlias:
                    result = "privacy-page";
                    break;
                case NewsArticleDocumentType.ModelTypeAlias:
                    result = "news-article-page";
                    break;
                case NewsLandingDocumentType.ModelTypeAlias:
                    result = "news-landing-page";
                    break;
                case FunctionPageDocumentType.ModelTypeAlias:
                    result = "function-page";
                    break;
                case FunctionsLandingPageDocumentType.ModelTypeAlias:
                    result = "functions-landing-page";
                    break;
                case SearchResultsDocumentType.ModelTypeAlias:
                    result = "search-results-page";
                    break;
            }

            return result;
        }

        #endregion

        #region Video Utils

        const string YoutubeVideoReplacePattern = @"(?:https?:\/\/)?(?:www\.)?(?:(?:(?:youtube.com\/watch\?[^?]*v=|youtu.be\/)([\w\-]+))(?:[^\s?]+)?)";
        const string YoutubeVideoSearchPattern = @"(?:https?:\/\/)?(?:www\.)?(youtube.com|youtu.be)([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?";

        public static string SearchYoutubeURL(string content)
        {
            var regex = new Regex(YoutubeVideoSearchPattern, RegexOptions.Multiline);
            var matches = regex.Matches(content);

            if (matches.Count > 0)
                return matches[0].Value;
            else
                return String.Empty;
        }

        public static string GetYoutubeEmbedUrl(string videoURL)
        {
            const string replacement = "https://www.youtube.com/embed/$1";

            var rgx = new Regex(YoutubeVideoReplacePattern);

            return rgx.Replace(videoURL, replacement);
        }

        #endregion

        #region Careers
        public static IEnumerable<JobModel> GetFeaturedJobsFromCache()
        {
            return (IEnumerable<JobModel>)ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem(BusinessContext.C_FeaturedJobListCacheName);
        }

        public static JobModel GetFeaturedJobFromCache()
        {
            return (JobModel)ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem(BusinessContext.C_FeaturedJobCacheName);
        }

        public static int GetCurrentJobsCountFromCache()
        {
            return (int)ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem(BusinessContext.C_JobsCountCacheName);
        }

        public static string GetSearchJobsFilterFromCache(string function)
        {
            string result = String.Empty;

            var urlsDictionary = (Dictionary<string, string>)ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem(BusinessContext.C_SearchJobFiltersCacheName);
            if (urlsDictionary != null)
            {
                if (urlsDictionary.ContainsKey(function))
                    result = urlsDictionary[function];
            }

            if (String.IsNullOrEmpty(result))
                result = GetSearchJobsUrl();

            return result;
        }

        public static string GetSearchJobsUrl()
        {
            return new KenexaSettings(Settings.GetKenexaSettingsNode())?.GetPropertyValueOrDefault("searchUrl", KenexaHelper.SearchUrlDefault)
                ?? KenexaHelper.SearchUrlDefault;
        }
        #endregion

    }
}