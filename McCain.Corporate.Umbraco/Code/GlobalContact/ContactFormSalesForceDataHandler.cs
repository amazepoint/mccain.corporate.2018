﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using McCain.Integration.SalesForce;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using Umbraco.Web.PublishedContentModels;

namespace McCain.Corporate.Umbraco
{
    class ContactFormSalesForceDataHandler
    {
        private SalesForceClient client;

        public ContactFormSalesForceDataHandler(SalesForceProfileItem profile)
        {
            
            client = new SalesForceClient()
            {
                LoginEndpoint = profile.LoginEndpoint,
                Username = profile.Username,
                Password = profile.UserPassword,
                Token = profile.SecurityToken,
                ClientId = profile.ClientId,
                ClientSecret = profile.ClientSecret
            };
        }

        public HttpResponseMessage Send(GlobalContactFormModel model)
        {
            // login
            client.Login();

            List<SRecordType> recordTypes = null;

            // read lookup columns from sales force
            var strRecordsResponse = client.Query("SELECT DeveloperName,Id,IsActive,Name,SobjectType FROM RecordType where SobjectType = 'Case' and IsActive = true");

            try
            {
                var fullResponse = JsonConvert.DeserializeObject<JObject>(strRecordsResponse);

                recordTypes = JsonConvert.DeserializeObject<List<SRecordType>>(fullResponse["records"].ToString());
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot read Case Record Types: " + ex.Message + ". API Response: " + strRecordsResponse);
            }

            //var strCaseSchema = client.Describe("Case");

            // prepare request
            var formData = new Dictionary<string, string>();
            formData.Add("Business_Segment__c", model.Segment);

            if (!String.IsNullOrEmpty(model.ContactChannel))
                formData.Add("Contact_Channel__c", model.ContactChannel);
            else
                formData.Add("Contact_Channel__c", "Webform");

            formData.Add("Origin", "Web");

            if (!String.IsNullOrEmpty(model.ContactChannelDetail))
                formData.Add("Contact_Channel_Detail__c", model.ContactChannelDetail);
            else
                formData.Add("Contact_Channel_Detail__c", "Webform " + model.Segment);

            formData.Add("Customer_Group__c", model.Segment == "Retail" ? "Consumer" : "");
            formData.Add("Case_Region__c", model.Region);
            if (!String.IsNullOrEmpty(model.SubRegion))
                formData.Add("Sub_Region__c", model.SubRegion);
            

            var rt = recordTypes.FirstOrDefault(x => x.DeveloperName == model.Subject);
            if (rt == null)
            {
                throw new ArgumentException("Cannot resolve RecordTypeId by Subject");
            }
            else
            {
                formData.Add("RecordTypeId", rt.Id);
            }

            // Contact Details
            AddFieldToDataDictionary(formData, "Contact_Country__c", model.Country);

            AddFieldToDataDictionary(formData, "Province_County__c", FormatProvince(model.Country, model.Province));

            // in SF we have 2 fields for Postal Code, one is for Case, one is for Contact
            // on webform we typycally have 1 Postal Code field that should be used in both fields
            if (!String.IsNullOrEmpty(model.CasePostalCode))
            {
                AddFieldToDataDictionary(formData, "Post_Code__c", model.CasePostalCode);
                AddFieldToDataDictionary(formData, "Contact_Postcode__c", model.PostalCode);
            }
            else
            {

                AddFieldToDataDictionary(formData, "Post_Code__c", model.PostalCode);
                AddFieldToDataDictionary(formData, "Contact_Postcode__c", model.PostalCode);
            }

            AddFieldToDataDictionary(formData, "Contact_Address__c", model.AddressLine1);
            AddFieldToDataDictionary(formData, "Web_Address2__c", model.AddressLine2);
            AddFieldToDataDictionary(formData, "City__c", model.City);

            AddFieldToDataDictionary(formData, "SuppliedCompany", model.BusinessName);
            AddFieldToDataDictionary(formData, "SuppliedName", model.ContactName);

            AddFieldToDataDictionary(formData, "Title__c", FormatTitle(model.Title));
            AddFieldToDataDictionary(formData, "Web_First_Name__c", model.FirstName);
            AddFieldToDataDictionary(formData, "Web_Last_Name__c", model.LastName);
            AddFieldToDataDictionary(formData, "SuppliedPhone", model.PhoneNumber);
            AddFieldToDataDictionary(formData, "Extn__c", model.PhoneNumberExt);
            AddFieldToDataDictionary(formData, "Cell_Phone_Number__c", model.CellphoneNumber);

            AddFieldToDataDictionary(formData, "Web_Contact_Fax__c", model.FaxNumber);
            AddFieldToDataDictionary(formData, "SuppliedEmail", model.EmailAddress);
            AddFieldToDataDictionary(formData, "Description", model.Comments);
            AddFieldToDataDictionary(formData, "Distributor_Name__c", model.DistributorName);
            AddFieldToDataDictionary(formData, "Distributor_Email_Address__c", model.DistributorEmailAddress);


            // Case Details
            AddFieldToDataDictionary(formData, "Customer_Product_Description__c", model.ProductName);
            AddFieldToDataDictionary(formData, "Pack_Code_Free_Text__c", model.BatchID);
            AddFieldToDataDictionary(formData, "Store_of_Purchase_1__c", model.StoreName);
            AddFieldToDataDictionary(formData, "Store_Location__c", model.StoreLocation);

            if (model.ExpiryDate != null)
                AddFieldToDataDictionary(formData, "Best_Before_Date__c", model.ExpiryDate.Value.ToString("yyyy-MM-dd"));
            else if (model.UseByDate != null)
                AddFieldToDataDictionary(formData, "Use_By_Date__c", model.UseByDate.Value.ToString("yyyy-MM-dd"));

            AddFieldToDataDictionary(formData, "Quantity__c", model.QtyOnHand);
            AddFieldToDataDictionary(formData, "Measure__c", model.Uom);
            AddFieldToDataDictionary(formData, "Purchase_Price__c", FormatDecimal(model.PurchasePrice));

            if (model.FirstTimePurchase)
            {
                AddFieldToDataDictionary(formData, "First_Time_Purchase__c", "true");
                AddFieldToDataDictionary(formData, "Buying_Frequency__c", "First Time Buyer");
            }

            AddFieldToDataDictionary(formData, "UPC__c", model.Upc);
            AddFieldToDataDictionary(formData, "Product_Code__c", model.ProductCode);
            AddFieldToDataDictionary(formData, "Name_of_the_Plant__c", model.Plant);
            AddFieldToDataDictionary(formData, "First_Notification_form_Division__c", model.Division);
            
            AddFieldToDataDictionary(formData, "Subject", model.ComplaintCode);

            if (model.FoodSafetyIssue)
                AddFieldToDataDictionary(formData, "Food_Safety_Issue__c", "true");

            AddFieldToDataDictionary(formData, "Special_Instruction__c", model.SpecialInstruction);
            AddFieldToDataDictionary(formData, "Priority", model.Priority);
            AddFieldToDataDictionary(formData, "Language__c", model.Language);
            AddFieldToDataDictionary(formData, "Case_Brand__c", model.CaseBrand);

            AddFieldToDataDictionary(formData, "Invoice_Number__c", model.InvoiceNumber);
            AddFieldToDataDictionary(formData, "Delivery_Number__c", model.DeliveryNumber);
            AddFieldToDataDictionary(formData, "PO_Number__c", model.PurchaseOrderNumber);

            // Person Completing Form
            AddFieldToDataDictionary(formData, "Person_Completing_Form__c", model.PersonCompletingFormName);
            AddFieldToDataDictionary(formData, "Submitted_By_Internal_Webform__c", model.PersonCompletingFormEmail);
            AddFieldToDataDictionary(formData, "First_Notification_form_Type__c", model.PersonCompletingFormType);
            AddFieldToDataDictionary(formData, "Distributor_Name__c", model.BrokerageFirm);
            if (model.PersonCompletingFormNotification)
                AddFieldToDataDictionary(formData, "Notification_to_person_submitting_form__c", "true");

            // Other
            if (model.IsExport)
                AddFieldToDataDictionary(formData, "Export__c", "true");

            AddFieldToDataDictionary(formData, "QA_Consumer_Affairs_Communication__C", model.ConsumerAffairsCommunication);

            AddFieldToDataDictionary(formData, "Preferred_Contact_Mode__c", model.PreferredContactMode);

            // Attachments
            var hasAttachments = model.Attachments != null && model.Attachments.Any(a => !string.IsNullOrEmpty(a.Key) && a.Value != null);
            if (hasAttachments)
                AddFieldToDataDictionary(formData, "Photo_Attached__c", "Yes");

            var postData = new Dictionary<string, object>();

            var dataElements = new List<object>();

            dataElements.Add(
                new
                {
                    method = "POST",
                    url = SalesForceClient.API_ENDPOINT + "sobjects/Case",
                    referenceId = "newCase",
                    body = formData
                });

            if (model.Attachments != null && model.Attachments.Count > 0)
            {
                for (int i = 0; i < model.Attachments.Count; i++)
                {
                    if (!String.IsNullOrEmpty(model.Attachments[i].Key) && model.Attachments[i].Value != null)
                    {
                        string fileName = model.Attachments[i].Key;
                        byte[] fileContent = model.Attachments[i].Value;

                        dataElements.Add(
                                new
                                {
                                    method = "POST",
                                    url = SalesForceClient.API_ENDPOINT + "sobjects/ContentVersion",
                                    referenceId = "contentVersion" + (i + 1),
                                    body = new
                                    {
                                        title = fileName,
                                        pathOnClient = fileName,
                                        versionData = Convert.ToBase64String(fileContent)
                                    }
                                });

                        dataElements.Add(
                                new
                                {
                                    method = "GET",
                                    url = SalesForceClient.API_ENDPOINT + "sobjects/ContentVersion/@{contentVersion" + (i + 1) + ".id}",
                                    referenceId = "contentVersionFields" + (i + 1)
                                });

                        dataElements.Add(
                                new
                                {
                                    method = "POST",
                                    url = SalesForceClient.API_ENDPOINT + "sobjects/ContentDocumentLink",
                                    body = new
                                    {
                                        contentDocumentId = "@{contentVersionFields" + (i + 1) + ".ContentDocumentId}",
                                        linkedEntityId = "@{newCase.id}",
                                        shareType = "V"
                                    },
                                    referenceId = "contentDocument" + (i + 1)
                                });

                    }
                }
            }

            postData.Add("compositeRequest",
                dataElements);

            var apiResult = client.Post("composite", postData);

            return apiResult;
        }

        private void AddFieldToDataDictionary(Dictionary<string, string> dest, string key, string value)
        {
            if (!String.IsNullOrEmpty(value) && !dest.ContainsKey(key))
                dest.Add(key, value);
        }

        private string FormatTitle(string title)
        {
            var result = String.Empty;

            if (!String.IsNullOrEmpty(title))
            {
                var validTitles = new List<string>() { "Dr.", "Mr.", "Ms.", "Miss." };

                if (validTitles.Contains(title))
                {
                    result = title;
                }
                else
                {
                    title += ".";

                    if (validTitles.Contains(title))
                    {
                        result = title;
                    }
                }
            }

            return result;
        }

        private string FormatProvince(string country, string province)
        {
            var result = province;

            Dictionary<string, string> dict = null;

            if (country == "Canada")
            {
                dict = new Dictionary<string, string>()
                {
                    { "AB", "Alberta" },
                    { "BC", "British Columbia" },
                    { "MB", "Manitoba" },
                    { "NB", "New Brunswick" },
                    { "NL", "Newfoundland and Labrador" },
                    { "NT", "Northwest Territories" },
                    { "NS", "Nova Scotia" },
                    { "NU", "Nunavut" },
                    { "ON", "Ontario" },
                    { "PE", "Prince Edward Island" },
                    { "QC", "Quebec" },
                    { "SK", "Saskatchewan" },
                    { "YT", "Yukon" }
                };
            }
            else if (country == "United States")
            {

            }

            if (dict != null)
            {
                string value = String.Empty;
                if (dict.TryGetValue(province, out value))
                {
                    result = value;
                }

            }

            return result;
        }

        private string FormatDecimal(string strValue)
        {
            string result = String.Empty;

            if (!String.IsNullOrEmpty(strValue))
            {
                result = Regex.Replace(strValue, "[^0-9.]", String.Empty);

                decimal value = 0;
                if (!decimal.TryParse(result, out value))
                    result = String.Empty;
            }

            return result;
        }
    }

   
}