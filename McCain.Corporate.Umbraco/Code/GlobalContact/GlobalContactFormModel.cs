﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McCain.Corporate.Umbraco
{
    public class GlobalContactFormModel : ICloneable
    {
        #region General

        public string WebSource { get; set; }

        public string FormType { get; set; }

        public string ContactChannel { get; set; }

        public string ContactChannelDetail { get; set; }

        public string Segment { get; set; }

        public string Region { get; set; }

        public string SubRegion { get; set; }

        public string CasePostalCode { get; set; }

        #endregion

        #region Contact Details

        public string Country { get; set; }

        public string Province { get; set; }

        public string PostalCode { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string BusinessName { get; set; }

        public string Title { get; set; }

        public string ContactName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string PhoneNumberExt { get; set; }

        public string CellphoneNumber { get; set; }

        public string FaxNumber { get; set; }

        public string EmailAddress { get; set; }

        #endregion

        #region Case Details

        public string Subject { get; set; }

        public string Comments { get; set; }

        public string ProductName { get; set; }

        public string BatchID { get; set; }

        public string StoreName { get; set; }

        public string StoreLocation { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public DateTime? ReceivedDate { get; set; }

        public DateTime? UseByDate { get; set; }

        public string QtyOnHand { get; set; }

        public string Uom { get; set; }

        public bool FirstTimePurchase { get; set; }

        public string PurchasePrice { get; set; }

        public string Upc { get; set; }

        public string ProductCode { get; set; }

        public string DistributorName { get; set; }

        public string DistributorEmailAddress { get; set; }

        public string Plant { get; set; }

        public string Division { get; set; }

        public bool FoodSafetyIssue { get; set; }

        public string ComplaintCode { get; set; }

        public string SpecialInstruction { get; set; }

        public string Priority { get; set; }

        public string Language { get; set; }

        public string CaseBrand { get; set; }

        public string InvoiceNumber { get; set; }

        public string DeliveryNumber { get; set; }

        public string PurchaseOrderNumber { get; set; }

        #endregion

        #region Person Completing Form

        public string PersonCompletingFormName { get; set; }

        public string PersonCompletingFormEmail { get; set; }

        public string PersonCompletingFormType { get; set; }

        public string BrokerageFirm { get; set; }

        public bool PersonCompletingFormNotification { get; set; }

        #endregion

        public string ConsumerAffairsCommunication { get; set; }

        public string PreferredContactMode { get; set; }

        public bool IsExport { get; set; }


        #region Attachments

        public List<KeyValuePair<string, byte[]>> Attachments { get; set; }

        #endregion

        public bool IsValid(out List<string> errorMessages)
        {
            bool isValid = true;

            errorMessages = new List<string>();

            if (String.IsNullOrEmpty(Segment))
            {
                errorMessages.Add("Segment is not specified");
            }

            isValid = errorMessages.Count == 0;

            return isValid;
        }

        public object Clone() { return this.MemberwiseClone(); }
    }
}