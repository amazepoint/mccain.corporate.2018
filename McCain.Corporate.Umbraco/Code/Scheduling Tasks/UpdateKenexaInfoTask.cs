﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Web;
using Umbraco.Core;
using Umbraco.Core.Configuration.UmbracoSettings;
using Umbraco.Core.Logging;
using Umbraco.Web.Scheduling;
using System.Web.Hosting;
using System.IO;
using Umbraco.Web.Routing;
using Umbraco.Web.Security;
using Umbraco.Core.Configuration;

namespace McCain.Corporate.Umbraco.Code
{
    public class UpdateKenexaInfoTask : RecurringTaskBase
    {
        public override bool IsAsync
        {
            get
            {
                return true;
            }
        }

        public override bool RunsOnShutdown
        {
            get { return false; }
        }

        public UpdateKenexaInfoTask(IBackgroundTaskRunner<RecurringTaskBase> runner, int delayMilliseconds, int periodMilliseconds)
        : base(runner, delayMilliseconds, periodMilliseconds)
        {
        }

        public override async Task<bool> PerformRunAsync(CancellationToken token)
        {
            using (ApplicationContext.Current.ProfilingLogger.DebugDuration<UpdateKenexaInfoTask>("UpdateKenexaInfoTask is starting its job", "UpdateKenexaInfoTask has finished"))
            {
                try
                {
                    KenexaHelper.ReloadSettings();
                    LogHelper.Info<UpdateKenexaInfoTask>("UpdateKenexaInfoTask has completed");
                }
                catch (Exception e)
                {
                    LogHelper.Error<UpdateKenexaInfoTask>("UpdateKenexaInfoTask failed", e);
                }
            }
            return true; // repeat
        }
    }
}