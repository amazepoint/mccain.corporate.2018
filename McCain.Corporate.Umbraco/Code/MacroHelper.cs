﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Logging;
using Umbraco.Web.Models;

namespace McCain.Corporate.Umbraco
{
    public static class MacroHelper
    {
        public static GenericButtonSettings GetGenericButtonSettings(PartialViewMacroModel model, string paramName)
        {
            GenericButtonSettings settings = null;

            try
            {
                settings = JsonConvert.DeserializeObject<GenericButtonSettings>(HttpUtility.HtmlDecode(model.MacroParameters[paramName].ToString()));
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(GenericButtonSettings), "Failed to parse GenericButtonSettings", ex);
                
            }

            if (settings == null)
                settings = new GenericButtonSettings();

            return settings;
        }
    }
}