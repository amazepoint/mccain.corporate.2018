//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Navigation Settings</summary>
	[PublishedContentModel("navigationSettings")]
	public partial class NavigationSettings : PublishedContentModel
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "navigationSettings";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public NavigationSettings(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<NavigationSettings, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Links
		///</summary>
		[ImplementPropertyType("footerLinks")]
		public Umbraco.Web.Models.RelatedLinks FooterLinks
		{
			get { return this.GetPropertyValue<Umbraco.Web.Models.RelatedLinks>("footerLinks"); }
		}

		///<summary>
		/// Main Menu Links
		///</summary>
		[ImplementPropertyType("mainMenu")]
		public IEnumerable<Cogworks.Meganav.Models.MeganavItem> MainMenu
		{
			get { return this.GetPropertyValue<IEnumerable<Cogworks.Meganav.Models.MeganavItem>>("mainMenu"); }
		}

		///<summary>
		/// Links
		///</summary>
		[ImplementPropertyType("mcCainForLinks")]
		public Umbraco.Web.Models.RelatedLinks McCainForLinks
		{
			get { return this.GetPropertyValue<Umbraco.Web.Models.RelatedLinks>("mcCainForLinks"); }
		}

		///<summary>
		/// Social Links
		///</summary>
		[ImplementPropertyType("socialLinks")]
		public IEnumerable<IPublishedContent> SocialLinks
		{
			get { return this.GetPropertyValue<IEnumerable<IPublishedContent>>("socialLinks"); }
		}

		///<summary>
		/// Links
		///</summary>
		[ImplementPropertyType("worldWideLinks")]
		public IEnumerable<Cogworks.Meganav.Models.MeganavItem> WorldWideLinks
		{
			get { return this.GetPropertyValue<IEnumerable<Cogworks.Meganav.Models.MeganavItem>>("worldWideLinks"); }
		}
	}
}
