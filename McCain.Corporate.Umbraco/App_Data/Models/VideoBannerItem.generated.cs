//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.10.102
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace Umbraco.Web.PublishedContentModels
{
	/// <summary>Video Banner Item</summary>
	[PublishedContentModel("videoBannerItem")]
	public partial class VideoBannerItem : PublishedContentModel, IBaseBannerItem
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "videoBannerItem";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public VideoBannerItem(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<VideoBannerItem, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Banner Image Mobile
		///</summary>
		[ImplementPropertyType("bannerImageMobile")]
		public IPublishedContent BannerImageMobile
		{
			get { return this.GetPropertyValue<IPublishedContent>("bannerImageMobile"); }
		}

		///<summary>
		/// Video Banner
		///</summary>
		[ImplementPropertyType("videoBanner")]
		public IPublishedContent VideoBanner
		{
			get { return this.GetPropertyValue<IPublishedContent>("videoBanner"); }
		}

		///<summary>
		/// Video Poster: Banner image that is displayed while video file is loading
		///</summary>
		[ImplementPropertyType("videoPoster")]
		public IPublishedContent VideoPoster
		{
			get { return this.GetPropertyValue<IPublishedContent>("videoPoster"); }
		}

		///<summary>
		/// Banner Title
		///</summary>
		[ImplementPropertyType("bannerTitle")]
		public string BannerTitle
		{
			get { return Umbraco.Web.PublishedContentModels.BaseBannerItem.GetBannerTitle(this); }
		}

		///<summary>
		/// Css Class: {b}darker{/b} - applies "dark" filter to the banner image{br} {b}black-theme{/b} - black text instead of white{br}{b}shadow-text{/b} - applies "shadow" effect to the banner title and text
		///</summary>
		[ImplementPropertyType("cssClass")]
		public string CssClass
		{
			get { return Umbraco.Web.PublishedContentModels.BaseBannerItem.GetCssClass(this); }
		}

		///<summary>
		/// Introduction
		///</summary>
		[ImplementPropertyType("introduction")]
		public IHtmlString Introduction
		{
			get { return Umbraco.Web.PublishedContentModels.BaseBannerItem.GetIntroduction(this); }
		}

		///<summary>
		/// Navigation Url
		///</summary>
		[ImplementPropertyType("navigationUrl")]
		public Umbraco.Web.Models.RelatedLinks NavigationUrl
		{
			get { return Umbraco.Web.PublishedContentModels.BaseBannerItem.GetNavigationUrl(this); }
		}

		///<summary>
		/// Styles
		///</summary>
		[ImplementPropertyType("styles")]
		public object Styles
		{
			get { return Umbraco.Web.PublishedContentModels.BaseBannerItem.GetStyles(this); }
		}
	}
}
