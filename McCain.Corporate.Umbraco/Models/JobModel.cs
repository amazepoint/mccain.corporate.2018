﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McCain.Corporate.Umbraco.Models
{
    public class JobModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Function { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public string DetailLink { get; set; }
        public string LastUpdated { get; set; }

        public string FunctionFilterLink { get; set; }
        public string CountryFilterLink { get; set; }
    }
}