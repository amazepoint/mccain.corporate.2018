﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace McCain.Corporate.Umbraco.Models
{
    public class VideoDataModel
    {
        public string Url { get; set; }


        public VideoDataModel(IPublishedContent content)
        {
            if (content != null)
            {
                Url = content.Url;
            }
        }

        public bool IsValid()
        {
            return !String.IsNullOrEmpty(Url);
        }
    }
}