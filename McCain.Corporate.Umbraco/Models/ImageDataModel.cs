﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Newtonsoft.Json;

namespace McCain.Corporate.Umbraco.Models
{
    public class ImageDataModel
    {
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("alt")]
        public string Alt { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("caption")]
        public string Caption { get; set; }

        public string AltOrDefault(string defaultValue)
        {
            return !String.IsNullOrEmpty(Alt) ? Alt : defaultValue;
        }

        public ImageDataModel()
        {
        }

        public ImageDataModel(string url, string caption = null)
        {
            Url = url;
            Caption = caption;
        }

        public ImageDataModel(IPublishedContent content, string caption = null)
        {
            if (content != null)
            {
                Url = content.Url;
                Alt = content.Name;
                Title = content.Name;
            }

            Caption = caption;
        }

        public bool IsValid()
        {
            return !String.IsNullOrEmpty(Url);
        }

        public static ImageDataModel GetDefaultImage()
        {
            var image = new ImageDataModel("/images/no-image-grid.png");
            image.Title = "Default Image";
            image.Alt = "Default Image";

            return image;
        }
    }
}