﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Examine;
using Umbraco.Web.PublishedContentModels;
using Umbraco.Core.Models;

namespace McCain.Corporate.Umbraco.Models
{
    public class NewsListModel
    {
        public IEnumerable<IPublishedContent> Items { get; set; }
    }
}