﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McCain.Corporate.Umbraco.Models
{
    public class ImagesGridModel
    {
        public string Title { get; set; }

        public string Theme { get; set; }
        public string CustomCss { get; set; }

        private int itemsPerRow;
        public int ItemsPerRow
        {
            get
            {
                return itemsPerRow > 0 ? itemsPerRow : 2;
            }
            set
            {
                itemsPerRow = value;
            }
        }

        public List<ImagesGridItem> Items { get; set; }

        public ImagesGridModel()
        {
            Items = new List<ImagesGridItem>();
        }
    }

    public class ImagesGridItem
    {
        public string Title { get; set; }

        public string SubTitle { get; set; }

        public string Description { get; set; }

        public string PictureUrl { get; set; }

        public string AltText { get; set; }

        public string NavigationUrl { get; set; }

        public bool OpenLinkInNewWindow { get; set; }
    }
}