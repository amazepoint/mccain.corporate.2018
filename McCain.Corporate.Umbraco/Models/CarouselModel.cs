﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.PublishedContentModels;

namespace McCain.Corporate.Umbraco.Models
{
    public class CarouselModel
    {
        public string CssClass { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public List<CarouselItem> Items { get; set; }

        public bool CarouselEnabled { get; set; }

        public int SlidesPerView { get; set; }

        public bool IsLooped { get; set; }

        public CarouselModel()
        {
            Items = new List<CarouselItem>();
        }
    }

    public class CarouselItem
    {
        public string PictureUrl { get; set; }

        public string AltText { get; set; }

        public string VideoUrl { get; set; }

        public string PromotionTitle { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string NavigationUrl { get; set; }
        public bool OpenInNewTab { get; set; }

        public CarouselItem()
        {
        }

        public CarouselItem(Slide slide)
        {
            PictureUrl = slide.Image == null ? String.Empty : slide.Image.Url;
            AltText = BusinessContext.GetImageAltTextOrDefault(slide.Image,  Title + " Image");
            VideoUrl = slide.VideoUrl;
            PromotionTitle = slide.PromotionTitle;
            Title = slide.Title;
            Description = slide.Description.ToString();
            NavigationUrl = slide.Link;
            OpenInNewTab = slide.OpenLinkInNewTab;
        }
    }
}