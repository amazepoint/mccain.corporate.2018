﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using Umbraco.Web.PublishedContentModels;

namespace McCain.Corporate.Umbraco.Models
{
    public class BannerItemModel
    {
        public string BannerTitle { get; set; }
        
        public VideoDataModel VideoBanner { get; set; }

        public ImageDataModel VideoPoster { get; set; }

        public ImageDataModel BannerImage { get; set; }

        public ImageDataModel BannerImageMobile { get; set; }

        public IHtmlString Introduction { get; set; }

        public RelatedLink NavigationUrl { get; set; }

        public string Styles { get; set; }

        public string CssClass { get; set; }

        public BannerItemModel()
        {

        }

        public BannerItemModel(IPublishedContent content)
        {
            if (content != null)
            {
                switch (content.DocumentTypeAlias)
                {
                    case BannerItem.ModelTypeAlias:
                        BuildFromBannerItem(new BannerItem(content));
                        break;
                }
            }
        }

       

        private void BuildFromBannerItem(BannerItem bannerItem)
        {
            BannerTitle = bannerItem.BannerTitle;
            //VideoBanner = new VideoDataModel(bannerItem.VideoBanner);
            //VideoPoster = new ImageDataModel(bannerItem.VideoPoster);
            BannerImage = new ImageDataModel(bannerItem.BannerImage);
            BannerImageMobile = new ImageDataModel(bannerItem.BannerImageMobile);
            Introduction = bannerItem.Introduction;
            NavigationUrl = bannerItem.NavigationUrl?.FirstOrDefault();
            Styles = bannerItem.Styles?.ToString() ?? "";
            CssClass = bannerItem.CssClass;
        }
    }
}