﻿using Examine;
using Examine.LuceneEngine;
using Lucene.Net.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.WebPages.Html;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace McCain.Corporate.Umbraco.Models
{
    public class SearchResultsModel
    {

        #region Properties

        public string SearchTerm { get; set; }
        
        public ISearchResults SearchResults { get; set; }

        public PagingModel PagingInfo { get; set; }

        public IEnumerable<IPublishedContent> Items { get; set; }

        public bool HasResults { get { return Items != null && Items.Count() > 0; } }

        public Searcher LuceneSearcher { get; set; }

        #endregion

        #region Constructor

        public SearchResultsModel(string searchTerm, ISearchResults searchResults, PagingModel paging)
        {
            SearchTerm = searchTerm;

            SearchResults = searchResults;

            PagingInfo = paging;
            paging.ItemsCount = SearchResults.TotalItemCount;

            Items = SearchHelper.GetResultsForPage(SearchResults, paging);
            LuceneSearcher = ((SearchResults)searchResults).LuceneSearcher;
        }

        #endregion

        #region Public Properties

        public string GetSearchHighlightText(UmbracoHelper umbraco, string fieldName, string fieldValue)
        {
            string result = String.Empty;

            if (!String.IsNullOrEmpty(SearchTerm))
            {
                result = LuceneHelper.Instance.GetHighlight(fieldValue, fieldName, LuceneSearcher, SearchTerm);
            }

            if (!String.IsNullOrEmpty(result))
            {
                result = result.Trim();

                // trim empty lines
                result = Regex.Replace(result, @"^\s+$[\n]*", "", RegexOptions.Multiline);
            }

            return result;
        }

        #endregion
    }
}