﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace McCain.Corporate.Umbraco.Models
{
    public class FeaturedJobsViewModel
    {
        public IEnumerable<JobModel> FeaturedJobs { get; set; }
        public int LiveJobsCount { get; set; }
    }
}