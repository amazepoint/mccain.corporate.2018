$(function () {
    var shareUrl = window.location.href;
    var twitterMsg = "cool+article"

    function openPopup(url) {
        var popup = window.open(url, 'popup', 'height=400,width=600,scrollbars=no');
        if (window.focus) { popup.focus(); }
    }

    $(".share-icons .facebook").on("click", function () {
        openPopup("https://www.facebook.com/sharer/sharer.php?u=" + shareUrl)
        return false;
    });

    $(".share-icons .twitter").on("click", function () {
        openPopup("http://twitter.com/share?url=" + shareUrl + ";text=" + twitterMsg + ";size=l&amp;count=none")
        return false;
    });

    $(".share-icons .linkedin").on("click", function () {
        openPopup("https://www.linkedin.com/shareArticle?mini=true&url=" + shareUrl)
        return false;
    });
})