﻿/* Utils and Helpers */
if (!String.prototype.format) {
    String.prototype.format = function () {
        var a = this;
        for (var k in arguments) {
            a = a.replace(new RegExp("\\{" + k + "\\}", 'g'), arguments[k]);
        }
        return a
    }
}

if (!String.prototype.replaceAll) {
    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    }
}

/* Main Logic */
window['McCainGCFObject'] = window['McCainGCFObject'] || (function () {

    var g_Settings = window['McCainGCFSettings']

    var g_OptionsRepository = {

        getSegments: function (region) {
            var result = ['Foodservice', 'Retail', 'QSR']

            switch (region) {
                case 'Celavita':
                    result = ['Foodservice', 'Retail', 'Industrial']
                    break
            }

            return result
        },

        getSubjects: function (region, subRegion, formType) {
            var result = ['Complaint', 'Compliment', 'Query' ]

            switch (region) {
                case 'Celavita':
                case 'India':
                    result = ['Complaint', 'Compliment', 'Delivery', 'Query']
                    break
                case 'LATAM':
                    if (!formType && subRegion == 'Mexico') {
                        result = ['Complaint', 'Compliment']
                    }
                    break
            }

            return result
        },

        regions: ['NORTH AMERICA', 'GB', 'LATAM', 'CEU', 'Celavita', 'South Africa', 'India' ],

        getSubRegions: function (region) {
            var result = []

            switch (region) {
                case 'NORTH AMERICA':
                    result = ['Canada', 'US']
                    break
                case 'LATAM':
                    result = ['Argentina', 'Brazil', 'Calatin', 'Colombia', 'Mexico', 'Uruguay']
                    break
            }

            return result
        },

        getSubRegionByCountry: function (region, country) {
            var result = ''

            switch (region) {
                case 'NORTH AMERICA':

                    switch (country) {
                        case 'United States':
                            result = 'US'
                            break
                        case 'Canada':
                            result = 'Canada'
                            break
                    }

                    break
                case 'CEU':

                    result = 'Other Countries'

                    var ceuList = [
                        { name: 'Baltics', countries: ['Latvia' ] },
                        { name: 'BeLux', countries: ['Belgium', 'Luxembourg' ] },
                        { name: 'Central Europe', countries: [ 'Czech Republic', 'Hungary', 'Slovakia' ] },
                        { name: 'DACH', countries: [ 'Germany', 'Austria', 'Switzerland'] },
                        { name: 'Eastern Balkans/Ukraine', countries: ['Bulgaria', 'Romania', 'Ukraine' ] },
                        { name: 'Finland/Baltics', countries: ['Lithuania', 'Estonia', 'Finland'] },
                        { name: 'France', countries: [ 'France' ] },
                        { name: 'Greece/Cyprus', countries: ['Cyprus', 'Greece'] },
                        { name: 'Iberia', countries: ['Spain', 'Portugal'] },
                        { name: 'Israel', countries: ['Israel'] },
                        { name: 'Italy/Malta', countries: ['Italy', 'Malta'] },
                        { name: 'Middle East', countries: ['Saudi Arabia', 'Kuwait', 'United Arab Emirates', 'Jordan', 'Lebanon', 'Bahrain', 'Qatar', 'Iceland', 'Reunion', 'Oman', 'Mauritius'] },
                        { name: 'Netherlands', countries: ['Netherlands'] },
                        { name: 'North Africa', countries: ['Algeria', 'Morocco', 'Tunisia'] },
                        { name: 'Other Africa', countries: ['Angola', 'Benin', 'Cameroon', 'Congo', 'Ethiopia', 'Gabon', 'Ghana', 'Cote d\'Ivoire', 'Kenya', 'Libyan Arab Jamahiriya', 'Mali', 'Nigeria', 'Senegal', 'Sudan', 'Tanzania,United Republic of', 'Togo', 'Congo, the Democratic Republic of the'] },
                        { name: 'Other EE', countries: ['Armenia', 'Azerbaijan', 'Belarus', 'Georgia', 'Kazakhstan', 'Kyrgyzstan', 'Moldova, Republic of', 'Tajikistan', 'Turkmenistan', 'Uzbekistan'] },
                        { name: 'Poland', countries: ['Poland'] },
                        { name: 'Russia', countries: ['Russia'] },
                        { name: 'Scandinavia', countries: ['Denmark', 'Norway', 'Sweden',] },
                        { name: 'Turkey', countries: ['Turkey'] },
                        { name: 'Western Balkans', countries: ['Bosnia and Herzegovina', 'Croatia', 'Kosovo', 'Macedonia', 'Montenegro', 'Serbia', 'Slovenia', 'Albania'] }
                    ]

                    for (var i = 0; i < ceuList.length; i++) {
                        var subRegion = ceuList[i]
                        if (subRegion.countries.indexOf(country) > -1) {
                            result = subRegion.name
                            break
                        }
                    }

                    break
            }

            return result
        },

        getCountries: function (region, subRegion) {
            var result = []

            var allCountries = ["Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan",
                "Bolivia, Plurinational State of", "Bonaire, Sint Eustatius and Saba", "Bosnia and Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria",
                "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia",
                "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Curaçao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica",
                "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana",
                "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guatemala", "Guernsey", "Guinea",
                "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City State)", "Honduras", "Hungary", "Iceland", "India", "Indonesia", "Iran, Islamic Republic of", "Iraq",
                "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Kosovo", "Kuwait", "Kyrgyzstan",
                "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar", "Malawi",
                "Malaysia", "Maldives", "Mali", "Malta", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Moldova, Republic of", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique",
                "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Norway", "Oman", "Pakistan", "Palestinian Territory, Occupied",
                "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Barthélemy",
                "Saint Helena, Ascension and Tristan da Cunha", "Saint Kitts and Nevis", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines",
                "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten (Dutch part)", "Slovakia", "Slovenia",
                "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "South Korea", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard and Jan Mayen",
                "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago",
                "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu",
                "Venezuela, Bolivarian Republic of", "Viet Nam", "Virgin Islands, British", "Wallis and Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"]

            function getAllCountriesSorted(mostPopularCountries) {
                var result = mostPopularCountries

                for (var i = 0; i < allCountries.length; i++) {
                    if (mostPopularCountries.indexOf(allCountries[i]) == -1)
                        result.push(allCountries[i])
                }
                return result
            }

            switch (region) {
                case 'NORTH AMERICA':
                    result = getAllCountriesSorted(['Canada', 'United States'])
                    break
                case 'GB':
                    result = getAllCountriesSorted(['United Kingdom'])
                    break
                case 'LATAM':
                    result = getAllCountriesSorted(['Argentina', 'Bolivia, Plurinational State of', 'Brazil', 'Chile', 'Colombia', 'Ecuador', 'Mexico', 'Paraguay', 'Peru', 'Puerto Rico', 'Uruguay', 'Venezuela, Bolivarian Republic of'])
                    break
                case 'CEU':
                case 'Celavita':
                    result = allCountries
                    break
                case 'South Africa':
                    result = getAllCountriesSorted(['South Africa', 'Zambia', 'Zimbabwe'])
                    break
                case 'India':
                    result = getAllCountriesSorted(['India'])
                    break
            }

            return result
        },

        getDivisions: function (region, country) {
            var result = ['', 'Potato', 'Snacks']

            if (region == 'NORTH AMERICA') {
                switch (country) {
                    case 'Canada':
                        result = ['', 'Potato', 'Snacks', 'Desserts', 'Pizza Pockets', 'Wong Wing Appetizers', 'Wong Wing Entrees', 'Wong Wing Rice & Noodles', 'Wong Wing Soup']
                        break
                }
            }
            else if (region == 'CEU') {
                result = ['', 'Potato', 'Dehydrated', 'Snacks']
            }

            return result
        },

        getUOM: function (formType, region) {
            var result = []

            if (formType == 'FirstNotification') {

                switch (region) {
                    case 'CEU':
                        result = [
                            { label: 'Bag/s', value: 'Bags' },
                            { label: 'Big Bag/s', value: 'Big Bag/s' },
                            { label: 'Carton/s', value: 'Cartons' },
                            { label: 'Case/s', value: 'Cases' },
                            { label: 'Kilogram', value: 'Kilogram' },
                            { label: 'Pack/s', value: 'Pack/s' },
                            { label: 'Pallet/s', value: 'Pallet' },
                            { label: 'Tote/s', value: 'Tote/s' },
                            { label: 'Unit/s', value: 'Unit/s' }
                        ]
                        break
                    default:
                        result = [
                            { label: 'Bag/s', value: 'Bags' },
                            { label: 'Case/s', value: 'Cases' },
                            { label: 'Pallet/s', value: 'Pallet' }
                        ]
                        break
                }
            } else {
                result = [
                    { label: 'Bag/s', value: 'Bags' },
                    { label: 'Unit/s', value: 'Unit/s' },
                    { label: 'Kilogram', value: 'Kilogram' },
                    { label: 'Case/s', value: 'Cases' },
                    { label: 'Pallet/s', value: 'Pallet' },
                    { label: 'Pack/s', value: 'Pack/s' }
                ]
            }

            return result
        },

        countries: {
            'Australia': {
                states: [
                    'New South Wales', 'Victoria', 'Queensland', 'Western Australia', 'South Australia', 'Tasmania',
                    'Australian Capital Territory', 'Northern Territory'
                ]
            },
            'South Africa': {
                states: [
                    'Eastern Cape', 'Freestate', 'Gauteng', 'Kwazulu/Natal',
                    'Limpopo', 'Mpumalanga', 'North-West', 'Northern Cape',
                    'Western Cape'
                ]
            },
            'Canada': {
                states: [
                    'Alberta', 'British Columbia', 'Manitoba', 'New Brunswick',
                    'Newfoundland and Labrador', 'Northwest Territories', 'Nova Scotia', 'Nunavut',
                    'Ontario', 'Prince Edward Island', 'Quebec', 'Saskatchewan',
                    'Yukon'
                ]
            },
            'Argentina': {
                states: ['Buenos Aires',
                    'Capital Federal',
                    'Catamarca',
                    'Chaco',
                    'Chubut',
                    'Cordoba',
                    'Corrientes',
                    'Entre Rios',
                    'Formosa',
                    'Jujuy',
                    'La Pampa',
                    'La Rioja',
                    'Mendoza',
                    'Misiones',
                    'Neuquen',
                    'Rio Negro',
                    'Salta',
                    'San Juan',
                    'San Luis',
                    'Santa Cruz',
                    'Santa Fe',
                    'Santiago del Estero',
                    'Tierra de Fuego',
                    'Tucuman']
            },
            'Brazil': {
                states: [
                    'Acre', 'Alagoas', 'Amazon', 'Amapa', 'Bahia', 'Ceara', 'Brasilia',
                    'Espirito Santo', 'Goias', 'Maranhao', 'Minas Gerais', 'Mato Grosso do Sul',
                    'Mato Grosso', 'Para', 'Paraiba', 'Pernambuco', 'Piaui', 'Parana',
                    'Rio de Janeiro', 'Rio Grande do Norte', 'Rondonia', 'Roraima', 'Rio Grande do Sul',
                    'Santa Catarina', 'Sergipe', 'Sao Paulo', 'Tocantins'
                ]
            },
            'Chile': {
                states: [
                    'I - Iquique', 'II - Antofagasta', 'III - Copiapo', 'IV - La Serena', 'V - Valparaiso',
                    'VI - Rancagua', 'VII - Talca', 'VIII - Concepcion', 'IX - Temuco', 'X - Puerto Montt',
                    'XI - Coyhaique', 'XII - Punta Arenas', 'RM - Santiago'
                ]
            },
            'Colombia': {
                states: [
                    'ANTIOQUIA', 'ATLANTICO', 'BOGOTA', 'BOLIVAR', 'BOYACA', 'CALDAS', 'CAQUETA', 'CAUCA',
                    'CESAR', 'CORDOBA', 'CUNDINAMARCA', 'CHOCO', 'HUILA', 'LA GUAJIRA', 'MAGDALENA',
                    'META', 'NARINO', 'NORTE SANTANDER', 'QUINDIO', 'RISARALDA', 'SANTANDER',
                    'SUCRE', 'TOLIMA', 'VALLE', 'ARAUCA', 'CASANARE', 'PUTUMAYO', 'SAN ANDRES',
                    'AMAZONAS', 'GUAINIA', 'GUAVIARE', 'VAUPES', 'VICHADA'
                ]
            },
            'Mexico': {
                states: [
                    'Aguascalientes',
                    'Baja California',
                    'Baja California S',
                    'Chihuahua',
                    'Chiapas',
                    'Campeche',
                    'Coahuila',
                    'Colima',
                    'Distrito Federal',
                    'Durango',
                    'Guerrero',
                    'Guanajuato',
                    'Hidalgo',
                    'Jalisco',
                    'Michoacan',
                    'Estado de Mexico',
                    'Morelos',
                    'Nayarit',
                    'Nuevo Leon',
                    'Oaxaca',
                    'Puebla',
                    'Quintana Roo',
                    'Queretaro',
                    'Sinaloa',
                    'San Luis Potosi',
                    'Sonora',
                    'Tabasco',
                    'Tlaxcala',
                    'Tamaulipas',
                    'Veracruz',
                    'Yucatan',
                    'Zacatecas'
                ]
            },
            'Venezuela': {
                states: [
                    'Amazon',
                    'Anzoategui',
                    'Apure',
                    'Aragua',
                    'Barinas',
                    'Bolivar',
                    'Carabobo',
                    'Cojedes',
                    'Delta Amacuro',
                    'Distrito Federal',
                    'Falcon',
                    'Guarico',
                    'Lara',
                    'Merida',
                    'Miranda',
                    'Monagas',
                    'Nueva Esparta',
                    'Portuguesa',
                    'Sucre',
                    'Tachira',
                    'Trujillo',
                    'Vargas',
                    'Yaracuy',
                    'Zulia'
                ],
            },
            'Ecuador': {
                states: [
                    'Azuay',
                    'Bolívar',
                    'Cañar',
                    'Carchi',
                    'Chimborazo',
                    'Cotopaxi',
                    'El Oro',
                    'Esmeraldas',
                    'Galápagos',
                    'Guayas',
                    'Imbabura',
                    'Loja',
                    'Los Ríos',
                    'Manabí',
                    'Morona Santiago',
                    'Napo',
                    'Orellana',
                    'Pastaza',
                    'Pichincha',
                    'Santa Elena',
                    'Santo Domingo de los',
                    'Sucumbíos',
                    'Tungurahua',
                    'Zamora Chinchipe'
                ],
            },
            'United States': {
                states: [
                    'Alabama',
                    'Alaska',
                    'Arizona',
                    'Arkansas',
                    'California',
                    'Colorado',
                    'Connecticut',
                    'DC',
                    'Delaware',
                    'Florida',
                    'Georgia',
                    'Hawaii',
                    'Idaho',
                    'Illinois',
                    'Indiana',
                    'Iowa',
                    'Kansas',
                    'Kentucky',
                    'Louisiana',
                    'Maine',
                    'Maryland',
                    'Massachusetts',
                    'Michigan',
                    'Minnesota',
                    'Mississippi',
                    'Missouri',
                    'Montana',
                    'Nebraska',
                    'Nevada',
                    'New Hampshire',
                    'New Jersey',
                    'New Mexico',
                    'New York',
                    'North Carolina',
                    'North Dakota',
                    'Ohio',
                    'Oklahoma',
                    'Oregon',
                    'Pennsylvania',
                    'Puerto Rico',
                    'Rhode Island',
                    'South Carolina',
                    'South Dakota',
                    'Tennessee',
                    'Texas',
                    'Utah',
                    'Vermont',
                    'Virginia',
                    'Washington',
                    'West Virginia',
                    'Wisconsin',
                    'Wyoming'
                ]
            },
            'India': {
                states: [
                    'Andaman and Nicobar Islands', 'Andhra Pradesh', 'Arunachal Pradesh', 'Assam',
                    'Bihar', 'Chandigarh', 'Chhattisgarh', 'Daman and Diu',
                    'Delhi', 'Dadra and Nagar Haveli', 'Goa', 'Gujarat',
                    'Himachal Pradesh', 'Haryana', 'Jharkhand', 'Jammu and Kashmir',
                    'Karnataka', 'Kerala', 'Lakshadweep', 'Maharashtra',
                    'Meghalaya', 'Manipur', 'Madhya Pradesh', 'Mizoram',
                    'Nagaland', 'Odisha', 'Punjab', 'Puducherry',
                    'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Tripura',
                    'Uttar Pradesh', 'Uttarakhand', 'West Bengal'
                ]
            }
        },

        firms: [
            '',
            'A & P',
            'ACH Foodservice, Inc.',
            'Albertson’s',
            'Albertson\'s, Inc. / Risk Management',
            'All Virginia Food Service Brokers',
            'Arby\'s Restaurant Group',
            'Arby\'s Restaurant Group, Inc.',
            'BI-LO LLC Corporate Offices',
            'Broadspire Services',
            'Broadspire Services, Inc.',
            'Burger King Corporation',
            'Buschor Brokerage Company',
            'C&G Food Brokerage, Inc.',
            'Catalina Restaurant Group, Inc.',
            'Chenault & Baroni Food Service',
            'Chick-fil-A Inc.',
            'CNA',
            'Complete FoodService Solutions',
            'Concept Food Sales',
            'Concept Food Sales - North',
            'Cracker Barrel Old Country Store, Inc.',
            'Crawford & Company',
            'Creative Food Service, LLC',
            'Del Taco',
            'Deli Dynamics (Deli Sales Only)',
            'Durst Brokerage, Inc.',
            'Elite FoodService Associates Inc.',
            'Elite FoodService Associates, Inc.',
            'Food Group Alabama',
            'Food Marketing Concepts',
            'Food Marketing Services, Inc.',
            'Food Marketing Specialists, Inc.',
            'Food Sales East',
            'Food Sales East  - Carolinas',
            'Food Sales East - Carolinas, LLC',
            'Food Sales East,  Georgia',
            'Food Sales East, Georgia',
            'Food Sales East,Georgia',
            'Food Sales Systems',
            'Friendly\'s Ice Cream',
            'GAB Robins Risk Management Services, Inc.',
            'Gallagher Bassett Services, Inc.',
            'General Casualty',
            'Glacier Sales',
            'Glacier Sales, Inc.',
            'Gordon Food Service',
            'Great Central Member Argonaut Group',
            'Hardee\'s Food Systems, Inc.',
            'Heinz North America',
            'Highland Ventures, Inc.',
            'Hiter Food Associates',
            'Hobby-Whalen Marketing',
            'Institutional Sales Associates - Austin',
            'Institutional Sales Associates - Houston',
            'Integrated Marketing Technologies',
            'Intersouth FoodService Group',
            'J. Goodman & Associates',
            'KDM Food Sales, Inc.',
            'Kelley Brokerage',
            'Key Impact North',
            'Key Impact Sales - North',
            'Key Impact Sales - South',
            'Key Impact Sales - Tennessee',
            'Key Impact Sales - Virginia',
            'Key Impact Sales & Systems',
            'KeyImpact Sales',
            'KeyImpact Sales & Systems',
            'Lad Food Service Sales Inc.',
            'Lamb\'s Progressive Food Service',
            'Liberty Mutual Group',
            'Liberty Mutual Insurance Company',
            'MAC Risk Management, Inc.',
            'Market Dynamics Group, LLC - New England',
            'Market Dynamics Group, LLC - Upstate NY',
            'Market Smart',
            'Marketing Specialists, Inc.',
            'Martin & Greer Brokerage',
            'Massey-Fair Industrial',
            'Michael\'s & Associates',
            'Michaels & Associates, Inc.',
            'Montgomery Insurance',
            'Morton & Associates',
            'Morton & Associates - Alaska',
            'O.E. Brokerage',
            'Overseas Service Corp',
            'erformance Marketing West',
            'Performance Marketing, Inc.',
            'Personalized Distribution Systems',
            'Pioneer Food Broker, Inc.',
            'Popeyes Chicken & Biscuits',
            'Prestige Sales & Marketing',
            'Prime Sales & Marketing',
            'Progressive FoodService Brokerage',
            'Pyramid Food Brokers, Inc.',
            'Rainbow Sales and Marketing',
            'Red Robin Gourmet Burgers',
            'Regan Marketing, Inc.',
            'Reinhart Food Service, LLC',
            'Reinhart FoodService, LLC ',
            'Roger Dunn Brokerage Co., Inc.',
            'Roisum Elite (IA)',
            'Roisum Elite (NE)',
            'Roisum Elite Sales & Marketing',
            'Roisum Food Service Sales',
            'Sentry Insurance a Mutual Company',
            'Sonic Industries',
            'Southeast Sales & Marketing, Inc.',
            'Southwest FoodService Co.',
            'Specialty Food Sales & Marketing',
            'St. Paul Travelers Insurance',
            'Super Store Industries',
            'Sygma Distribution Center',
            'SYSCO Corporation',
            'T.A. Dowd Brokerage LLC',
            'TCM',
            'The Kroger Company',
            'Tucker Sales',
            'Wal-Mart Stores, Inc.',
            'WAUSAU Insurance Companies',
            'Wegman\'s Food Markets',
            'Wendy\'s International, Inc.',
            'Western Foodservice Marketing',
            'Western Specialty Sales',
            'Willis Marketing',
            'York Claims Service, Inc.',
            'Zaxby\'s Franchising, Inc.'
        ],

        complaintCodes: [
            'Batter Coverage',
            'Cheese',
            'Chemical Flavor/Odor',
            'Clusters/Married',
            'Color',
            'Cooking',
            'Count Per Pound',
            'Damaged Product',
            'Defects',
            'Dissatisfied',
            'Exploding',
            'Flavor/Odor',
            'FM - Annatto',
            'FM - Carbon',
            'FM - Crumbs',
            'FM - Floaters',
            'FM - Glass',
            'FM - Grease',
            'FM - Hair',
            'FM - Insect',
            'FM - Metal',
            'FM - Onion Peel/Skin',
            'FM - Other',
            'FM - Plastic',
            'FM - Rock',
            'FM - Rubber',
            'FM - Scrapings',
            'FM - Stems/Vines',
            'FM - Whole Potato',
            'FM - Wood',
            'GMO',
            'Inquiry',
            'Length',
            'Light Weight',
            'Missing Slices',
            'Missing Topping',
            'Mixed Product',
            'NP - Batter Coverage',
            'NP - Black Pepper',
            'NP - Cheese',
            'NP - Chemical Flavor/Odor',
            'NP - Clusters/Married',
            'NP - Color',
            'NP - Comment',
            'NP - Cooking',
            'NP - Count Per Pound',
            'NP - Damaged Product',
            'NP - Defects',
            'NP - Duplicate Entry',
            'NP - Exploding',
            'NP - Flavor/Odor',
            'NP - Length',
            'NP - Light Weight',
            'NP - Mislabeling',
            'NP - Missing Slices',
            'NP - Missing Topping',
            'NP - Mixed Product',
            'NP - Old Product',
            'NP - Other',
            'NP - Packaging/Poly',
            'NP - Scoring',
            'NP - Shrink Wrap',
            'NP - Size',
            'NP - Size (cut)',
            'NP - Temperature Abuse',
            'NP - Test Entry',
            'NP - Texture',
            'NP - Torn Overwrap',
            'NP - Uniformity',
            'Old Product',
            'Other',
            'Packaging/Poly',
            'Palletization Errors',
            'Praise',
            'Process Control',
            'Sauce Coverage',
            'Scoring',
            'Shipping/Storage',
            'Shrink Wrap',
            'Size',
            'Size (cut)',
            'Temperature Abuse',
            'Testing',
            'Texture',
            'Torn Overwrap',
            'Uniformity',
            'Where to Buy'
        ],

        getPlants: function (region) {
            var result = []

            switch (region) {
                case 'NORTH AMERICA':
                    result = [
                        '',
                        'Aliya’s Food Ltd (Chef Bombay)',
                        'Apple Valley Foods Inc',
                        'Appleton Plant',
                        'Ardo',
                        'Burley Plant',
                        'Carberry Plant',
                        'Coaldale Plant',
                        'Colton Plant',
                        'DC Foods (Co-Man for Maple Leaf Foods Inc)',
                        'Dickinson Frozen Foods Inc',
                        'Double B, Inc.',
                        'Easton Plant',
                        'Florenceville Plant (2 plants)',
                        'Frozavo',
                        'Grand Falls Plant',
                        'Grand Island Plant',
                        'Great American Snacks, Nampa Plant',
                        'Infinity Foods',
                        'Lutosa Frozen',
                        'Marsan Foods Ltd',
                        'Morrison LaMothe Inc (Middlefield)',
                        'Mr. Dells',
                        'Oregon Potato Company',
                        'Othello Plant',
                        'Oxford Frozen Foods Ltd',
                        'Pasco Processing',
                        'Plover Plant',
                        'Portage La Prairie',
                        'Rice Lake Plant',
                        'Rite Stuff Foods',
                        'Scelta Products B.V.',
                        'Scelta Products Kruiningen',
                        'Scelta Products Yerseke',
                        'Unknown Plant',
                        'Ventura Foods',
                        'Western Waffles Ltd (Division of ConAgra)',
                        'Wing Noodles Ltd',
                        'Wong Wing Plant'
                    ]
                    break
                
                case 'GB':
                    result = ['', 'Scarborough Plant', 'Whittlesey Plant']
                    break
                case 'CEU':
                    result = ['', 'Béthune Plant', 'Celavita', 'Harnes Plant', 'Lelystad Plant', 'Lewedorp Plant', 'Lutosa Plant', 'Matougues Plant', 'Strzelin']
                    break
                case 'Celavita':
                    result = ['', 'Celavita', 'Lutosa' ]
                    break
                case 'India':
                    result = ['', 'Mehsana' ]
                    break
                case 'LATAM':

                    result = ['', 'AB Logistica',
                        'ARCOSA TEPOTZOTLAN',
                        'ARFRIO - Sao Paulo',
                        'ARFRIO Rio de Janerio',
                        'Accel - DF',
                        'Accel- Chihuahua',
                        'Alamesa- Tultitlan',
                        'Andesmar - Buenos Aires',
                        'Appleton Plant',
                        'Ardo',
                        'Axion Log',
                        'Axionlog - Malvinas',
                        'Axis Logistica SA',
                        'Bajo Cero',
                        'Beyman',
                        'Borden Plant',
                        'Buen Ayre (CALICO)',
                        'Burley Plant',
                        'Béthune Plant',
                        'Calico - MDP',
                        'Carberry Plant',
                        'Cefri',
                        'Ciantini - Balcarce',
                        'Coaldale Plant',
                        'Colfrigos - Barranquilla',
                        'Colfrigos - Bogotá',
                        'Colfrigos - Cali',
                        'Colfrigos - Medelllin',
                        'Colton Plant',
                        'Congelados Centro SA',
                        'Corfrisa',
                        'DSD Truck Plant',
                        'Delpack - Mar Del Plata',
                        'Distribuidora Star SRL',
                        'Dodero - Pacheco',
                        'EXPOR SAN ANTONIO',
                        'Easton Plant',
                        'Ecofrio, S.A.P.I. de C.V.',
                        'Fernando Ruiz',
                        'Florenceville Plant',
                        'Fort Atkinson Plant',
                        'Frialsa - Atitalaquia',
                        'Frialsa - Cancun',
                        'Frialsa - Colombia',
                        'Frialsa - Culiacan',
                        'Frialsa - Jaliso',
                        'Frialsa - Monterry',
                        'Frialsa - Ocoyoacac',
                        'Frialsa Villagran 2',
                        'Frialsa-Aguascalientes',
                        'Frialsa-San Martín de Obispo',
                        'Frialsa-Tijuana',
                        'Frialsa-Tultitlán',
                        'Frideco SAS - Cali',
                        'Frigorif Metrop Bucaramanga',
                        'Frigorif Metrop Cartagena',
                        'Frio Frimac',
                        'Frio Polar',
                        'Frozavo',
                        'Grand Falls Plant',
                        'Grand Island Plant',
                        'Great American Snacks, Nampa Plant',
                        'Grobbendonk Plant',
                        'Harnes Plant',
                        'Hastings  Plant',
                        'Javier Palacios Pereira',
                        'Javier Palacios',
                        'LOS OLIVOS SRL',
                        'La Esperanza',
                        'Laredo Cold Storage',
                        'Lelystad Plant',
                        'Lewedorp Plant',
                        'Lineage McAllen DC',
                        'LocalFrio - Guarujá',
                        'Localfrio',
                        'Lodi Plant',
                        'Lutosa Frozen',
                        'Matougues Plant',
                        'Megafin',
                        'México City',
                        'NORTH COOL S.A.',
                        'Occidental de congelados',
                        'Operacion Santa Martha',
                        'Othello Plant',
                        'Planta Balcarce',
                        'Planta Bogotá',
                        'Planta Pilar',
                        'Plover Infinity',
                        'Plover Plant',
                        'Portage La Prairie Plant',
                        'Pronta Entrega',
                        'Puerto Frio SRL',
                        'Rentafrio - Bogotá',
                        'Rice Lake Plant',
                        'Rodo Teixeira',
                        'STAC',
                        'SURFRIGO EZEIZA',
                        'Santana Distribuciones',
                        'Scarborough Plant',
                        'Servifrio',
                        'Silbrumar - Mar del Plata',
                        'Surfrigo',
                        'Timaru  Plant',
                        'USL CARGO SERVICES',
                        'Versacold Logistics Pilar',
                        'Versacold Mercado Central',
                        'Whittlesey Plant',
                        'Wong Wing Plant' ]

                    break
            }

            return result
        },

        getCaseBrands: function (region) {
            var result = [ '', 'McCain', 'Private Label', 'Brew City', 'Other' ]

            switch (region) {
                case 'CEU':
                    result = ['', 'Burger King', 'Celavita', 'McCain', 'McDonald\'s', 'Private Label', 'KFC', 'Other']
                    break
                case 'Celavita':
                    result = ['', 'Albert Heijn', 'Aldi', 'Brew City', 'Carrefour', 'Celavita', 'Colruyt', 'Jumbo', 'McCain', 'Private Label', 'Other' ]
                    break
                case 'LATAM':
                    result = ['','Anchor', 'Brew City', 'Burger King', 'Carleton Farms', 'Caterpac',
                        'Chef Sensations', 'Farmpac', 'Favorita', 'Forno de Minas',
                        'Generic Label', 'Golden Crisp', 'Golden Fry', 'Grabitizers', 'Idaho Valley',
                        'Jon Lin', 'KFC', 'McCain', 'McDonald\'s','Moores','One Fry',
                        'Ore-Ida', 'Pine Tree', 'Potato Harvest', 'Private Label', 'Second',
                        'Snowflake', 'Star Julienne', 'Sysco', 'Tater Chef', 'Valley Farms',
                        'Wendys', 'Wong Wing','Other']
                    break
            }

            return result
        },

        getConsumerAffairsCommunication: function () {
            var result = ['', 'Email', 'Phone', 'Letter', 'WhatsApp', 'No Contact']

            return result
        },

        getPreferredContactMode: function () {
            var result = ['', 'Email', 'Phone', 'Both']

            return result
        }
    }

    var g_CookieUtils = {
        setCookie: function (name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";
            document.cookie = name + "=" + value + expires + "; path=/";
        },
        getCookie: function (name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        },
        eraseCookie: function (name) {
            g_CookieUtils.setCookie(name, "", -1);
        }
    }


    function objectToFormData(obj, form, namespace) {

        var fd = form || new FormData();
        var formKey;

        for (var property in obj) {
            if (obj.hasOwnProperty(property)) {

                if (namespace) {
                    formKey = namespace + '[' + property + ']';
                } else {
                    formKey = property;
                }

                // if the property is an object, but not a File,
                // use recursivity.
                if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {

                    objectToFormData(obj[property], fd, formKey);

                } else {

                    // if it's a string or a File object
                    fd.append(formKey, obj[property]);
                }

            }
        }

        return fd
    }

    function createSelectDataSourceFromLabels(arrayOfLabels) {
        var result = []

        if (arrayOfLabels && arrayOfLabels.length > 0) {
            for (var i = 0; i < arrayOfLabels.length; i++) {
                result.push({ label: arrayOfLabels[i], value: arrayOfLabels[i] })
            }
        }

        return result
    }

    function createFormItem(itemType, name, additionalParams) {

        var result = ''

        switch (itemType) {
            case 'text':

                var inputTemplate = '<el-form-item  v-bind:class="[ \'gcf-input\', \'gcf-input-{0}\' ]" prop="{0}">' +
                    '<template slot="label">{{getFieldLabel(\'{0}\')}}' + getFormItemTooltip(additionalParams) + '</template>' +
                    '<el-input type="text" v-model="contactForm.{0}" :placeholder="getFieldTitle(\'{0}\')" maxlength="100" ></el-input>' +
                    '</el-form-item>'

                result = inputTemplate.format(name)

                break
            
            case 'number':

                var inputNumberTemplate = '<el-form-item :label="getFieldLabel(\'{0}\')" v-bind:class="[ \'gcf-input\', \'gcf-input-{0}\' ]" prop="{0}">' +
                    '<el-input-number :controls="true" controls-position="right" :min="0" v-model="contactForm.{0}" :placeholder="getFieldTitle(\'{0}\')"></el-input-number>' +
                    '</el-form-item>'

                result = inputNumberTemplate.format(name)

                break
            case 'select':

                var selectTemplate = '<el-form-item prop="{0}" v-bind:class="[ \'gcf-select\', \'gcf-select-{0}\' ]" :label="getFieldLabel(\'{0}\')">' +
                    '<el-select v-model="contactForm.{0}" :placeholder="getFieldTitle(\'{0}\')" v-on:change="handlePropChange(\'{0}\')" ref="{0}Select" filterable>' +
                    '<el-option v-for="item in options.{0}"' +
                    ':key="item.value"' +
                    ':label="translate(item.label)"' +
                    ':value="item.value">' +
                    '</el-option>' +
                    '</el-select>' +
                    '</el-form-item >'

                result = selectTemplate.format(name)

                break
            case 'date':

                var type = 'date'
                if (additionalParams && additionalParams.type) {
                    type = additionalParams.type
                }

                var format = (type == 'month' ? 'MM/yyyy' : 'dd/MM/yyyy')

                var datePickerTemplate = '<el-form-item prop="{0}" v-bind:class="[ \'gcf-datepicker\', \'gcf-datepicker-{0}\'  ]"' +
                    ':label="getFieldLabel(\'{0}\')">' +
                    '<el-date-picker ref="{0}Picker"' +
                    'v-model="contactForm.{0}"' +
                    'value-format="MM/dd/yyyy"' +
                    'type="' + type + '"' +
                    'format="' + format + '"' +
                    'placeholder="' + format + '">' +
                    '</el-date-picker>' +
                    '<template slot="label">{{getFieldLabel(\'{0}\')}}' + getFormItemTooltip(additionalParams) + '</template>' +
                    '</el-form-item>'

                result = datePickerTemplate.format(name)

                break
            case 'textarea':

                var textAreaTemplate = '<el-form-item prop="{0}" v-bind:class="[ \'gcf-textarea\', \'gcf-textarea-{0}\' ]"' +
                    ':label="getFieldLabel(\'{0}\')">' +
                    '<el-input type="textarea" v-model="contactForm.{0}" :rows="5" :placeholder="getFieldTitle(\'{0}\')" maxlength="1000"></el-input>' +
                    '</el-form-item>'

                result = textAreaTemplate.format(name)

                break
            case 'file':

                var title = additionalParams ? additionalParams.label : '';
                if (!title) {
                    title = 'Add photo (optional)';
                }

                var fileUploadTemplate = '<div id="gcf-upload-holder{0}" class="button-holder-upload upload{0}">' +
                    '<el-upload class="upload-photo"' +
                    'ref="photoUpload{0}"' +
                    'id="gcf-upload-photo{0}"' +
                    'name="gcf-upload-photo-input{0}"' +
                    'action="/"' +
                    'accept="image/*"' +
                    ':limit="1"' +
                    ':auto-upload="false"' +
                    ':file-list="fileList{0}"' +
                    ':on-remove="handleFileRemove{0}"' +
                    ':on-change="handleFileChange{0}">' +
                    '<el-button slot="trigger">{{translate(\'' + title + '\')}}</el-button>' +
                    '</el-upload>' +
                    '</div>'

                result = fileUploadTemplate.format(name)

                break
            case 'checkbox':

                var inputTemplate = '<el-form-item :label="getFieldLabel(\'{0}\')" v-bind:class="[ \'gcf-checkbox\', \'gcf-checkbox-{0}\' ]" prop="{0}">' +
                    '<el-checkbox v-model="contactForm.{0}"><span v-html="getFieldTitle(\'{0}\')"></span></el-checkbox>' +
                    '</el-form-item>'

                result = inputTemplate.format(name)

                break
            case 'radio':

                var inputTemplate = '<el-form-item :label="getFieldLabel(\'{0}\')" v-bind:class="[ \'gcf-radio\', \'gcf-radio-{0}\' ]" prop="{0}">' +
                    '<el-radio v-model="contactForm.{0}" label="yes">Yes</el-radio>' +
                    '<el-radio v-model="contactForm.{0}" label="no">No</el-radio>' +
                    '</el-form-item>'

                result = inputTemplate.format(name)

                break
            case 'separator':

                result = '<div class="gcf-separator"></div>'

                break
            case 'submit':

                result = '<div class="button-holder button-holder-submit"><el-button native-type="submit" class="gcf-button gcf-button-submit" v-on:click="submitForm">{{ translate(buttonTitle) }}</el-button></div>'

                break
            default:
                throw 'MGCF: createFormItem - not supported itemType'
                break
        }

        return result
    }

    function getFormItemTooltip(additionalParams) {
        var result = ''

        if (additionalParams && additionalParams.tooltip) {
            result = '<el-popover popper-class="gcf-popper" placement="top" trigger="click">' + additionalParams.tooltip.content + '<span class="gcf-tooltip" slot="reference">' + additionalParams.tooltip.title + '</span></el-popover>'
        }

        return result
    }

    function partializeFormItem(formItemHtml, percentage) {
        return '<div class="gcf-form-item gcf-p' + percentage + '">' + formItemHtml + '</div>'
    }

    function createSeparatorRow() {
        return '<el-row>' +
            '<el-col :sm="24" :lg="24">' + createFormItem('separator') + '</el-col>' +
        '</el-row>'
    }

    function wrapContentWithIfCondition(condition, content) {
        return '<template v-if="' + condition + '">' + content + '</template>'
    }

    var formHTMLTemplate = ''

    if (g_Settings.formType) {

        switch (g_Settings.formType) {
            case 'BrokerPortal':
                formHTMLTemplate = '<el-row>' +

                    '<el-col :sm="24" :lg="12">' +

                    createFormItem('text', 'businessName') +
                    createFormItem('select', 'title') +
                    createFormItem('text', 'firstName') +
                    createFormItem('text', 'lastName') +
                    
                    createFormItem('text', 'addressLine1') +
                    createFormItem('text', 'addressLine2') +
                    createFormItem('text', 'city') +
                    createFormItem('select', 'province') +
                    createFormItem('text', 'postalCode') +
                    createFormItem('text', 'emailAddress') +
                    createFormItem('text', 'phoneNumber') +

                    '</el-col>' +

                    '<el-col :sm="24" :lg="12">' +

                    createFormItem('select', 'subject') +
                    createFormItem('text', 'productName') +
                    createFormItem('select', 'plant') +
                    createFormItem('text', 'batchID') +
                    createFormItem('text', 'upc') +
                    createFormItem('textarea', 'comments') +
                    '</el-col>' +

                    '</el-row>' +

                    createSeparatorRow() +

                    '<div class="gcf-person-completing-holder">' +

                    '<div class="gcf-person-completing-inputs">' +

                    '<el-row>' +
                    '<el-col :sm="24" :lg="24">' + '<h4>' + 'Person Completing Form' + '</h4>' + '</el-col>' +
                    '</el-row>' +


                    '<el-row>' +
                    '<el-col :sm="24" :lg="12">' + createFormItem('text', 'personCompletingFormName') +
                    createFormItem('text', 'brokerageFirm') + '</el-col>' +

                    '<el-col :sm="24" :lg="12">' +
                    createFormItem('text', 'personCompletingFormEmail') + createFormItem('checkbox', 'personCompletingFormNotification') + '</el-col>' +

                    '</el-row>' +

                    '</div>' +
                    '</div>' +


                    createSeparatorRow() +

                    '<el-row>' +
                    '<el-col :span="16" :offset="4">' + createFormItem('select', 'consumerAffairsCommunication') + '</el-col>' +
                    '</el-row>' +

                    createSeparatorRow() +

                    '<el-row>' +
                    '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Upload Images\') }}</h4></div>' + '</el-col>' +
                    '</el-row>' +

                   

                    '<el-row>' +
                    '<el-col :xs="24" :md="8">' + createFormItem('file', '1') + '<br/>' + '</el-col>' +
                    '<el-col :xs="24" :md="8">' + createFormItem('file', '2') + '<br/>' +'</el-col>' +
                    '<el-col :xs="24" :md="8">' + createFormItem('file', '3') + '<br/>' +'</el-col>' +
                    '</el-row>' +

                    createSeparatorRow() +

                    '<el-row>' +
                    '<el-col :sm="24" :lg="24">' +
                    
                    createFormItem('checkbox', 'isPrivacyPolicyAgreed') +
                    
                    '</el-col>' +
                    '</el-row>' +
                    createSeparatorRow() +

                    '<el-row>' +
                    '<el-col :sm="24" :lg="24">' + createFormItem('submit') + '</el-col>' +
                    '</el-row>'

                break;

                break;
            case 'FirstNotification':

                
                var upcExampleHtml = '<img src="https://www.mccain.com/upc-canada.jpg" />'
                var productionCodeExampleHtml = '<img src="https://www.mccain.com/production-code-ceu-1.jpg" /><img src="https://www.mccain.com/production-code-ceu-2.jpg" />'
               

                formHTMLTemplate = '<el-row>' +

                    '<el-col :sm="24" :lg="12">' +

                    '<fieldset>' +
                    '<legend>General</legend>' +

                    createFormItem('select', 'region') +
                   
                    wrapContentWithIfCondition('options.subRegion.length > 0',
                        createFormItem('select', 'subRegion')) +

                    createFormItem('select', 'segment') +

                    wrapContentWithIfCondition('isNotInRegions([ \'South Africa\' ])',
                        createFormItem('select', 'caseBrand')) +

                    '</fieldset>' +

                    '<fieldset>' +
                    '<legend>Contact Details</legend>' +

                    createFormItem('text', 'businessName') +


                    createFormItem('select', 'title') +
                    createFormItem('text', 'firstName') +
                    createFormItem('text', 'lastName') +

                    createFormItem('select', 'country') +

                    wrapContentWithIfCondition('isNotInRegions([ \'South Africa\' ])',
                        createFormItem('text', 'addressLine1') +
                        createFormItem('text', 'addressLine2')) +

                    createFormItem('text', 'city') +
                    createFormItem('select', 'province') +
                    createFormItem('text', 'postalCode') +
                    createFormItem('text', 'emailAddress') +
                    createFormItem('text', 'phoneNumber') +

                    '</fieldset>' +

                    '</el-col>' +

                    '<el-col :sm="24" :lg="12">' +

                    '<fieldset>' +
                    '<legend>Case Details</legend>' +

                    createFormItem('select', 'subject') +

                    wrapContentWithIfCondition('isInRegions([ \'NORTH AMERICA\', \'CEU\' ])',
                        createFormItem('select', 'division')) +

                    wrapContentWithIfCondition('isNotInRegions([ \'South Africa\', \'GB\' ])',
                        createFormItem('checkbox', 'foodSafetyIssue')) +

                    wrapContentWithIfCondition('isNotInRegions([ \'South Africa\' ])',
                        createFormItem('select', 'plant')) +

                    wrapContentWithIfCondition('isInRegions([ \'Celavita\' ])',
                        createFormItem('text', 'invoiceNumber') +
                        createFormItem('text', 'deliveryNumber') +
                        createFormItem('text', 'purchaseOrderNumber')) +

                    createFormItem('text', 'productName') +

                    wrapContentWithIfCondition('isNotInRegions([ \'South Africa\' ])',
                        createFormItem('text', 'batchID', { tooltip: { title: '<i class="el-icon-question"></i>', content: productionCodeExampleHtml } })) +

                    wrapContentWithIfCondition('isNotInRegions([ \'South Africa\' ])',
                        createFormItem('text', 'upc', { tooltip: { title: '<i class="el-icon-question"></i>', content: upcExampleHtml } })) +

                    wrapContentWithIfCondition('isInRegions([ \'Celavita\', \'NORTH AMERICA\', \'South Africa\', \'LATAM\'  ])',
                        createFormItem('date', 'expiryDate')) +

                    wrapContentWithIfCondition('isInRegions([ \'CEU\', \'GB\'  ])',
                        createFormItem('date', 'expiryDate', { type: 'month' })) +

                    wrapContentWithIfCondition('isInRegions([ \'GB\'  ])',
                        createFormItem('date', 'useByDate')) +

                    wrapContentWithIfCondition('isInRegions([ \'South Africa\', \'LATAM\', \'CEU\' ])',
                        createFormItem('number', 'qtyOnHand') +
                        createFormItem('select', 'uom')) +

                    wrapContentWithIfCondition('isInRegions([ \'CEU\', \'Celavita\' ])',
                        createFormItem('select', 'language')) +

                    createFormItem('textarea', 'comments') +
                    createFormItem('textarea', 'specialInstruction') +

                    '</fieldset>' +

                    '</el-col>' +

                    '</el-row>' +

                    createSeparatorRow() +

                    '<el-row>' +
                    '<el-col :sm="24" :lg="24">' +

                    '<fieldset>' +
                    '<legend>Person Completing Form</legend>' +
                        '<el-row>' +
                    '<el-col :sm="24" :lg="12">'  + createFormItem('text', 'personCompletingFormName') +
                    createFormItem('text', 'personCompletingFormEmail') + createFormItem('checkbox', 'personCompletingFormNotification') + '</el-col>' +

                    '<el-col :sm="24" :lg="12">' +
                        wrapContentWithIfCondition('isInRegions([ \'NORTH AMERICA\' ])',
                            createFormItem('select', 'personCompletingFormType') +
                            createFormItem('select', 'brokerageFirm')) +
                        '</el-col>' +
                        '</el-row>' +
                        '</fieldset>' +
                    '</el-col>' +
                    '</el-row>' +


                    createSeparatorRow() +

                    wrapContentWithIfCondition('isNotInRegions([ \'South Africa\', \'LATAM\' ])',
                        '<el-row>' +
                        '<el-col :span="16" :offset="4">' + createFormItem('select', 'consumerAffairsCommunication') + '</el-col>' +
                        '</el-row>' +
                        createSeparatorRow()) +

                    wrapContentWithIfCondition('isInRegions([ \'NORTH AMERICA\' ])',
                        '<el-row>' +
                        '<el-col :span="12" :offset="6">' + createFormItem('checkbox', 'isExport') + '</el-col>' +
                        '</el-row>' +
                        createSeparatorRow()) +

                    '<el-row>' +
                    '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Upload Images\') }}</h4></div>' + '</el-col>' +
                    '</el-row>' +

                    '<el-row>' +
                    '<el-col :xs="24" :md="8">' + createFormItem('file', '1') + '<br/>' +'</el-col>' +
                    '<el-col :xs="24" :md="8">' + createFormItem('file', '2') + '<br/>' +'</el-col>' +
                    '<el-col :xs="24" :md="8">' + createFormItem('file', '3') + '<br/>' +'</el-col>' +
                    '</el-row>' +
                    createSeparatorRow() +

                    '<el-row>' +
                    '<el-col :sm="24" :lg="24">' +
                    createFormItem('checkbox', 'isPrivacyPolicyAgreed') +
                    
                    '</el-col>' +
                    '</el-row>' +
                    createSeparatorRow() +

                    '<el-row>' +
                    '<el-col :sm="24" :lg="24">' + createFormItem('submit') + '</el-col>' +
                    '</el-row>'

                break;
        }

    } else {

        var brand = g_Settings.brand
        if (!brand)
            brand =  "McCain"

        switch (g_Settings.segment) {
            case 'Retail':

                switch (g_Settings.region) {
                    case 'NORTH AMERICA':

                        var upcExampleImageUrl = 'https://www.mccain.com/upc-united-states.jpg'
                        var productionCodeExampleImageUrl = 'https://www.mccain.com/production-code-united-states.jpg'

                        if (g_Settings.country == 'Canada') {
                            upcExampleImageUrl = 'https://www.mccain.com/upc-canada.jpg'
                            productionCodeExampleImageUrl = 'https://www.mccain.com/production-code-canada.jpg'
                        }

                        formHTMLTemplate = '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + partializeFormItem(createFormItem('select', 'title'), 25) + partializeFormItem(createFormItem('text', 'firstName'), 75) + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'lastName') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'addressLine1') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'addressLine2') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'city') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + partializeFormItem(createFormItem('select', 'province'), 70) +  partializeFormItem(createFormItem('text', 'postalCode'), 30) + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'emailAddress') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">'  + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + partializeFormItem(createFormItem('text', 'phoneNumber'), 70) + partializeFormItem(createFormItem('text', 'phoneNumberExt'), 30) + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'cellphoneNumber') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-separator"></div>' + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'subject') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'productName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'batchID', { tooltip: { title: 'See example', content: '<img src="' + productionCodeExampleImageUrl + '" />' } }) + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'upc', { tooltip: { title: 'See example', content: '<img src="' + upcExampleImageUrl + '" />' } }) + '</el-col>' +
                            '<el-col :sm="12" :lg="7">' + createFormItem('date', 'expiryDate') + '</el-col>' +
                            '<el-col :sm="12" :lg="5">' + createFormItem('number', 'purchasePrice') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'storeName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'storeLocation') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('checkbox', 'firstTimePurchase') + '</el-col>' +
                            '</el-row>' +
                            createSeparatorRow() +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('textarea', 'comments') + '</el-col>' +
                            '</el-row>' +
                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Image Files\') }}</h4><p>{{ translate(\'You can upload 3 images with your enquiry (the total size may not exceed 12MB)\') }}</p></div>' + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="8">' + createFormItem('file', '1') + '</el-col>' +
                            '<el-col :sm="24" :lg="8">' + createFormItem('file', '2') + '</el-col>' +
                            '<el-col :sm="24" :lg="8">' + createFormItem('file', '3') + '</el-col>' +
                            '</el-row>' +
                            createSeparatorRow() +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('submit') + '</el-col>' +
                            '</el-row>'

                        break

                    case 'GB':

                        var productNameExampleImageUrl = 'https://www.mccain.com/images/global-contact-form/product-name-UK.jpg'
                        var productionCodeExampleImageUrl = 'https://www.mccain.com/images/global-contact-form/production-code-UK.jpg'
                        var bestBeforeExampleImageUrl = 'https://www.mccain.com//images/global-contact-form/best-before-UK.jpg'
                        var useByExampleImageUrl = 'https://www.mccain.com/images/global-contact-form/use-by-UK.jpg'

                        var tooltipLayoutTemplate = '<div class="gcf-tooltip-body"><el-row :gutter="10">' +
                            '<el-col :span="16">{0}</el-col>' +
                            '<el-col :span="8">{1}</el-col>' +
                        '</el-row></div>'

                        formHTMLTemplate = '<el-row>' +
                            '<el-col :sm="24" :lg="12">' +
                                createFormItem('select', 'subject') +
                                createFormItem('textarea', 'comments') +

                                createFormItem('radio', 'firstTimePurchase') +
                            '</el-col>' +

                            '<el-col :sm="24" :lg="12">' +
                            createFormItem('text', 'productName', { tooltip: { title: '<i class="el-icon-question"></i>', content: tooltipLayoutTemplate.format('<p class="gcf-tooltip-title">Product Name</p><p>Please state the full product name located on the front of the pack indicating the bag size and cut type e.g. Home Chip Crinkle Cut 1.5kg</p>', '<img src="' + productNameExampleImageUrl + '" />') } }) +
                            createFormItem('text', 'batchID', { tooltip: { title: '<i class="el-icon-question"></i>', content: tooltipLayoutTemplate.format('<p class="gcf-tooltip-title">Quality Number</p><p>Please state the Quality Number located on the back of the pack in the best before panel.</p>', '<img src="' + productionCodeExampleImageUrl + '" />') } }) +
                            partializeFormItem(createFormItem('date', 'expiryDate', { type: 'month', tooltip: { title: '<i class="el-icon-question"></i>', content: tooltipLayoutTemplate.format('<p class="gcf-tooltip-title">Best Before</p><p> Please state the best before end date as shown on the back of the pack e.g. 03/2020.</p>', '<img src="' + bestBeforeExampleImageUrl + '" />') } }), 50) +
                            partializeFormItem(createFormItem('date', 'useByDate', { tooltip: { title: '<i class="el-icon-question"></i>', content: tooltipLayoutTemplate.format('<p class="gcf-tooltip-title">Use by</p><p> Please state the Use By Date located on either the bag or the cardboard sleeve e.g. 15/02/2020.</p>', '<img src="' + useByExampleImageUrl + '" />') } }), 50) +

                            partializeFormItem(createFormItem('text', 'storeName', { tooltip: { title: '<i class="el-icon-question"></i>', content: '<div class="gcf-tooltip-body"><p>Store Name (where product was purchased)</p></div>' } }), 50) +
                            partializeFormItem(createFormItem('text', 'storeLocation'), 50) +


                            '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Image Files\') }}</h4><p>{{ translate(\'Please attach a clear photo of the best before panel/quality number box on your pack, along with photos of the affected product. You can attach three photos in total to support your enquiry. (The total size must not exceed 12MB)\') }}</p></div>' + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="8">' + createFormItem('file', '1', { label: 'Add photo' }) + '</el-col>' +
                            '<el-col :sm="24" :lg="8">' + createFormItem('file', '2', { label: 'Add photo' }) + '</el-col>' +
                            '<el-col :sm="24" :lg="8">' + createFormItem('file', '3', { label: 'Add photo' }) + '</el-col>' +
                            '</el-row>' +

                            '<br />' +
                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Contact Details\') }}</h4>' + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + partializeFormItem(createFormItem('select', 'title'), 25) + partializeFormItem(createFormItem('text', 'firstName'), 75) + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'lastName') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'emailAddress') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + partializeFormItem(createFormItem('text', 'phoneNumber'), 50) + partializeFormItem(createFormItem('text', 'cellphoneNumber'), 50) + '</el-col>' +
                            
                            '</el-row>' +

                            '<el-row>' +

                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'preferredContactMode') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + '</el-col>' +

                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' +
                            
                            createFormItem('checkbox', 'isPrivacyPolicyAgreed') +
                            
                            '</el-col>' +
                            '</el-row>' +
                            

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('submit') + '</el-col>' +
                            '</el-row>'

                        break

                        break

                    case 'LATAM':

                        formHTMLTemplate = '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'subject') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'province') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'firstName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'lastName') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'phoneNumber') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'emailAddress') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('textarea', 'comments') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h3>{{ translate(\'If your enquiry relates to a ' + brand + ' product you have purchased please provide the following information (if available)\') }}</h3></div>' + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'productName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'batchID') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('date', 'expiryDate') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'storeName') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Image Files\') }}</h4><p>{{ translate(\'You can upload 2 images with your enquiry (the total size may not exceed 12MB)\') }}</p></div>' + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '1') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '2') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('submit') + '</el-col>' +
                            '</el-row>'

                        break

                    default:

                        formHTMLTemplate = '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'subject') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'province') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'businessName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + partializeFormItem(createFormItem('select', 'title'), 25) + partializeFormItem(createFormItem('text', 'firstName'), 75) + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'lastName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'cellphoneNumber') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'phoneNumber') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'emailAddress') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('textarea', 'comments') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h3>{{ translate(\'If your enquiry relates to a ' + brand + ' product you have purchased please provide the following information (if available)\') }}</h3></div>' + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'productName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'batchID') + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('date', 'expiryDate') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'storeName') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Image Files\') }}</h4><p>{{ translate(\'You can upload 2 images with your enquiry (the total size may not exceed 12MB)\') }}</p></div>' + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '1') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '2') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('submit') + '</el-col>' +
                            '</el-row>'

                        break
                }
                    
                break

            case 'Foodservice':
                switch (g_Settings.region) {
                    case 'GB':

                        formHTMLTemplate = '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'businessName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'postalCode') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + partializeFormItem(createFormItem('select', 'title'), 25) + partializeFormItem(createFormItem('text', 'firstName'), 75) + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'lastName') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('text', 'emailAddress') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'cellphoneNumber') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'phoneNumber') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'distributorName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'distributorEmailAddress') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('select', 'subject') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'productName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'batchID') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('date', 'expiryDate') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('number', 'qtyOnHand') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'uom') + '</el-col>' +

                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('textarea', 'comments') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Image Files\') }}</h4><p>{{ translate(\'You can upload 2 images with your enquiry (the total size may not exceed 12MB)\') }}</p></div>' + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '1') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '2') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('submit') + '</el-col>' +
                            '</el-row>'

                        break

                    case 'Celavita':

                        formHTMLTemplate = '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'businessName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'postalCode') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' +
                            wrapContentWithIfCondition('isNotInRegions([ \'LATAM\' ])', partializeFormItem(createFormItem('select', 'title'), 25) + partializeFormItem(createFormItem('text', 'firstName'), 75)) +
                            wrapContentWithIfCondition('isInRegions([ \'LATAM\' ])', createFormItem('text', 'firstName')) +
                            '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'lastName') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('text', 'emailAddress') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + wrapContentWithIfCondition('isNotInRegions([ \'LATAM\' ])', createFormItem('text', 'cellphoneNumber')) + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'phoneNumber') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'distributorName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'distributorEmailAddress') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('select', 'subject') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'productName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'batchID') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('date', 'expiryDate') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('number', 'qtyOnHand') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'uom') + '</el-col>' +

                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('textarea', 'comments') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Image Files\') }}</h4><p>{{ translate(\'You can upload 2 images with your enquiry (the total size may not exceed 12MB)\') }}</p></div>' + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '1') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '2') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('submit') + '</el-col>' +
                            '</el-row>'

                        break

                    default:

                        formHTMLTemplate = '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('text', 'businessName') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'province') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'postalCode') + '</el-col>' +
                            '</el-row>' +

                            // wrapContentWithIfCondition('isNotInRegions([ \'South Africa\' ])', createFormItem('select', 'caseBrand')) +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' +
                                wrapContentWithIfCondition('isNotInRegions([ \'LATAM\' ])', partializeFormItem(createFormItem('select', 'title'), 25) + partializeFormItem(createFormItem('text', 'firstName'), 75)) +
                                wrapContentWithIfCondition('isInRegions([ \'LATAM\' ])', createFormItem('text', 'firstName')) +
                            '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'lastName') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('text', 'emailAddress') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + wrapContentWithIfCondition('isNotInRegions([ \'LATAM\' ])', createFormItem('text', 'cellphoneNumber')) + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'phoneNumber') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'distributorName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'distributorEmailAddress') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('select', 'subject') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'productName') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('text', 'batchID') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('date', 'expiryDate') + '</el-col>' +
                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('number', 'qtyOnHand') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('select', 'uom') + '</el-col>' +

                            '</el-row>' +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('textarea', 'comments') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + '<div class="gcf-section-title"><h4>{{ translate(\'Image Files\') }}</h4><p>{{ translate(\'You can upload 2 images with your enquiry (the total size may not exceed 12MB)\') }}</p></div>' + '</el-col>' +
                            '</el-row>' +
                            '<el-row>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '1') + '</el-col>' +
                            '<el-col :sm="24" :lg="12">' + createFormItem('file', '2') + '</el-col>' +
                            '</el-row>' +

                            createSeparatorRow() +

                            '<el-row>' +
                            '<el-col :sm="24" :lg="24">' + createFormItem('submit') + '</el-col>' +
                            '</el-row>'

                        break
                }
                
                break
        }
    }

    // wrap with form element
    formHTMLTemplate = '<el-form ref="contactForm" class="gcf-form" v-bind:model="contactForm" :validate-on-rule-change="false" action="/" v-bind:rules="rules" method="POST" onsubmit="return false">' +
        formHTMLTemplate +
        '</el-form>'

    Vue.component('contact-form', {
        props: {
            buttonTitle: {
                type: String,
                default: 'Submit'
            },

            profile: String,
            formtype: String,
            segment: String,
            region: String,
            subregion: String,
            country: String,
            brand: String,
            locale: String,
            baseurl: String,
            redirecturl: String,
            cssclass: String,
            showconfirmationpopup: {
                type: Boolean,
                default: true
            }

        },
        template: '<div v-loading.fullscreen="loading" v-bind:class="[cssclass, \'mccain-gcf-inner\', formatGcfCssClassByFormType(formtype), formatGcfCssClass(segment), \'gcf-\' + country.toLowerCase().replaceAll(\' \', \'-\') ] ">' + formHTMLTemplate + '</div>',
        data: function () {
            return {
                loading: false,
                formCompleted: false,

                contactForm: {
                    webSource: '',
                    formType: '',

                    contactChannel: '',
                    contactChannelDetail: '',
                    region: '',
                    subRegion: '',
                    country: '',
                    segment: '',

                    subject: '',

                    businessName: '',
                    title: '',
                    firstName: '',
                    lastName: '',
                    phoneNumber: '',
                    phoneNumberExt: '',
                    cellphoneNumber: '',
                    emailAddress: '',
                    comments: '',
                    province: '',
                    postalCode: '',

                    distributorName: '',
                    distributorEmailAddress: '',

                    productName: '',
                    batchID: '',
                    storeName: '',
                    expiryDate: '',
                    qtyOnHand: null,
                    uom: '',

                    addressLine1: '',
                    addressLine2: '',
                    city: '',
                    faxNumber: '',
                    contactName: '',
                    personCompletingFormName: '',
                    personCompletingFormEmail: '',
                    personCompletingFormType: '',
                    brokerageFirm: '',
                    personCompletingFormNotification: false,
                    division: '',
                    plant: '',
                    receivedDate: '',
                    foodSafetyIssue: false,
                    consumerAffairsCommunication: '',
                    preferredContactMode: '',
                    isExport: false,
                    specialInstruction: '',
                    priority: '',
                    caseBrand: '',
                    language: '',
                    invoiceNumber: '',
                    deliveryNumber: '',
                    purchaseOrderNumber: '',

                    isPrivacyPolicyAgreed: false
                },
                fileList1: [],
                fileList2: [],
                fileList3: [],

                options: {
                    region: [],
                    subRegion: [],
                    country: [],
                    province: [],
                    subject: [],
                    title: [],
                    uom: [],
                    caseBrand: [],
                    reimbursement: []
                },


                reqFields: {
                    productName: false,
                    country: true,
                    province: true,
                    postalCode: true,
                    businessName: true,
                    addressLine1: true,
                    city: true,
                    firstName: true
                },

                attachmentsMaxSize: 12, // Mb

                messages: {
                    'fr': {
                        'Subject': 'Assujettir',
                        'Business Name': 'Nom de l\'entreprise',
                        'Title': 'Titre',
                        'First Name': 'Prénom',
                        'Last Name': 'Nom de famille',
                        'Telephone': 'Téléphone',
                        'Mobile Phone Number': 'Numéro de téléphone portable',
                        'Email Address': 'Adresse électronique',
                        'Message': 'Message',
                        'Province': 'Province',
                        'Postal Code': 'Code Postal',
                        'Distributor Name': 'Nom du distributeur',
                        'Distributor Email Address': 'Adresse email du distributeur',
                        'Product Name': 'Nom du produit',
                        'Batch ID': 'ID de lot',
                        'Store Name': 'Nom du magasin',
                        'Expiry Date': 'Date d\'expiration',
                        'Qty on hand at time of complaint': 'Quantité disponible au moment de la plainte',
                        'Unit of Measure': 'Unité de mesure',
                        'Add photo (optional)': 'Ajouter une photo (facultatif)',
                        'This field': 'Ce champ',
                        'is required': 'est obligatoire',
                        'Submit': 'Soumettre',
                        'Please enter a value greater than 0': 'Veuillez entrer une valeur supérieure à 0',
                        'Please input correct Email Address': 'Veuillez saisir l\'adresse e-mail correcte',

                        'Enquiry': 'Enquête',
                        'Compliment': 'Compliment',
                        'Complaint': 'Plainte',

                        'If your enquiry relates to a McCain product you have purchased please provide the following information (if available)': 'Si votre demande concerne un produit McCain que vous avez acheté, veuillez fournir les informations suivantes (le cas échéant)',
                        'Image Files': 'Fichiers d\'image',
                        'You can upload 2 images with your enquiry (the total size may not exceed 12MB)': 'Vous pouvez télécharger 2 images avec votre demande (la taille totale ne doit pas dépasser 12 Mo)'
                    },
                    'es': {
                        'Subject': 'Tema',
                        'Business Name': 'Nombre del Negocio',
                        'Title': 'Título',
                        'First Name': 'Primer nombre',
                        'Last Name': 'Apellido',
                        'Telephone': 'Teléfono',
                        'Mobile Phone Number': 'Número de teléfono móvil',
                        'Email Address': 'Dirección de correo electrónico',
                        'Message': 'Mensaje',
                        'Province': 'Estado',
                        'Postal Code': 'Código postal',
                        'Distributor Name': 'Nombre del distribuidor',
                        'Distributor Email Address': 'Dirección de correo electrónico del distribuidor',
                        'Product Name': 'Nombre del producto',
                        'Batch ID': 'ID del lote',
                        'Store Name': 'Nombre de la tienda',
                        'Expiry Date': 'Fecha de caducidad',
                        'Qty on hand at time of complaint': 'Cantidad disponible al momento de la queja',
                        'Unit of Measure': 'Unidad de medida',
                        'Add photo (optional)': 'Añadir foto (opcional)',
                        'This field': 'Este campo',
                        'is required': 'es requerido',
                        'Submit': 'Enviar',
                        'Please enter a value greater than 0': 'Por favor, introduzca un valor mayor que 0',
                        'Please input correct Email Address': 'Por favor ingrese la dirección de correo electrónico correcta',

                        'Enquiry': 'Investigación',
                        'Compliment': 'Sugerencia',
                        'Complaint': 'Queja',
                        'Query': 'Pregunta',

                        'If your enquiry relates to a McCain product you have purchased please provide the following information (if available)': 'Si su consulta se relaciona con un producto McCain que ha comprado, proporcione la siguiente información (si está disponible)',
                        'Image Files': 'Archivos de imagen',
                        'You can upload 2 images with your enquiry (the total size may not exceed 12MB)': 'Puede cargar 2 imágenes con su consulta (el tamaño total no puede exceder los 12 MB)',

                        'Type of Enquiry': 'Motivo de contacto',
                        'Alternate Phone Number': 'Número de teléfono alternativo',
                        'Contact Phone Number': 'Número de teléfono de contacto',
                        'Your Comments': 'Tus comentarios',
                        'Product Name & Pack Size': 'Nombre y peso del producto',
                        'Batch ID (on back of pack)': 'Número de lote (en el empaque)',
                        'Expiry Date (on back of pack)': 'Fecha de caducidad  (en el empaque)',
                        'Store Name (where product was purchased)': 'Nombre de la tienda (en donde compró el producto)',
                        'Business Name (if applicable)': 'Nombre comercial (si aplica)'
                    },
                    'nl': {
                        'Subject': 'Onderwerp',
                        'Business Name': 'Business',
                        'Title': 'Titel',
                        'First Name': 'Voornaam',
                        'Last Name': 'Achternaam',
                        'Telephone': 'Telefoonnummer',
                        'Mobile Phone Number': 'Mobiele telefoonnummer',
                        'Email Address': 'Email',
                        'Message': 'Bericht',
                        'Province': 'Provincie',
                        'Postal Code': 'Postcode',
                        'Distributor Name': 'Naam distributeur',
                        'Distributor Email Address': 'E-mailadres distributeur',
                        'Product Name': 'Productnaam',
                        'Batch ID': 'Lotnummer',
                        'Store Name': 'Winkel naam',
                        'Expiry Date': 'Vervaldatum',
                        'Qty on hand at time of complaint': 'Aantal bij de hand op het moment van de klacht',
                        'Unit of Measure': 'Maateenheid',
                        'Add photo (optional)': 'Foto toevoegen (optioneel)Ajouter une photo (facultatif)',
                        'This field': 'Dit veld',
                        'is required': 'verplicht',
                        'Submit': 'Versturen',
                        'Please enter a value greater than 0': 'Voer een waarde in die groter is dan 0',
                        'Please input correct Email Address': 'Voer het juiste e-mailadres in',

                        'Enquiry': 'Aanvraag',
                        'Compliment': 'Compliment',
                        'Complaint': 'Klacht',

                        'If your enquiry relates to a McCain product you have purchased please provide the following information (if available)': 'Gelieve voor uw vraag de volgende gegevens aan te leveren',
                        'Image Files': 'Afbeeldingen',
                        'You can upload 2 images with your enquiry (the total size may not exceed 12MB)': 'U kunt 2 afbeeldingen bijvoegen (max. 12 MB)',

                        'Type of Enquiry': 'Type aanvraag',
                        'Alternate Phone Number': 'Alternatief telefoonnummer',
                        'Contact Phone Number': 'Telefoonnummer',
                        'Your Comments': 'Opmerking',
                        'Product Name & Pack Size': 'Productnaam',
                        'Batch ID (on back of pack)': 'Lotnummer (onderaan verpakking)',
                        'Expiry Date (on back of pack)': 'Houdbaarheidsdatum',
                        'Store Name (where product was purchased)': 'Winkelnaam (waar is het product gekocht)',
                        'Business Name (if applicable)': 'Bedrijfsnaam'
                    },
                }
            }
        },
        computed: {
            

            rules: function () {
                var self = this

                var result = {
                    segment: [
                        { required: true, message: self.getFieldTitle('segment') + ' ' + self.translate('is required') }
                    ],

                    subject: [
                        { required: true, message: self.getFieldTitle('subject') + ' ' + self.translate('is required') }
                    ],

                    province: [
                        { required: this.reqFields.province, message: self.getFieldTitle('province') + ' ' + self.translate('is required') }
                    ],

                    postalCode: [
                        { required: this.reqFields.postalCode, message: self.getFieldTitle('postalCode') + ' ' + self.translate('is required') }
                    ],

                    businessName: [
                        { required: this.reqFields.businessName, message: self.getFieldTitle('businessName') + ' ' + self.translate('is required') }
                    ],

                    contactName: [
                        { required: false, message: self.getFieldTitle('contactName') + ' ' + self.translate('is required') }
                    ],

                    firstName: [
                        { required: this.reqFields.firstName, message: self.getFieldTitle('firstName') + ' ' + self.translate('is required')  }
                    ],

                    lastName: [
                        { required: true, message: self.getFieldTitle('lastName') + ' ' + self.translate('is required') }
                    ],

                    cellphoneNumber: [
                        { required: (self.region == 'South Africa'), message: self.getFieldTitle('cellphoneNumber') + ' ' + self.translate('is required') }
                    ],

                    phoneNumber: [
                        { required: (self.segment == 'Retail' || self.formtype == 'BrokerPortal'), message: self.translate('This field') + ' ' + self.translate('is required') }
                    ],

                    emailAddress: [
                        { required: true, message: self.getFieldTitle('emailAddress') + ' ' + self.translate('is required') },
                        { type: 'email', message: self.translate('Please input correct Email Address'), trigger: ['blur', 'change'] }
                    ],

                    comments: [
                        { required: true, message: self.translate('This field') + ' ' + self.translate('is required') }
                    ],

                    distributorEmailAddress: [
                        { type: 'email', message: self.translate('Please input correct Email Address'), trigger: ['blur', 'change'] }
                    ],

                    productName: [
                        { required: this.reqFields.productName, message: self.getFieldTitle('productName') + ' ' + self.translate('is required') }
                    ],

                    batchID: [
                        { required: this.formtype == 'BrokerPortal' && this.reqFields.productName, message: self.getFieldTitle('batchID') + ' ' + self.translate('is required') }
                    ],

                    qtyOnHand: [
                        { required: !self.formtype && this.reqFields.productName, message: self.getFieldTitle('qtyOnHand') + ' ' + self.translate('is required') },
                        { type: 'number', min: !self.formtype && this.reqFields.productName ? 0.1 : 0, message: self.translate('Please enter a value greater than 0') }
                    ],

                    uom: [
                        { required: !self.formtype && this.reqFields.productName, message: self.getFieldTitle('uom') + ' ' + self.translate('is required') }
                    ],

                    personCompletingFormName: [
                        { required: (self.formtype == 'FirstNotification'), message: self.getFieldTitle('personCompletingFormName') + ' ' + self.translate('is required') }
                    ],

                    personCompletingFormEmail: [
                        { required: (self.formtype == 'FirstNotification'), message: self.getFieldTitle('personCompletingFormEmail') + ' ' + self.translate('is required') },
                        { type: 'email', message: self.translate('Please input correct Email Address'), trigger: ['blur', 'change'] }
                    ],

                    personCompletingFormType: [
                        { required: false, message: self.getFieldTitle('personCompletingFormType') + ' ' + self.translate('is required') }
                    ],

                    addressLine1: [
                        { required: this.reqFields.addressLine1, message: self.getFieldTitle('addressLine1') + ' ' + self.translate('is required') }
                    ],

                    city: [
                        { required: this.reqFields.city, message: self.getFieldTitle('city') + ' ' + self.translate('is required') }
                    ],

                    brokerageFirm: [
                        { required: this.formtype == 'BrokerPortal', message: self.getFieldTitle('brokerageFirm') + ' ' + self.translate('is required') }
                    ],

                    complaintCode: [
                        { required: false, message: self.getFieldTitle('complaintCode') + ' ' + self.translate('is required') }
                    ],

                    region: [
                        { required: true, message: self.getFieldTitle('region') + ' ' + self.translate('is required') }
                    ],

                    subRegion: [
                        { required: true, message: self.getFieldTitle('subRegion') + ' ' + self.translate('is required') }
                    ],

                    country: [
                        { required: this.reqFields.country, message: self.getFieldTitle('country') + ' ' + self.translate('is required') }
                    ],

                    priority: [
                        { required: true, message: self.getFieldTitle('priority') + ' ' + self.translate('is required') }
                    ],

                    isPrivacyPolicyAgreed: [
                        { validator: self.validateIfChecked, message: 'Global Privacy Policy is required' }
                    ]
                }

                return result
            }
        },
        created: function () {
            var self = this

            self.contactForm.segment = self.segment

            self.options.region = createSelectDataSourceFromLabels(g_OptionsRepository.regions)

            if (self.region)
                self.contactForm.region = self.region
            else
                self.contactForm.region = self.country

            self.contactForm.subRegion = self.subregion
            self.contactForm.country = self.country
            self.contactForm.formType = self.formtype

            self.options.country = createSelectDataSourceFromLabels(g_OptionsRepository.getCountries(self.region, self.subregion)) 

            if (self.country && g_OptionsRepository.countries[self.country])
                self.options.province = createSelectDataSourceFromLabels(g_OptionsRepository.countries[self.country].states)
            else
                self.options.province = []

            self.options.subject = createSelectDataSourceFromLabels(g_OptionsRepository.getSubjects(self.region, self.subregion, self.formtype))

            self.options.language = createSelectDataSourceFromLabels(['', 'Dutch', 'English', 'French', 'German', 'Italian', 'Spanish'])
            self.options.reimbursement = createSelectDataSourceFromLabels(['', 'Yes', 'No', 'Unknown' ])

            self.options.title = createSelectDataSourceFromLabels(['', 'Mr.', 'Mrs.', 'Ms.', 'Miss.'])
            self.options.uom = g_OptionsRepository.getUOM(self.formtype, self.region)

            self.options.segment = createSelectDataSourceFromLabels(g_OptionsRepository.getSegments(self.region))

            self.options.personCompletingFormType = createSelectDataSourceFromLabels(['Broker', 'Sales Person', 'Other'])

            if (self.contactForm.personCompletingFormType && self.contactForm.personCompletingFormType !== 'Other')
                self.options.brokerageFirm = createSelectDataSourceFromLabels(g_OptionsRepository.firms)
            else
                self.options.brokerageFirm = []

            self.options.division = createSelectDataSourceFromLabels(g_OptionsRepository.getDivisions(self.region, self.country))

            self.options.priority = createSelectDataSourceFromLabels(['Low', 'Medium', 'High'])

            self.options.complaintCode = createSelectDataSourceFromLabels(g_OptionsRepository.complaintCodes)

            self.options.plant = createSelectDataSourceFromLabels(g_OptionsRepository.getPlants(self.region))

            self.options.consumerAffairsCommunication = createSelectDataSourceFromLabels(g_OptionsRepository.getConsumerAffairsCommunication())

            self.options.preferredContactMode = createSelectDataSourceFromLabels(g_OptionsRepository.getPreferredContactMode())

            self.options.caseBrand = createSelectDataSourceFromLabels(g_OptionsRepository.getCaseBrands(self.region))

            // default values for FirstNotification and BrokerPortal forms
            if (self.formtype == 'BrokerPortal') {
                self.contactForm.segment = 'Foodservice'
                self.contactForm.region = 'NORTH AMERICA'
                self.handlePropChange('region')

                self.contactForm.subRegion = 'US'

                self.contactForm.country = 'United States'
                self.handlePropChange('country')
            }

            // default case details values from g_Settings
            if (g_Settings && g_Settings.caseDetails) {
                var props = ['caseBrand', 'contactChannel', 'contactChannelDetail', 'language']

                for (var i = 0; i < props.length; i++) {
                    var propName = props[i]
                    if (g_Settings.caseDetails[propName])
                        self.contactForm[propName] = g_Settings.caseDetails[propName]
                }
            }
            
            // restore values from cookie cache
            if (self.formtype === 'FirstNotification') {
                var strCachedData = g_CookieUtils.getCookie('mccain.GCF.cachedData')

                if (strCachedData) {
                    try {
                        var cachedData = JSON.parse(strCachedData)
                        if (cachedData) {
                            self.contactForm.region = cachedData.region
                            self.contactForm.personCompletingFormName = cachedData.personCompletingFormName
                            self.contactForm.personCompletingFormEmail = cachedData.personCompletingFormEmail

                            self.handlePropChange('region')
                        }
                    } catch (e) {
                        console.log(e)
                    }
                }
            }

            // attachments max size
            if (self.region == 'GB' && self.country == 'United Kingdom') {
                self.attachmentsMaxSize = 12
            }

            self.contactForm.webSource = window.location.href

            self.calculateIfFieldIsRequired('productName')
            self.calculateIfFieldIsRequired('businessName')
            self.calculateIfFieldIsRequired('province')
        },
        mounted: function () {
            var self = this
        },
        methods: {
            submitForm: function () {
                var self = this

                var form = self.$refs['contactForm'];

                form.validate(function (valid, fields) {
                    if (valid) {

                        self.loading = true

                        self.contactForm.subRegion = self.getSubRegion()

                        var formData = new FormData(form.$el)
                        objectToFormData(self.contactForm, formData)

                        // save to cookie cache
                        if (self.formtype === 'FirstNotification') {
                            var cachedData = {
                                region: self.contactForm.region,
                                personCompletingFormName: self.contactForm.personCompletingFormName,
                                personCompletingFormEmail: self.contactForm.personCompletingFormEmail
                            }

                            g_CookieUtils.setCookie('mccain.GCF.cachedData', JSON.stringify(cachedData), 30)
                        }

                        var xhr = new XMLHttpRequest()
                        
                        //application/x-www-form-urlencoded;charset=UTF-8
                        xhr.onreadystatechange = function() {
                            var C_Generic_ErrorMessage = 'Something went wrong. Please try again later.'
                            var apiErrorMessage = ''

                            if (xhr.readyState === 4) {

                                var success = false
                                
                                if (xhr.status == 200) {
                                    var responseData = JSON.parse(xhr.responseText)

                                    if (responseData.success && responseData.external_result) {
                                        var er = responseData.external_result
                                        if (er.compositeResponse && er.compositeResponse[0].body) {
                                            var crStatusCode = er.compositeResponse[0].httpStatusCode
                                            var crBody = er.compositeResponse[0].body

                                            if (crBody.id) {
                                                // success
                                                success = true
                                            } else if (crStatusCode == 400 && Array.isArray(crBody)) {
                                                // error
                                                apiErrorMessage += '<ul>'
                                                for (var i = 0; i < crBody.length; i++) {
                                                    apiErrorMessage += '<li>' + crBody[i].message + '</li>'
                                                }
                                                apiErrorMessage += '</ul>'
                                            }
                                        }
                                    }

                                    if (success) {
                                        form.resetFields();
                                        if (self.$refs.photoUpload1)
                                            self.$refs.photoUpload1.clearFiles()

                                        if (self.$refs.photoUpload2)
                                            self.$refs.photoUpload2.clearFiles()

                                        if (self.$refs.photoUpload3)
                                            self.$refs.photoUpload3.clearFiles()

                                        self.loading = false

                                        self.formCompleted = true

                                        if (self.showconfirmationpopup) {
                                            self.$alert('We\'ll get back to you soon', 'Thank you for contacting us', {
                                                confirmButtonText: 'back to site',
                                                confirmButtonClass: 'gcf-button gcf-button-close',
                                                center: true,
                                                dangerouslyUseHTMLString: true,
                                                lockScroll: false,
                                                close: function () {
                                                    if (self.redirecturl)
                                                        window.location.href = self.redirecturl
                                                    else
                                                        window.location.reload()
                                                }
                                            })
                                        } else {
                                            if (self.redirecturl)
                                                window.location.href = self.redirecturl
                                            else
                                                window.location.reload()
                                        }
                                    } else {
                                        self.loading = false

                                        if (self.formtype && apiErrorMessage) {
                                            var detailedErrorMessage = 'An unexpected error occurred. Error details:' + '<br/><br/>' + apiErrorMessage

                                            self.$message({
                                                showClose: true,
                                                message: detailedErrorMessage,
                                                dangerouslyUseHTMLString: true,
                                                duration: 7500,
                                                customClass: 'gcf-alert-content',
                                                type: 'error'
                                            });
                                        } else {
                                            self.$message.error(C_Generic_ErrorMessage)
                                        }
                                    }

                                    
                                }
                                else {
                                    // unexpected error
                                    self.loading = false

                                    self.$message.error(C_Generic_ErrorMessage)
                                }

                                self.$emit('completed', { status: success ? 'success' : 'failed' })
                            } 
                        }

                        var postUrl = self.baseurl + '/api/contactapi/post'
                        if (self.profile)
                            postUrl += '?profile=' + self.profile

                        xhr.open('POST', postUrl, true)
                        xhr.send(formData);

                    } else {
                        Vue.nextTick(function () {
                            var elItem = form.$el.querySelector('.is-error')

                            if (elItem) {
                                var input = elItem.querySelector('input, textarea')
                                if (input)
                                    input.focus()
                            }
                        });

                        return false
                    }
                })
            },

            getFieldLabel: function (prop) {
                var self = this

                var result = prop

                switch (prop) {
                    case 'segment':
                        result = 'Business Segment'
                        break
                    case 'businessName':
                        if (self.formtype == 'FirstNotification') {
                            result = 'Customer Account Name (not for Retail consumer)'
                        } else if (self.formtype == 'BrokerPortal') {
                            result = 'Customer Account Name'
                        }
                        else {

                            if (self.segment == 'Retail')
                                result = 'Business Name (if applicable)'
                            else
                                result = 'Business Name'
                        }
                        break
                    case 'province':

                        if (self.formtype == 'FirstNotification') {
                            result = 'Province/State'
                        } else {

                            if (self.country == 'United States' || self.country == 'Australia') {
                                result = 'State'
                            } else {
                                result = 'Province'
                            }
                        }

                        break
                    case 'postalCode':

                        if (self.formtype == 'FirstNotification') {
                            result = 'Postal/Zip Code'
                        } else {
                            if (self.country == 'United States') {
                                result = 'Zip Code'
                            } else {
                                result = 'Postal Code'
                            }
                        }

                        break
                    case 'title':
                        result = 'Title'
                        break
                    case 'firstName':
                        result = "First Name"
                        break
                    case 'lastName':
                        result = "Last Name"
                        break
                    case 'emailAddress':
                        result = "Email Address"
                        break
                    case 'phoneNumber':
                        result = "Contact Phone Number"
                        break
                    case 'phoneNumberExt':
                        result = 'Ext.'
                        break
                    case 'cellphoneNumber':
                        if (self.region == 'South Africa')
                            result = "Cellphone Number"
                        else
                            result = "Alternate Phone Number"
                        break
                    case 'distributorName':
                        result = "Distributor Name"
                        break
                    case 'distributorEmailAddress':
                        result = "Distributor Email Address"
                        break
                    case 'subject':
                        if (self.formtype)
                            result = "Contact Reason"
                        else
                            result = "Type of Enquiry"
                        break
                    case 'productName':
                        result = "Product Name & Pack Size"
                        break
                    case 'batchID':
                        if (self.formtype) {
                            var result = 'Production Code / Lot Code'

                            if (self.contactForm.region == 'LATAM') {
                                result = 'Production Code or Pack Code'
                            } else if (self.contactForm.region == 'GB') {
                                result = 'Production Code / Lot / Pack Code'
                            }

                        } else {
                            if (self.segment == 'Retail') {
                                if (self.region == 'GB')
                                    result =  'Best Before Panel / Quality Number Including Printed Time'
                                else
                                    result = "Batch ID (on back of pack)"
                            }
                            else {
                                result = 'Batch ID'
                            }
                        }
                        break
                    case 'storeName':
                        if (self.segment == 'Retail')
                            if (self.region == 'GB')
                                result = "Store Name"
                            else
                                result = "Store Name (where product was purchased)"
                        else
                            result = "Store Name"
                        break
                    case 'storeLocation':
                        result = "Store Location"
                        break
                    case 'purchasePrice':
                        result = "Purchase Price"
                        break
                    case 'expiryDate':
                        result = "Expiry Date"

                        if (self.formtype) {
                            switch (self.contactForm.region) {
                                case 'South Africa':
                                    result = 'Best Before'
                                case 'CEU':
                                case 'Celavita':
                                case 'GB':
                                    result = 'Best Before End'
                                    break
                            }
                        }
                        else {
                            if (self.segment == 'Retail') {
                                switch (self.contactForm.region) {
                                    case 'GB':
                                        result = "Best Before"
                                        break
                                    default:
                                        result = "Expiry Date (on back of pack)"
                                        break
                                }
                            }
                        }
                        break
                    case 'qtyOnHand':
                        result = "Qty on hand at time of complaint"

                        if (self.formtype) {
                            switch (self.contactForm.region) {
                                case 'CEU':
                                    result = "Quantity concerned"
                                    break
                            }
                        }

                        break
                    case 'uom':
                        result = "Unit of Measure"
                        break
                    case 'comments':
                        result = 'Your Comments'
                        break
                    case 'foodSafetyIssue':
                        result = 'Food Safety' 
                        break
                    case 'contactName':
                        result = 'Customer Contact'
                        break
                    case 'addressLine1':
                        result = 'Address Line 1'
                        break
                    case 'addressLine2':
                        result = 'Address Line 2'
                        break
                    case 'city':
                        result = 'City'
                        break
                    case 'faxNumber':
                        result = 'Fax Number'
                        break
                    case 'personCompletingFormName':
                        result = 'Name'
                        break
                    case 'personCompletingFormEmail':
                        result = 'Email Address'
                        break
                    case 'personCompletingFormType':
                        result = 'Type'
                        break
                    case 'brokerageFirm':
                        result = 'Brokerage Firm'
                        break
                    case 'personCompletingFormNotification':
                        result = 'Case Notification'
                        break
                    case 'division':
                        result = 'Division'
                        break
                    case 'complaintCode':
                        result = 'Complaint Code'
                        break
                    case 'plant':
                        result = 'Plant'
                        break
                    case 'receivedDate':
                        result = 'Date Received'
                        break
                    case 'useByDate':
                        result = 'Use By Date'
                        break
                    case 'consumerAffairsCommunication':
                        result = 'How would you like QA-Consumer Affairs to respond?'
                        break
                    case 'preferredContactMode':
                        result = 'How would you prefer us to contact you?'
                        break
                    case 'isExport':
                        result = 'Is Export?'
                        break
                    case 'specialInstruction':
                        result = 'Special Instructions'
                        break
                    case 'priority':
                        result = 'Priority'
                        break
                    case 'region':
                        result = 'Region'
                        break
                    case 'subRegion':
                        result = 'Sub-Region'
                        break
                    case 'country':
                        result = 'Country'
                        break
                    case 'upc':
                        if (self.formtype)
                            result = 'McCain Product Code (SKU/UPC)'
                        else
                            result = 'UPC'
                        break
                    case 'caseBrand':
                        result = 'Case Brand'
                        break
                    case 'language':
                        result = 'Language'
                        break
                    case 'invoiceNumber':
                        result = 'Invoice Number'
                        break
                    case 'deliveryNumber':
                        result = 'Delivery Number'
                        break
                    case 'purchaseOrderNumber':
                        result = 'Purchase Order Number'
                        break
                    case 'firstTimePurchase':
                        result = 'Is this the first time you have purchased this product?'
                        break
                    case 'isPrivacyPolicyAgreed':
                        result = 'Global Privacy Policy'
                        break
                    
                }

                result = self.translate(result)

                return result
            },

            getFieldTitle: function (prop) {
                var self = this

                var result = self.getFieldLabel(prop)
                

                switch (prop) {
                    case 'businessName':
                        if (self.formtype == 'FirstNotification') {
                            result = 'Customer Account Name'
                        }

                        break
                    case 'batchID':
                        if (!self.formtype) {
                            if (self.region == 'GB')
                                result = 'P12012024 16:20 05'
                            else 
                                result = "Batch ID"
                        } 
                        break
                    case 'storeName':
                        result = "Store Name"
                        break
                    case 'expiryDate':
                        result = "Expiry Date"
                        break
                    case 'consumerAffairsCommunication':
                        result = 'Please select'
                        break
                    case 'comments':
                        if (self.formtype) {
                            result = 'Detailed description of Contact Reason'
                        }
                        break
                    case 'firstTimePurchase':
                        result = 'Yes'
                        break
                    case 'personCompletingFormNotification':
                        result = 'Send Case Notification to this person'
                        break
                    case 'isPrivacyPolicyAgreed':
                        if (self.formtype == 'BrokerPortal') {
                            result = 'Please tick the box to confirm that you Agree the personal data will be handled in accordance with our <a target=\'_blank\' href=\'https://www.mccain.com/privacy/\'>Global Privacy Policy</a>.'
                        }
                        else if (self.formtype == 'FirstNotification') {
                            result = 'Please tick the box to confirm that when you are submitting personal data or information relating to another person, you have obtained the necessary consent to do so.'
                        }
                        else {
                            result = 'Your privacy is important to us. By submitting personal data or information to us, you agree this will be handled in accordance with our <a target="_blank" href="https://www.mccain.com/privacy/">Global Privacy Policy</a>.'
                        }
                        break
                }

                result = self.translate(result)

                return result
            },

            getSubRegion: function () {
                var self = this

                var result = self.contactForm.subRegion

                if (!result) {
                    result = g_OptionsRepository.getSubRegionByCountry(self.contactForm.region, self.contactForm.country)
                }

                return result
            },

            handleFileRemove1: function (file, fileList) {
                this.handleFileRemoveCommon(file, fileList, '1')
            },

            handleFileChange1: function (file, fileList) {
                this.handleFileChangeCommon(file, fileList, '1')
            },

            handleFileRemove2: function (file, fileList) {
                this.handleFileRemoveCommon(file, fileList, '2')
            },

            handleFileChange2: function (file, fileList) {
                this.handleFileChangeCommon(file, fileList, '2')
            },

            handleFileRemove3: function (file, fileList) {
                this.handleFileRemoveCommon(file, fileList, '3')
            },

            handleFileChange3: function (file, fileList) {
                this.handleFileChangeCommon(file, fileList, '3')
            },

            handleFileRemoveCommon: function (file, fileList, index) {
                document.getElementById('gcf-upload-photo' + index).getElementsByClassName('el-upload')[0].style.display = 'inline-block'
                document.getElementsByName('gcf-upload-photo-input' + index)[0].value = ''

                this['fileList' + index] = fileList
            },

            handleFileChangeCommon: function(file, fileList, index) {
                var self = this

                if (self.validateTotalFilesSize(file)) {
                    document.getElementById('gcf-upload-photo' + index).getElementsByClassName('el-upload')[0].style.display = 'none'
                } else {
                    fileList = []
                    self.$message.error('File(s) size exceeds ' + self.attachmentsMaxSize + ' MB')
                }

                this['fileList' + index] = fileList
            },

            validateTotalFilesSize: function(file) {
                var self = this

                var sum = file.size

                for (var i = 0; i < self.fileList1.length; i++) {
                    sum += self.fileList1[i].size
                }

                for (var i = 0; i < self.fileList2.length; i++) {
                    sum += self.fileList2[i].size
                }

                for (var i = 0; i < self.fileList3.length; i++) {
                    sum += self.fileList3[i].size
                }


                var fileSize = sum / 1024 / 1024 // Mb

                if (fileSize > self.attachmentsMaxSize)
                    return false
                else
                    return true
            },

            handlePropChange: function (propName) {

                // this event handler solves 2 problems
                // 1 - change what field is required, what not depends on selection
                // 2 - change options in other selects that depends on selection

                var self = this

                function triggerSegmentChange() {
                    self.calculateIfFieldIsRequired('businessName')
                    self.calculateIfFieldIsRequired('postalCode')
                    self.clearErrors()
                }

                function triggerSubjectChange() {
                    self.calculateIfFieldIsRequired('productName')
                    self.clearErrors()
                }

                function triggerPersonCompletingFormTypeChange() {
                    if (self.contactForm.personCompletingFormType && self.contactForm.personCompletingFormType !== 'Other')
                        self.options.brokerageFirm = createSelectDataSourceFromLabels(g_OptionsRepository.firms)
                    else
                        self.options.brokerageFirm = []

                    self.contactForm.brokerageFirm = ''
                }

                function triggerRegionChange() {
                    // segments
                    self.options.segment = createSelectDataSourceFromLabels(g_OptionsRepository.getSegments(self.contactForm.region))

                    // subRegion
                    var subRegions = g_OptionsRepository.getSubRegions(self.contactForm.region)
                    self.contactForm.subRegion = ''
                    self.options.subRegion = createSelectDataSourceFromLabels(subRegions)

                    // subject
                    self.options.subject = createSelectDataSourceFromLabels(g_OptionsRepository.getSubjects(self.contactForm.region, self.contactForm.subRegion, self.contactForm.formType))

                    // country
                    var countries = g_OptionsRepository.getCountries(self.contactForm.region, self.contactForm.subRegion)
                    self.contactForm.country = ''
                    self.options.country = createSelectDataSourceFromLabels(countries)
                    self.calculateIfFieldIsRequired('country')
                    if (countries && countries.length == 1) {
                        self.contactForm.country = countries[0]
                    }

                    // firstName
                    self.calculateIfFieldIsRequired('firstName')

                    // postalCode
                    self.calculateIfFieldIsRequired('postalCode')

                    self.calculateIfFieldIsRequired('addressLine1')
                    self.calculateIfFieldIsRequired('city')

                    // caseBrand
                    self.contactForm.caseBrand = ''
                    self.options.caseBrand = createSelectDataSourceFromLabels(g_OptionsRepository.getCaseBrands(self.contactForm.region))

                    // plants
                    self.contactForm.plant = ''
                    self.options.plant = createSelectDataSourceFromLabels(g_OptionsRepository.getPlants(self.contactForm.region))

                    // uom
                    self.contactForm.uom = ''
                    self.options.uom = g_OptionsRepository.getUOM(self.formtype, self.contactForm.region)

                    self.clearErrors()
                }

                function triggerCountryChange() {
                    var countryObj = g_OptionsRepository.countries[self.contactForm.country]

                    

                    // States
                    var states = []

                    if (countryObj) {
                        states = countryObj.states
                    }

                    self.contactForm.province = ''
                    self.options.province = createSelectDataSourceFromLabels(states)
                    self.calculateIfFieldIsRequired('province')

                    

                    // Division
                    self.contactForm.division = ''
                    self.options.division = createSelectDataSourceFromLabels(
                        g_OptionsRepository.getDivisions(self.contactForm.region, self.contactForm.country))

                    // clear IsExport
                    if (self.contactForm.region !== 'NORTH AMERICA')
                        self.contactForm.isExport = false

                    self.clearErrors()
                }

                switch (propName) {
                    case 'segment':
                        triggerSegmentChange()
                        break
                    case 'subject':
                        triggerSubjectChange()
                        break
                    case 'personCompletingFormType':
                        triggerPersonCompletingFormTypeChange()
                        break
                    case 'region':
                        triggerRegionChange()
                        triggerCountryChange()
                        break
                    case 'subRegion':
                        triggerCountryChange()
                        break
                    case 'country':
                        triggerCountryChange()
                        break
                    default:
                        break
                }

            },

            formatGcfCssClass: function (p) {
                var result = ''
                if (p)
                    result = 'gcf-' + p.toLowerCase()
                return result
            },

            formatGcfCssClassByFormType: function (p) {
                var result = ''
                if (p) {
                    result = 'gcf-' + p.toLowerCase()

                    // BrokerPortal should have same styles as FirstNotification
                    if (p == 'BrokerPortal') {
                        result = 'gcf-firstnotification'
                    }

                    
                }
                return result
            },

            translate: function (phrase) {
                var result = ''

                var self = this

                var dict = self.messages[self.locale]

                if (dict) {
                    result = dict[phrase]
                }

                if (!result)
                    result = phrase

                return result
            },

            calculateIfFieldIsRequired: function (fieldName) {
                var self = this

                var result = false

                switch (fieldName)
                {
                    case 'productName':
                        if (!self.formtype)
                            result = self.contactForm.segment === 'Foodservice' && self.contactForm.subject === 'Complaint'
                        else
                            result = true
                        break
                    case 'country':
                        result = self.options.country && self.options.country.length > 0
                        break
                    case 'province':
                        result = self.options.province && self.options.province.length > 0
                        break
                    case 'postalCode':
                        result = true

                        if (self.formtype) {
                            if (self.isInRegions(['LATAM'])) {
                                result = false
                            } else {
                                if (self.contactForm.segment === 'Retail')
                                    result = false
                            }
                        }
                        
                        break
                    case 'businessName':
                        result = (self.contactForm.segment !== 'Retail')
                        break
                    case 'addressLine1':
                    case 'city':
                        result = true
                        if (self.isInRegions(['CEU', 'Celavita']))
                            result = false   
                        break
                    case 'firstName':
                        result = true
                        if (self.isInRegions(['CEU', 'Celavita'])) {
                            result = false
                        }
                        break
                    
                }

                self.reqFields[fieldName] = result

                return result
            },

            clearErrors: function () {
                var self = this

                Vue.nextTick(function () {
                    self.$refs['contactForm'].clearValidate() // trick
                })
            },

            validateIfChecked: function (rule, value, callback) {
                var self = this

                if (value) {
                    callback()
                }
                else {
                    callback(new Error(self.getFieldTitle(rule.field) + ' is required'))
                }
            },

            isInRegions: function (regions) {
                return regions.indexOf(this.contactForm.region) > -1
            },

            isNotInRegions: function (regions) {
                return regions.indexOf(this.contactForm.region) == -1
            },

            mimicCheckboxClick: function (fieldName) {
                this.contactForm[fieldName] = !this.contactForm[fieldName]
            }
        }
    });


    // init Vue app
    new Vue({
        el: '#' + g_Settings.applicationId,
        data: {

        }
    })

})();
