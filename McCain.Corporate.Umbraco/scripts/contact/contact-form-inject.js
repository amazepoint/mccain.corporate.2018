﻿window['McCainGCFInitialization'] = window['McCainGCFInitialization'] || (function () {
    function getInjectScriptTag() {
        var me = null;
        var scripts = document.getElementsByTagName("script")
        for (var i = 0; i < scripts.length; i++) {
            if (scripts[i].src && scripts[i].src.indexOf('contact-form-inject.js') > -1) {
                me = scripts[i];
                break;
            }
        }
        return me
    }

    function loadRequiredFiles(scripts, styles, conditionFunc, callback, scriptId) {

        var areFilesAlreadyOnPage = false

        if (typeof conditionFunc == 'function')
            areFilesAlreadyOnPage = conditionFunc()

        if (!areFilesAlreadyOnPage) {

            var filesloaded = 0;
            var filestoload = scripts.length + styles.length;


            for (var i = 0; i < styles.length; i++) {                
                var style = document.createElement('link');
                style.rel = 'stylesheet';
                style.href = styles[i];
                style.type = 'text/css';
                style.onload = function () {
                    
                    filesloaded++;
                    finishLoad();
                };

                document.head.appendChild(style);
            }


            for (var i = 0; i < scripts.length; i++) {
                var script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = scripts[i];
				if (scriptId)
					script.id = scriptId;
                script.onload = function () {
                    filesloaded++;
                    finishLoad();
                };
				
				if (scriptId) {
					if (!document.getElementById(scriptId))
						document.body.appendChild(script)
				} else {
					document.body.appendChild(script)
				}
            }


            function finishLoad() {
                if (filesloaded === filestoload) {
                    if (typeof callback == 'function') {
                        callback()
                    }
                }
            }
        } else {
            callback()
        }
    }

    document.addEventListener("DOMContentLoaded", function () {

        var g_Settings = window['McCainGCFSettings']

        if (typeof g_Settings === 'undefined') {
            throw 'MGCF: McCainGCFSettings is not specified, Contact Form can\'t be rendered'
        } else {

            // validate parent element (if specified)
            var parentElement = null
            if (g_Settings['parentElement']) {
                var element = document.querySelector(g_Settings['parentElement'])
                if (element) {
                    parentElement = element
                }

                if (parentElement == null) {
                    throw 'MGCF: Cannot resolve parent element for McCain GCF, Contact Form can\'t be rendered'
                }
            }

            // validate Country and Segment
            if (!g_Settings['formType']) {
                if (g_Settings['segment'] !== 'Retail' && g_Settings['segment'] != 'Foodservice')
                    throw 'MGCF: Invalid segment value'
            } else {
                if (g_Settings['formType'] !== 'BrokerPortal' && g_Settings['formType'] != 'FirstNotification')
                    throw 'MGCF: Invalid form type'
            }

            function getBaseUrl(url) {

                if (url.indexOf('/') !== 0) {
                    var pathArray = url.split('/')
                    var protocol = pathArray[0]
                    var host = pathArray[2]
                    var url = protocol + '//' + host
                    return url
                } else {
                    return ''
                }
            }

            function getQueryStringParameterByName(name, url) {
                if (!url) url = window.location.href;
                name = name.replace(/[\[\]]/g, '\\$&');
                var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, ' '));
            }

            var currentScript = getInjectScriptTag()
            var baseUrl = getBaseUrl(currentScript.src)

            var redirectUrl = ''
            if (typeof g_Settings['redirectUrl'] !== 'undefined')
                redirectUrl = g_Settings['redirectUrl']

            var locale = 'en'
            if (g_Settings['locale'])
                locale = g_Settings['locale']

            var profile = ''
            if (g_Settings['profile'])
                profile = g_Settings['profile']

            var segment = ''
            if (g_Settings['segment'])
                segment = g_Settings['segment']

            var country = ''
            if (g_Settings['country'])
                country = g_Settings['country']

            var region = ''
            if (g_Settings['region'])
                region = g_Settings['region']

            var subregion = ''
            if (g_Settings['subRegion'])
                subregion = g_Settings['subRegion']

            var brand = ''
            if (brand['brand'])
                subregion = g_Settings['brand']

            var cssclass = ''
            if (g_Settings['cssClass'])
                cssclass = g_Settings['cssClass']

            var formType = ''
            if (g_Settings['formType'])
                formType = g_Settings['formType']

            g_Settings['applicationId'] = 'mccain-gcf-app'

            // insert contact form Vue app
            var div = document.createElement('div')
            div.id = g_Settings['applicationId']
            div.className = 'mccain-gcf'
            div.setAttribute('v-cloak', '')
            var HTML = '<contact-form profile="' + profile + '" formtype="' + formType + '" segment="' + segment + '" country="' + country + '" region="' + region + '" subregion="' + subregion + '" brand="' + brand +  '" baseurl="' + baseUrl + '" redirecturl="' + redirectUrl + '" locale="' + locale + '" cssclass="' + cssclass + '"'

            if (typeof g_Settings.completed === 'function') {
                HTML += ' v-on:completed="window[' + "'" +  'McCainGCFSettings' + "'" + '].completed"'
            }

            if (typeof g_Settings.showConfirmationPopup === "boolean") {
                HTML += ' :showconfirmationpopup="' + g_Settings.showConfirmationPopup + '"'
            }

            HTML += '></contact-form>'

            div.innerHTML = HTML

            if (parentElement != null) {
                // append to element specified in settings
                parentElement.appendChild(div)
            }
            else {
                // insert in the same place where script was placed
                currentScript.parentNode.insertBefore(div, currentScript.nextSibling)
            }

            var revision = getQueryStringParameterByName('r', currentScript.src)
            if (!revision)
                revision = '1.0'

            var version = '4.8'

            // load all required js and css, initialize GCF app
            loadRequiredFiles([baseUrl + '/scripts/vue/vue.min.js'], [],
                function () {
                    return typeof Vue !== 'undefined'
                },
                function () {

                    loadRequiredFiles([baseUrl + '/scripts/element-ui/index.js'], [baseUrl + '/scripts/element-ui/theme-chalk/index.css'],
                        function () {
                            return typeof ELEMENT !== 'undefined'
                        },
                        function () {
                            loadRequiredFiles([baseUrl + '/scripts/element-ui/locale/' + locale + '.js'], [], null, function () {

                                ELEMENT.locale(ELEMENT.lang[locale])

                                loadRequiredFiles([baseUrl + '/scripts/contact/contact-form.js?v=' + version + '&r=' + revision], [baseUrl + '/css/contact-form.css?v=' + version + '&r=' + revision], null, null, 'mccain-gcf-script')
                            })
                        })
                })
        }

    });
})();



