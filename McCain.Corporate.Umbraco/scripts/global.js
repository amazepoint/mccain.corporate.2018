var C_MobileHeaderBodyWidth = 1200

$(function () {
    // Allow  to click links inside of the labels
    $(document).on("tap click", 'label a', function (event, data) {
        event.stopPropagation();
        event.preventDefault();

        var target = $(this).attr('target')
        if (!target) target = '_self'

        window.open($(this).attr('href'), target);

        return false;
    });

    // Worldwide
    $('.search-worldwide-holder .worldwide').on('click', function () {
        var $holder = $('.worldwide-holder')

        if ($holder.hasClass('active')) {
            $holder.removeClass('active')

            if (isMobileViewEnabled()) {
                $('.menu-wrapper').addClass('active')
            }
        }
        else {
            $holder.addClass('active')

            if (isMobileViewEnabled()) {
                $('.menu-wrapper').removeClass('active')
            }
        }

        return false
    })

    $('.worldwide-holder .worldwide-close').on('click', function () {
        var $holder = $('.worldwide-holder')

        if ($holder.hasClass('active')) {
            $holder.removeClass('active')
        }

        return false
    })

    $('.worldwide-holder .worldwide-links-holder .title').on('click', function () {
        $(this).toggleClass('active');
        $(this).parent().toggleClass('active');

        var $nextHolder = $(this).parent().next()
        if ($nextHolder.find('.title-empty').length == 1)
            $nextHolder.toggleClass('active')

        return false;
    });

    // Search
    $('.search-worldwide-holder .search').on('click', function () {
        var $holder = $('.main-search-holder')

        if ($holder.hasClass('active')) {
            $holder.removeClass('active')

            if (isMobileViewEnabled()) {
                $('.menu-wrapper').addClass('active')
            }
        } else {
            $holder.addClass('active')

            if (isMobileViewEnabled()) {
                $('.menu-wrapper').removeClass('active')
            }
        }

        return false
    })

    $('.main-search-holder .main-search-close').on('click', function () {
        var $holder = $('.main-search-holder')

        if ($holder.hasClass('active')) {
            $holder.removeClass('active')
        }

        return false
    })

    $('form .search-button').on('click', function () {
        $(this).parents('form').submit()
    });

    // Menu
    $('.menu-item.level-first').hover(function () {
        if (window.innerWidth > C_MobileHeaderBodyWidth) {
            $(this).toggleClass('active'); return false;
        };
    });
    $('.menu-item.level-first > a').click(function () {
        if (window.innerWidth < C_MobileHeaderBodyWidth) {

            var $menuItem = $(this).parent()

            if ($menuItem.find('.level-second').length !== 0) {

                if ($menuItem.hasClass('active')) {
                    $menuItem.removeClass('active')
                }
                else {
                    $menuItem.addClass('active')
                    $menuItem.parent().find('.menu-item').not($menuItem).removeClass('active')
                }

                return false
            }

        };
    });

    // Mobile Menu
    $('.menu-mobile-icon').on('click', function () {
        if (window.innerWidth < C_MobileHeaderBodyWidth) {
            $(".menu-wrapper").toggleClass('animated');
            $(".menu-wrapper").toggleClass('slideInDown');
            $(".menu-wrapper").toggleClass('active');

            return false;
        }
    });
    $('.menu-mobile-close').on('click', function () {
        $(".menu-wrapper").toggleClass('animated');
        $(".menu-wrapper").toggleClass('slideInDown');
        $('.menu-wrapper').toggleClass('active');

        return false;
    });

    $(window).resize(function () {
        if (window.innerWidth >= C_MobileHeaderBodyWidth) {
            if (!($(".menu-holder").is(':visible')))
                $(".menu-holder").css('display', '')

            $('.menu-item.level-first.active').removeClass('active')
        }
    });

    // I like to know
    $('#like-to-know-ddl').on('click', function () {
        $('.like-to-know-holder').toggleClass('active'); return false;
    });

    // Footer Dropdown
    $('.footer-dropdown-button').on('click', function () {
        $(this).toggleClass('active');
        $(this).siblings('.footer-dropdown-holder').toggleClass('active'); return false;
    });

    // Accordion Item
    $('.accordion-item').on('click', function () {
        $(this).closest('.accordion-item').toggleClass('active'); return false;
    });

    $('.accordion-item .content').on('click', function (e) {
        e.stopPropagation()
    });

    $('.accordion-item .content').on("click", 'a', function (e) {
        e.stopPropagation();
        e.preventDefault();

        var target = $(this).attr('target')
        if (!target) target = '_self'

        window.open($(this).attr('href'), target);
        return false;
    });

    //check if hash tag exists in the FAQs URL
    if (window.location.href.toLowerCase().indexOf("/faqs") > -1 && window.location.hash) {
        var taggedItem = $(window.location.hash).closest('.accordion .accordion-item')
        $(taggedItem).toggleClass('active');
    }

    // Sliders
    var swipers = []

    function fixSwiperNavigation(swiper) {
        var contentholder = $(swiper.$el).find('.image-holder, .video-wrapper');
        if (contentholder) {
            var top = contentholder.height() / 2 + 30

            $(swiper.$el).parent().find('.swiper-button-next, .swiper-button-prev').css('top', top + 'px')
        }
    }

    $('.swiper-container').each(function () {
        var self = this
        var $el = $(self)

        var slidesPerView = $el.attr('data-slides-per-view')

        if (slidesPerView === undefined || slidesPerView <= 0)
            slidesPerView = 3

        var isLooped = $el.data('is-looped')

        var breakpoints = {};

        var key = $el.data('key');
        switch (key) {
            case 'brands':
                breakpoints = {
                    1200: {
                        slidesPerView: 3,
                        spaceBetween: 40,
                    },
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                    }
                };
                break;
            case 'videos':
                if (slidesPerView >= 3) {
                    breakpoints = {
                        1200: {
                            slidesPerView: 2,
                            spaceBetween: 30,
                        },

                        1000: {
                            slidesPerView: 1,
                            spaceBetween: 20,
                        }
                    }
                } else if (slidesPerView >= 2) {
                    breakpoints = {
                        1200: {
                            slidesPerView: 2,
                            spaceBetween: 40,
                        },
                        800: {
                            slidesPerView: 1,
                            spaceBetween: 20,
                        }
                    }
                }
                break;
            case 'header':
                breakpoints = {
                    1200: {
                        slidesPerView: 1,
                        spaceBetween: 20,
                    }
                };
                break;
            default:
                breakpoints = {
                    1200: {
                        slidesPerView: slidesPerView,
                        spaceBetween: 40,
                    },
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 20,
                    }
                };
                break;
        }

        var scrollbar = {};

        if (key !== 'header') {
            scrollbar = {
                el: $el.find('.swiper-scrollbar')[0],
                hide: false,
                draggable: true,
                snapOnRelease: true
            };
        }

        var pagination = {};

        if (key === 'header') {
            pagination = {
                el: $el.find('.swiper-pagination')[0],
                type: 'bullets',
                clickable: true
            };
        }

        var autoplay = false
        
        if (key == 'header') {
            if ($el.data('autoplay')) {
                autoplay = {
                    delay: 7000
                }
            }
        }

        var swiper = new Swiper(self, {
            spaceBetween: 30,
            loop: isLooped,
            freeMode: false,
            autoplay: autoplay,
            grabCursor: true,
            a11y: true, // accessibility
            scrollbar: scrollbar,
            pagination: pagination,
            breakpoints: breakpoints,
            autoHeight: true,
            navigation: {
                nextEl: $el.parent().find('.swiper-button-next')[0],
                prevEl: $el.parent().find('.swiper-button-prev')[0]
            },
            watchOverflow: true,
            on: {
                init: function () {
                    if (key !== 'header')
                        fixSwiperNavigation(this);
                },
                resize: function () {
                    if (key !== 'header')
                        fixSwiperNavigation(this);
                },
                imagesReady: function () {
                    if (key !== 'header')
                        fixSwiperNavigation(this);
                }
            }

        });

        swipers.push(swiper);

    });

    // fix scrollbar
    $(window).resize(function () {
        for (var i = 0; i < swipers.length; i++) {
            swipers[i].update()
            swipers[i].updateAutoHeight(100)
        }
    })

    // fix header
    setTimeout(function () {
        for (var i = 0; i < swipers.length; i++) {
            if ($(swipers[i].$el).data('key') == 'header') {
                swipers[i].update()
                swipers[i].updateAutoHeight(100)
            }
        }
    }, 100)
    

    // Fix heights
    // home page
    $('.news-item .title').matchHeight();
    // careers page
    $('.featured-opportunities-item .title').matchHeight();
    $('.featured-opportunities-item .description').matchHeight();
    $('.featured-opportunities-item .location').matchHeight();

    $('.cta-with-description-widget, .share-widget').matchHeight();

    $('.text-on-image-wrapper').each(function () {
        if ($(this).find('.button-holder').length > 1) {
            // wrap text before button in css-class
            $(this).find('.button-holder').each(function () {
                var $thisButton = $(this);
                if ($thisButton.parent().hasClass('button-wrapper'))
                    $thisButton = $thisButton.parent()

                //$thisButton.prevUntil('h3').wrapAll("<div class='text-wrapped'/>")
            });

            $(this).find('.text-on-image-item .article-content .title').matchHeight();
            $(this).find('.text-on-image-item .article-content .text').matchHeight();
        }
    });

    // Fix home page widget
    $(window).resize(function () {
        $('.box-with-image .box-image img').force_redraw()
    })

});


jQuery.fn.hint = function (blurClass) {
    if (!blurClass) {
        blurClass = 'blur';
    }

    return this.each(function () {
        // get jQuery version of 'this'
        var $input = jQuery(this),

            // capture the rest of the variable to allow for reuse
            title = $input.attr('title'),
            $form = jQuery(this.form),
            $win = jQuery(window);

        function remove() {
            if ($input.val() === title && $input.hasClass(blurClass)) {
                $input.val('').removeClass(blurClass);
            }
        }

        // only apply logic if the element has the attribute
        if (title) {
            // on blur, set value to title attr if text is blank
            $input.blur(function () {
                if (this.value === '') {
                    $input.val(title).addClass(blurClass);
                }
            }).focus(remove).blur(); // now change all inputs to title

            // clear the pre-defined text when form is submitted
            $form.submit(remove);
            $win.unload(remove); // handles Firefox's autocomplete
        }
    });
};


$.fn.force_redraw = function () {
    return this.hide(0, function () {
        $(this).show();
    });
}

function isMobileViewEnabled() {
    return window.innerWidth < C_MobileHeaderBodyWidth
}