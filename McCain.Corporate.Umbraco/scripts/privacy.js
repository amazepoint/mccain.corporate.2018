﻿$(function () {
    var selectedLanguage = ''
    
    // set selected language
    var pathname = window.location.pathname.toLowerCase()
    if (pathname.endsWith('/'))
        pathname = pathname.substring(0, pathname.length - 1)

    $('#language-selector option').each(function () {
        var selected = false

        if (pathname.endsWith('/' + this.value)) {
            selectedLanguage = this.value
            return false
        }  
    })

    
    if (!selectedLanguage)
        selectedLanguage = 'en'

    $('#language-selector option[value="' + selectedLanguage + '"]').prop('selected', true)


    // on language change
    var $parent = $('.privacy-page')

    var relativePath = pathname
    var languagePath = '/' + selectedLanguage
    if (relativePath.endsWith(languagePath)) {
        relativePath = relativePath.substring(0, relativePath.length - languagePath.length)
    }

    $parent.on('change', '#language-selector', function (e) {
        var language = $(this).val()

        var newUrl = relativePath
        if (language !== 'en')
            newUrl += '/' + language

        window.location.href = newUrl
    })


})