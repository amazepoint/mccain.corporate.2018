﻿using Examine;
using Examine.SearchCriteria;
using Lucene.Net.Search;
using McCain.Corporate.Umbraco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedContentModels;

namespace McCain.Corporate.Umbraco.Controllers
{
    public class NewsController : SurfaceController
    {
     
        public ActionResult RenderNews()
        {
            var pageIndex = 0;
            int.TryParse(Request.QueryString["page"], out pageIndex);
            if (pageIndex == 0)
                pageIndex = 1;

            var pageSize = 10;

            string searchTerm = Request.QueryString["s"] ?? "";

            var criteria = new SearchCriteria()
            {
                SearchTerm = searchTerm,
                DocTypeAliases = "newsArticleDocumentType",
                FieldPropertyAliases = "pageTitle,pageContent",
                OrderByFieldPropertyAliases = "articleDate",
                OrderDescending = true
            };

            var paging = new PagingModel()
            {
               PageIndex = pageIndex,
               PageSize = pageSize
            };

            var searchResults = SearchHelper.SearchUsingExamine(criteria, paging);

            var model = new SearchResultsModel(searchTerm, searchResults, paging);

            return PartialView($"~/Views/Partials/Content/_NewsArticles.cshtml", model);
        }

        public ActionResult RenderLatestNews(int itemsCount)
        {
            var newsRoot = UmbracoContext.Current.ContentCache.GetByRoute("/information-centre/news");

            var latestItems = newsRoot.Children.OrderByDescending(i => i.GetPropertyValue<DateTime>("articleDate")).Take(itemsCount);

            var model = new NewsListModel()
            {
                Items = latestItems
            };

            return PartialView($"~/Views/Partials/Content/_LatestArticles.cshtml", model);
        }
    }
}