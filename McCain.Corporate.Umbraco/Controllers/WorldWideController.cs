﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedContentModels;

namespace McCain.Corporate.Umbraco.Controllers
{
    public class WorldWideController : SurfaceController
    {
        public JsonResult GetCategories()
        {
            List<string> categories = new List<string>();

            var navigationSettingsNode = new NavigationSettings(NavigationUtils.GetNavigationSettingsNode());
            var worldWideLinks = navigationSettingsNode.WorldWideLinks;
       
            if (worldWideLinks != null) {
                foreach (var link in worldWideLinks)
                {
                    categories.Add(link.Title);
                }
            }
            
            return Json(categories, JsonRequestBehavior.AllowGet);
        }
    }
}