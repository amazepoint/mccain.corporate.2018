﻿using McCain.Integration.SalesForce;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Umbraco.Core.Logging;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Enums;
using Umbraco.Forms.Data.Storage;
using Umbraco.Web.PublishedContentModels;
using Umbraco.Web.WebApi;

namespace McCain.Corporate.Umbraco.Controllers
{
    public class ContactApiController : UmbracoApiController
    {

        [HttpPost]
        public async Task<HttpResponseMessage> Post()
        {
            var salesForceSettings = Settings.GetSalesForceSettingsNode();

            var userName = string.Empty;

            if (!Authenticate(Request, salesForceSettings, out userName) || !ValidateOrigin(Request, salesForceSettings))
			{
                var unauthorizedResponse = Request.CreateResponse(HttpStatusCode.Unauthorized);
                unauthorizedResponse.Headers.Add("Access-Control-Allow-Origin", "*");
                return unauthorizedResponse;
            }


            var salesForcePostResult = new ContactApiPostResult() { success = false };

            try
            {
                var model = await GetContactModelFromRequest();

                // validate model
                var errorMessages = new List<string>();
                if (model.IsValid(out errorMessages))
                {
                    // profile
                    var profileName = Request.GetQueryNameValuePairs().FirstOrDefault(i => i.Key == "profile").Value;
                    var profile = Settings.GetSalesForceProfile(salesForceSettings, profileName);
                    if (profile == null || !profile.Active)
                        throw new Exception("Sales Force Profile does not exist or inactive");

                    // clone model for log, clear attachments as we don't want to save it
                    var modelCloneForLog = (GlobalContactFormModel)model.Clone();
                    if (modelCloneForLog.Attachments != null)
                    {
                        modelCloneForLog.Attachments = new List<KeyValuePair<string, byte[]>>();
                        foreach (var kvp in model.Attachments)
                        {
                            modelCloneForLog.Attachments.Add(new KeyValuePair<string, byte[]>(kvp.Key, null));
                        }
                    }

                    // log
                    Func<string> modelDataString = delegate ()
                    {
                        return $"GCF Data Post: Data =[{ JsonConvert.SerializeObject(modelCloneForLog) }], Origin=[{GetRequestOrigin(Request)}]";
                    };
                    LogHelper.Info<ContactApiController>(modelDataString);

                    var handler = new ContactFormSalesForceDataHandler(profile);
                    var apiResponse = handler.Send(model);

                    var strApiResponse = apiResponse.Content.ReadAsStringAsync().Result;

                    // log
                    Func<string> getApiResultString = delegate ()
                    {
                        return $"GCF post to SalesForce API Result: StatusCode=[{apiResponse.StatusCode}], Success=[{apiResponse.IsSuccessStatusCode}], Body=[{strApiResponse}]";
                    };
                    LogHelper.Info<ContactApiController>(getApiResultString);

                    // parse result
                    if (apiResponse.IsSuccessStatusCode)
                    {
                        salesForcePostResult.success = true;

                        salesForcePostResult.external_result = JsonConvert.DeserializeObject(strApiResponse);
                    }

                    try
                    {
                        if (salesForceSettings.SaveFormDataLocally)
                        {
                            CreateRecord(Request, profile, modelCloneForLog, salesForcePostResult.external_result, userName);
                        }
                    }
                    catch (Exception e)
                    {
                        LogHelper.Error<ContactApiController>("GCF save to Umbraco Forms failed", e);
                    }
                }
                else
                {
                    salesForcePostResult.errors = errorMessages;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error<ContactApiController>("GCF post to SalesForce failed", ex);
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(JsonConvert.SerializeObject(salesForcePostResult), Encoding.UTF8, "application/json");
            response.Headers.Add("Access-Control-Allow-Origin", "*");

            return response;

        }

        private async Task<GlobalContactFormModel> GetContactModelFromRequest()
        {
            GlobalContactFormModel model = null;

            if (Request.Content.IsMimeMultipartContent())
            {
                var provider = new MultipartMemoryStreamProvider();

                await Request.Content.ReadAsMultipartAsync(provider);

                var formItems = new Dictionary<string, FormItem>();

                // Scan the Multiple Parts 
                foreach (HttpContent contentPart in provider.Contents)
                {
                    var formItem = new FormItem();
                    var contentDisposition = contentPart.Headers.ContentDisposition;
                    formItem.name = contentDisposition.Name.Trim('"');
                    formItem.data = await contentPart.ReadAsByteArrayAsync();
                    formItem.fileName = String.IsNullOrEmpty(contentDisposition.FileName) ? "" : contentDisposition.FileName.Trim('"');
                    formItem.mediaType = contentPart.Headers.ContentType == null ? "" : String.IsNullOrEmpty(contentPart.Headers.ContentType.MediaType) ? "" : contentPart.Headers.ContentType.MediaType;
                    if (!formItems.ContainsKey(formItem.name))
                        formItems.Add(formItem.name, formItem);
                }

                model = new GlobalContactFormModel();

                model.WebSource = GetFormItemValue(formItems, "webSource");
                model.FormType = GetFormItemValue(formItems, "formType");
                model.ContactChannel = GetFormItemValue(formItems, "contactChannel");
                model.ContactChannelDetail = GetFormItemValue(formItems, "contactChannelDetail");
                model.Region = GetFormItemValue(formItems, "region");
                model.SubRegion = GetFormItemValue(formItems, "subRegion");
                model.Segment = GetFormItemValue(formItems, "segment");
                model.Country = GetFormItemValue(formItems, "country");
                model.Subject = GetFormItemValue(formItems, "subject");
                model.Province = GetFormItemValue(formItems, "province");
                model.PostalCode = GetFormItemValue(formItems, "postalCode");

                model.AddressLine1 = GetFormItemValue(formItems, "addressLine1");
                model.AddressLine2 = GetFormItemValue(formItems, "addressLine2");
                model.City = GetFormItemValue(formItems, "city");

                model.BusinessName = GetFormItemValue(formItems, "businessName");
                model.Title = GetFormItemValue(formItems, "title");
                model.ContactName = GetFormItemValue(formItems, "contactName");
                model.FirstName = GetFormItemValue(formItems, "firstName");
                model.LastName = GetFormItemValue(formItems, "lastName");
                model.PhoneNumber = GetFormItemValue(formItems, "phoneNumber");
                model.PhoneNumberExt = GetFormItemValue(formItems, "phoneNumberExt");
                model.CellphoneNumber = GetFormItemValue(formItems, "cellphoneNumber");
                model.FaxNumber = GetFormItemValue(formItems, "faxNumber");
                model.EmailAddress = GetFormItemValue(formItems, "emailAddress");
                model.Comments = GetFormItemValue(formItems, "comments");
                model.ProductName = GetFormItemValue(formItems, "productName");
                model.BatchID = GetFormItemValue(formItems, "batchID");
                model.StoreName = GetFormItemValue(formItems, "storeName");
                model.StoreLocation = GetFormItemValue(formItems, "storeLocation");

                model.ExpiryDate = GetFormItemValueAsDateTime(formItems, "expiryDate");
                model.ReceivedDate = GetFormItemValueAsDateTime(formItems, "receivedDate");
                model.UseByDate = GetFormItemValueAsDateTime(formItems, "useByDate");

                model.QtyOnHand = GetFormItemValue(formItems, "qtyOnHand");
                model.Uom = GetFormItemValue(formItems, "uom");
                model.PurchasePrice = GetFormItemValue(formItems, "purchasePrice");
                model.DistributorName = GetFormItemValue(formItems, "distributorName");
                model.DistributorEmailAddress = GetFormItemValue(formItems, "distributorEmailAddress");

                model.FirstTimePurchase = GetFormItemValueAsBoolean(formItems, "firstTimePurchase");
                model.Upc = GetFormItemValue(formItems, "upc");
                model.ProductCode = GetFormItemValue(formItems, "productCode");
                model.Plant = GetFormItemValue(formItems, "plant");
                model.Division = GetFormItemValue(formItems, "division");
                model.PersonCompletingFormName = GetFormItemValue(formItems, "personCompletingFormName");
                model.PersonCompletingFormEmail = GetFormItemValue(formItems, "personCompletingFormEmail");
                model.PersonCompletingFormType = GetFormItemValue(formItems, "personCompletingFormType");
                model.BrokerageFirm = GetFormItemValue(formItems, "brokerageFirm");
                model.PersonCompletingFormNotification = GetFormItemValueAsBoolean(formItems, "personCompletingFormNotification");

                model.ConsumerAffairsCommunication = GetFormItemValue(formItems, "consumerAffairsCommunication");
                model.PreferredContactMode = GetFormItemValue(formItems, "preferredContactMode");
                model.ComplaintCode = GetFormItemValue(formItems, "complaintCode");
                model.FirstTimePurchase = GetFormItemValueAsBoolean(formItems, "firstTimePurchase");
                model.FoodSafetyIssue = GetFormItemValueAsBoolean(formItems, "foodSafetyIssue");
                model.IsExport = GetFormItemValueAsBoolean(formItems, "isExport");
                model.SpecialInstruction = GetFormItemValue(formItems, "specialInstruction");
                model.Priority = GetFormItemValue(formItems, "priority");
                model.Language = GetFormItemValue(formItems, "language");
                model.CaseBrand = GetFormItemValue(formItems, "caseBrand");
                model.InvoiceNumber = GetFormItemValue(formItems, "invoiceNumber");
                model.DeliveryNumber = GetFormItemValue(formItems, "deliveryNumber");
                model.PurchaseOrderNumber = GetFormItemValue(formItems, "purchaseOrderNumber");

                model.Attachments = new List<KeyValuePair<string, byte[]>>();
                var files = formItems.Values.Where(i => i.isAFileUpload);
                foreach (var file in files)
                {
                    var fileName = new FileInfo(file.fileName).Name;

                    model.Attachments.Add(
                        new KeyValuePair<string, byte[]>(fileName, file.data));
                }
            }
            else if (IsJsonRequest())
            {
                string jsonContent = Request.Content.ReadAsStringAsync().Result;
                model = JsonConvert.DeserializeObject<GlobalContactFormModel>(jsonContent);
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            return model;
        }

        private string GetFormItemValue(Dictionary<string, FormItem> source, string key)
        {
            string result = null;

            if (source.ContainsKey(key))
            {
                result = Encoding.UTF8.GetString(Encoding.Default.GetBytes(source[key].value));
            }

            return result;
        }

        private bool GetFormItemValueAsBoolean(Dictionary<string, FormItem> source, string key)
        {
            var strValue = GetFormItemValue(source, key);

            bool result = false;

            if (!string.IsNullOrEmpty(strValue))
            {
                if (!bool.TryParse(strValue, out result))
                {
                    strValue = strValue.ToLower();
                    if (strValue == "on" || strValue == "yes")
                        result = true;
                }
            }

            return result;
        }

        private DateTime? GetFormItemValueAsDateTime(Dictionary<string, FormItem> source, string key)
        {
            var date = DateTime.MinValue;
            var strDate = GetFormItemValue(source, key);
            if (DateTime.TryParseExact(strDate, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                return date;
            else
                return null;
        }

        private void CreateRecord(HttpRequestMessage request, SalesForceProfileItem profile, GlobalContactFormModel model, object apiResult, string userName)
        {
            string C_FormID = "dc3e8aa3-808a-446c-8d6a-c25744340677";

            var recordStorage = new RecordStorage();

            var formStorage = new FormStorage();

            var form = formStorage.GetForm(new Guid(C_FormID));

            var record = new Record(); 


            foreach (var f in form.AllFields)
            {
                string data = null;


                if (f.Alias == "formData")
                {
                    data = JsonConvert.SerializeObject(model,
                        Formatting.None,
                        new JsonSerializerSettings
                        {
                            NullValueHandling = NullValueHandling.Ignore
                        });
                }
                else if (f.Alias == "apiResult" && apiResult != null)
                {
                    data = JsonConvert.SerializeObject(apiResult);
                }
                else if (f.Alias == "apiSettings")
                {
                    data = profile.Name;
                }
                else if (f.Alias == "origin")
                {
                    data = GetRequestOrigin(request);
                }
                else if (f.Alias == "region")
                {
                    data = model.Region;
                }
                else if (f.Alias == "userName")
                {
                    data = userName;
                }

                if (!String.IsNullOrEmpty(data))
                {
                    var key = Guid.NewGuid();
                    var recordField = new RecordField()
                    {
                        Alias = f.Alias,
                        FieldId = f.Id,
                        Field = f,
                        Key = key,
                        Record = record.Id,
                        Values = new List<object>() { data }
                    };

                    var recordFieldStorage = new RecordFieldStorage();
                    recordFieldStorage.InsertRecordField(recordField);
                    record.RecordFields.Add(key, recordField);
                }
            }

            var recordDataDict = new Dictionary<string, string>();

            foreach (var rf in record.RecordFields)
            {
                recordDataDict.Add(rf.Value.FieldId.ToString(), rf.Value.Values[0] as string);
            }

            record.RecordData = JsonConvert.SerializeObject(recordDataDict);
                    
            record.Form = new Guid(C_FormID);
            record.IP = Request.GetClientIpAddress();
            record.UmbracoPageId = Settings.GetHomePageNode(Umbraco).Id;
            record.State = FormState.Approved;

            recordStorage.InsertRecord(record, form);            
        }

        private string GetRequestOrigin(HttpRequestMessage request)
        {
            var result = "";

            if (request.Headers.Contains("Origin"))
            {
                var headerValues = request.Headers.FirstOrDefault(x => "Origin".Equals(x.Key, StringComparison.InvariantCultureIgnoreCase)).Value;

                if (headerValues != null && headerValues.Any())
                    result = headerValues.FirstOrDefault();
            }

            return result;
        }

        private bool IsJsonRequest()
        {
            return true;
        }

        private bool Authenticate(HttpRequestMessage request, SalesForceSettings settings, out string userName)
		{
            userName = string.Empty;

            if (settings.AuthorizationEnabled)
            {
                if (request.Headers.Contains("Authorization") && !string.IsNullOrEmpty(settings.AuthorizationSettings))
                {
                    try
                    {
                        var authHeader = request.Headers.FirstOrDefault(x => x.Key == "Authorization").Value.FirstOrDefault();
                        if (authHeader != null)
                        {
                            var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);

                            // RFC 2617 sec 1.2, "scheme" name is case-insensitive
                            if (authHeaderVal.Scheme.Equals("basic",
                                    StringComparison.OrdinalIgnoreCase) &&
                                authHeaderVal.Parameter != null)
                            {
                                var authHeaderValString = authHeaderVal.Parameter;

                                var encoding = Encoding.GetEncoding("iso-8859-1");
                                var credentials = encoding.GetString(Convert.FromBase64String(authHeaderValString));

                                int separator = credentials.IndexOf(':');
                                string name = credentials.Substring(0, separator);
                                string password = credentials.Substring(separator + 1);

                                var allApiKeys = JsonConvert.DeserializeObject<ApiKeyItem[]>(settings.AuthorizationSettings);
                                var dbApiKey = allApiKeys.FirstOrDefault(x => x.apiKey == password && x.active);

                                if (dbApiKey == null)
                                {
                                    return false;
                                }
                                else
                                {
                                    userName = dbApiKey.name;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error<ContactApiController>("GCF Authorization Failed", ex);
                        return false;
                    }
                }
                else
				{
                    var originValue = GetRequestOrigin(request);

                    var allApiKeys = JsonConvert.DeserializeObject<ApiKeyItem[]>(settings.AuthorizationSettings);

                    if (string.IsNullOrEmpty(originValue) && !settings.AnonymousAuthorizationEnabled)
                        return false;
				}
            }

            return true;
		}

        private bool ValidateOrigin(HttpRequestMessage request, SalesForceSettings settings)
		{
            var originValue = GetRequestOrigin(request);

            if (string.IsNullOrEmpty(originValue))
                return true;

            if (settings.AllowOriginValidationEnabled && !string.IsNullOrEmpty(settings.AllowOrigin))
			{
                try
				{
                    var allowedList = JsonConvert.DeserializeObject<List<string>>(settings.AllowOrigin);

                    foreach (var s in allowedList)
					{
                        if (originValue.ToLower().Contains(s))
                            return true;
					}
				}
                catch (Exception ex)
				{
                    LogHelper.Error<ContactApiController>("GCF ValidateOrigin Failed", ex);
                }

                LogHelper.Info<ContactApiController>($"GCF ValidateOrigin failed: {originValue} is not valid");

                return false;
			}

            return true;
		}
    }

    class ContactApiPostResult
    {
        public bool success { get; set; }

        public object external_result { get; set; }

        public List<string> errors { get; set; }
    }

    class FormItem
    {
        public FormItem() { }
        public string name { get; set; }
        public byte[] data { get; set; }
        public string fileName { get; set; }
        public string mediaType { get; set; }
        public string value { get { return Encoding.Default.GetString(data); } }
        public bool isAFileUpload { get { return !String.IsNullOrEmpty(fileName); } }
    }

    class ApiKeyItem
	{
        public string apiKey { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public bool active { get; set; }
    }
}