﻿using System;
using System.Web.Http;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Editors;
using Umbraco.Web.PublishedContentModels;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace McCain.Corporate.Umbraco
{
    public class ContentDataApiController : UmbracoAuthorizedJsonController
    {
        [HttpPost]
        public HttpGeneralResponse FixHeaderBanners()
        {
            HttpGeneralResponse response = new HttpGeneralResponse();

            if (UmbracoContext.Current.Security.CurrentUser.IsAdmin())
            {
                try
                {
                    IContentService cs = Services.ContentService;
                    IContentTypeService cts = Services.ContentTypeService;
                    var basicDocType = cts.GetContentType("basicDocumentType");
                    var childrenDocType = cts.GetContentTypeChildren(basicDocType.Id);

                    var updated = 0;
                    var failed = 0;
                    var failedIds = "";

                    var contentPages = new List<IContent>();
                    var pages = cs.GetContentOfContentType(basicDocType.Id);
                    if (pages?.Any() ?? false)
                    {
                        contentPages.AddRange(pages);
                    }
                    foreach (var ct in childrenDocType)
                    {
                        pages = new List<IContent>();
                        pages = cs.GetContentOfContentType(ct.Id);
                        if (pages?.Any() ?? false)
                        {
                            contentPages.AddRange(pages);
                        }
                    }

                    foreach (var page in contentPages)
                    {
                        try
                        {
                            if (!page.Trashed && page.Published)
                            {
                                var content = cs.GetById(page.Id);
                                var bannerCarousel = content.GetValue<string>("headerBannerItems");
                                if (string.IsNullOrEmpty(bannerCarousel))
                                {
                                    var banner = new Dictionary<string, object>() {
                                          {
                                            "bannerTitle",
                                             content.GetValue("bannerTitle")
                                          },
                                          {
                                            "bannerImage",
                                            content.GetValue("bannerImage")
                                          },
                                          {
                                            "bannerImageMobile",
                                            content.GetValue("bannerImageMobile")
                                          },
                                          {
                                            "introduction",
                                            content.GetValue("introduction")
                                          },
                                          {
                                            "navigationUrl",
                                            content.GetValue("navigationUrl")
                                          },
                                          {
                                            "styles",
                                            content.GetValue("styles")
                                          },
                                          {
                                            "cssClass",
                                            content.GetValue("cssClass")
                                          },

                                };

                                    var isValid = banner != null && !(banner.All(x => x.Value == null || !(x.GetType().GetProperties()?.Any() ?? false)));
                                    if (isValid)
                                    {
                                        var items = new List<Dictionary<string, object>>() { banner };
                                        content.SetValue("headerBannerItems", JsonConvert.SerializeObject(items));
                                        cs.SaveAndPublishWithStatus(content);
                                        updated++;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            failed++;
                            failedIds += $"{page?.Id ?? -1};";
                        }

                    }

                    response.Success = true;
                    response.Data = new
                    {
                        message = $"all: {contentPages?.Count ?? 0}, updated: {updated}, failed: {failed}, failedIds: {failedIds}"
                    };
                    return response;
                }
                catch (Exception ex)
                {
                    LogHelper.Error<ContentDataApiController>($"*ERROR* *BackOffice* FixHeaderBanners failed.", ex);

                    response.Errors.Add("Exception: " + ex.Message + " Inner Exception: " + ex.InnerException?.Message);
                }
            }
            else
            {
                response.Errors.Add("Unauthorized.");
            }

            return response;
        }
    }
}