 _    _           _                           ______                       
 | |  | |         | |                         |  ____|                      
 | |  | |_ __ ___ | |__  _ __ __ _  ___ ___   | |__ ___  _ __ _ __ ___  ___ 
 | |  | | '_ ` _ \| '_ \| '__/ _` |/ __/ _ \  |  __/ _ \| '__| '_ ` _ \/ __|
 | |__| | | | | | | |_) | | | (_| | (_| (_) | | | | (_) | |  | | | | | \__ \
  \____/|_| |_| |_|_.__/|_|  \__,_|\___\___/  |_|  \___/|_|  |_| |_| |_|___/ 


=== Version 6.0.0 ===

*New* Form Markup customisation with Themes
*New* Email Workflow - Send HTML emails with Razor
*New* Compatabilty with Umbraco Deploy & Umbraco Cloud & requires a minimum of 7.6.0+


=== Upgraded? ===

If you have upgraded Umbraco Forms please ensure you read the following documentation on our.umbraco.org
https://our.umbraco.org/documentation/Add-ons/UmbracoForms/Installation/Version-Specific


Umbraco Forms runs the following migration tasks in Version 6.0.0

* Moves Umbraco Forms JSON data files from /App_Plugins/UmbracoForms/Data/ to /App_Data/UmbracoForms/Data
* Moves any Forms PreValues that uses text files stored in the Media Section to Umbraco Forms Data storage /App_Data/UmbracoForms/Data/